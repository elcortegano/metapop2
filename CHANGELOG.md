# CHANGELOG for metapop2

This file reflects the most important changes between releases. For further details, please chech the commits in the releases branches in gitlab.

## v2.5.3 (2024-12-05)
- The 'Pop' label for genepop files is followed by the corresponding subpopulation name

## v2.5.2 (2024-12-04)
- Reinstate the 'Pop' label for converted GEN files

## v2.5.1 (2024-12-04)
- Subpopulation labels are preserved when converting to MTP and GEN formats

## v2.5 (2024-10-30)
- Tabular output files are saved as TSV files
- Add option to save tables for the main output file as TSV files

## v2.4.2 (2021-11-17)
- Include boostrap CI for all parameters reported
- Deprecate option pairwise_stats

## v2.4.1 (2021-11-12)
- Fix inbreeding coefficient for poliploids (experimental)

## v2.4 (2020-05-31)
- Important improvements in performance

## v2.3 (2020-03-06)
- Fixes using restriction matrix for management
- Fixes using relationship matrix

## v.2.2.2 (2020-02-18)
- Minor fixes in the use of forbidden and restriction matrix
- Minor fixes in documentation and configuration files

## v.2.2.1 (2019-12-11)
- Allow input of pedigree files without markers if relationship matrix is called.

## v.2.2.0 (2019-11-25)
- Fixed bugs reading mtp files without molecular markers.
- Improvements on functions calling and saving relationship matrix.

## v.2.1.3 (2019-06-10)
- Added option to set ploidy numbers above 2.
- Fixed minor bug for incomplete genotypic information.

## v.2.1.2 (2019-04-27)
- Fixed parsing of trailing blanck spaces in genepop files.

## v.2.1.1 (2019-02-12)
- Fixed bug overwritting frequency files with generations under simulation mode.

## v.2.1 (2019-02-01)
- Added option to read genealogies from PED files.
- Coancestry matrix can be saved as a separate output file.
- Added default values for population structure.
- Population structure can be changed over time under the simulation mode.
- Small fixes.

## v.2.0.0 (2018-10-10)
- Added convergence criteria to boost management computation.
- Improvement and fixes in the main output and documentation files.

**Versions named beta are not stable releases yet**

## v.2.beta.3 (2018-09-22)
- Plink PED files become the default input file format.
- Molecular markers can be inputed as nucleotides.
- Output files revised and simplified.
- Fixes and improvements when using haploid data.
- Other improvements regarding use and conversion of input files.

## v.2.beta.2 (2018-08-28)
- Added bootstrap option.
- Improved call to configuration file.

## v.2.beta.1 (2018-08-10)
- Fixed error reading genepop files.

## v.2.beta.0 (2018-08-05)
- Added option to restrict matings pairs.
- Added option to forbid specific mating pairs.
- Added option to set a maximum offspring per mate.

**Versions named alpha are not stable releases yet**

## v.2.alpha.9 (2018-07-06)
- Cross-software compatibility enhancements.
- Simplifications on the input file format.
- Improvements in the debug option.
- Memory usage improvements.

## v.2.alpha.8 (2018-06-02)
- Important improvement regarding calculation speed of coancestry matrix, and of genetic and allelic parameters.

## v.2.alpha.7 (2018-05-22)
- Portability enhancements.

## v.2.alpha.6 (2018-04-03)
- Fixed bugs for analysis with haploid data.
- Input files accept coancestry matrix data.
- Genepop files can be used as input.
- New option to show pairwise statistics in matrix form.
- New option to calculate contributions to a pool using all methods at once.

## v.2.alpha.5 (2018-01-04)
- Fixed bug for individuals with non-genotyped loci.
- Fixed bug in output section 6 "Summary".
- Improved memory usage.
- Checking input and config files format.
- Main output file is in markdown format, and includes information about the run.
- Output files documentation has been improved.
- Output section "Input for next generation" is optional.
- Management method He has been removed.

## v.2.alpha.4 (2017-12-11)
- Fixed important bugs affecting gene and allele diversity algorithms

## v.2.alpha.3 (2016-12-07)
- Fixed bad initialization of vector causing that Nalleles management method didn't work as expected for more than 1 locus.

## v.2.alpha.2 (2016-11-22)
- New joint file for output of the simulation statistics (to be analysed externally). Simulation statitics removed from the FULL report.
- Added new output (optional) with all the allele frequencies from the simulations.
- Added new output with the number of migrants between pair of subpopulations from the simulation mode.
- Improved memory management and speed.
- Fixed several memory leaks during simulation.
- Fixed bug causing wrong migrations during simulation of management.
- Fixed several minor bugs.

## v.2.alpha.1 (2016-01-18)
- Improved documentation.
- Fixed bug causing a segmentation fault when run using manage=none with simulation mode.
- Fixed bug not reporting obtained He in the pool when run with synthetic=He.
- Minor aesthetical changes in the output.
- Changed extension for the genepop converted file to .gen

## v.2.alpha.0 (2016-01-13)
- This is the intial code of metapop2, formerly tagged as 14.07.08. It is the first public release.
