# Metapop2 (v2.5.3) [![build status](https://gitlab.com/elcortegano/metapop2/badges/master/pipeline.svg)](https://gitlab.com/elcortegano/metapop2/commits/master)

## Overview

**Metapop2** provides an analysis of gene and allelic diversity in subdivided populations from molecular genotype or coancestry data as well as a tool for the management of genetic diversity in conservation programs. A partition of gene and allelic diversity is made within and between subpopulations, in order to assess the contribution of each subpopulation to global diversity for descriptive population genetics or conservation purposes. In the context of management of subdivided populations in in-situ conservation programs, the software also determines the optimal contributions (i.e. number of offspring) of each individual, the number of migrants, and the particular subpopulations involved in the exchange of individuals in order to maintain the largest level of gene diversity in the whole population with a desired control in the rate of inbreeding. A simulation mode allows for predicting the outcome of such management during several generations. Relative contributions from each subpopulation can also be obtained if they should contribute to produce a single pool (a synthetic population or a germplasm bank) of maximal heterozygosity or allelic richness (see more in the [Fundamentals wiki page](https://gitlab.com/elcortegano/metapop2/wikis/Fundamentals)).

Metapop2 is an extended reimplementation of [metapop](http://webs.uvigo.es/anpefi/metapop/) with significant new features as the analysis/management of allelic diversity, the simulation of the management and optimization of the number of alleles among other improvements.

## Quick Install

You can download Metapop2 from the terminal and compile it by entering:

```
git clone https://gitlab.com/elcortegano/metapop2.git
cd metapop2
make
```
then you can copy or move the `metapop` binary to a directory in your PATH or to working directories. You can also install it by entering `sudo make install`. Some examples of data and configuration files can be found in the directory `templates/`.

Metapop2 can also be installed as a [Conda](https://docs.conda.io/en/latest/) package. To install it in the current environment, enter:

```
conda install -c elcortegano metapop
```

Alternatively, you can also use [Docker](https://www.docker.com/) to download and safely run a Metapop2 docker image independently of the OS you use:

```
docker pull elcortegano/metapop2:latest
docker tag elcortegano/metapop2:latest metapop
docker run --rm -v $PWD:/tmp metapop datafile config
```

A ready-to-use executable for Windows can also be downloaded directly by clicking below:

 - [Download Windows executable](https://metapop2.webs.uvigo.es/bin/metapop.exe)

More information about alternative ways of installation in the [wiki](https://gitlab.com/elcortegano/metapop2/wikis/Installation-details).

## Running Metapop2

Metapop2 needs a datafile (in the right format, see below) with the population information, as well as a set of instructions to define settings parameters (described in 'Configuration File' section below).

Assuming you have your `datafile` and your modified `config` files in your working directory, and the `metapop` executable in a directory in your $PATH, then the program can be launched in this way:
```
metapop datafile config
```

For example, using the files provided in the `templates/` folder, metapop2 can be run simply with the command:

```
metapop templates/input.ped templates/conf_example
```

Sections below contain more information on the input format, as well as on the additional options you may want to use or change in the config file, and their default values. In what follows, this section describes alternative ways to run metapop.

Without a configuration file, metapop2 can be run using the default parameters only:
```
metapop datafile
```

A more advanced way to run Metapop enables the use of parameters entered directly from the terminal, using a notation with double-dash (--), including a `--config=<FILE>` option that allows to read a configuration file:
```
metapop [--config=<configMTP>] [--options] datafile
```

Note that the parameters defined in the configuration file will prevail over the default ones, but those settings entered from the terminal will prevail over all. This is the recommended way to use metapop2. See more examples in section 'Command line terminal options' below.

A help summary of the command line terminal options can be shown using the `--help` option as follows:

```
metapop --help
```

After metapop finishes running, and depending on the options used, you will see a bunch of files (see description below) with the name you have specified in the configuration file for this run (see 'Output' section below).

## Input file format

Metapop2 has its own file format for input data (.mtp) following the previous version, but the current release uses [plink PED](https://www.cog-genomics.org/plink/1.9/formats#ped) files as the default. Only note that the first field in the PED file (Family ID, or FID) must be indicative of subpopulations (not families), so the full PED file represents a population with different subpopulations. [Genepop format](http://genepop.curtin.edu.au/help_input.html) is also accepted for population analyses. Examples of [plink](templates/input.ped) and [genepop](templates/input.gp) formats are given in the `templates/` folder. Metapop input format is described below.

### Metapop format

Metapop files are loaded as plain text file (preferably ASCII text) with any extension (although we recommend using “.mtp"). Input files are divided in two sections: header and body. Parameters and values can be separated by tabs or spaces. Comments are not allowed in the file.

The **header** is optional, and includes summary information of the population. Namely, the number of subpopulations (`n`) and/or the total number of loci (`nloci`). Note that, if this information is included it should match the actual number of subpopulations and loci declared in the file, and that the number of loci must be the same for all individuals. For example:

```
n 3
nloci 2
```

The **body** contains information on the specific subpopulations and individuals used. The declaration of each subpopulation must begin with a one-word label (without whitespace characters) naming the subpopulation (*e.g.* Pop1). In separated rows, it can optionally include information on the total number of individuals (`N`) or its effective size (`Ne`). If some management method is going to be used, the desired number of females and males in the next generation (`newN` or `NewN`) can be set here or in a **management file** (see below), otherwise the population will retain its structure after management.

The last rows for each subpopulation correspond to one individual per line. In the *legacy* Metapop input format, individuals are declared with a unique ID (numbered from 1 to N), a label for each individual that does not need to be unique (*e.g.* same label for a family), and the sex in the third column, being 0 for females and 1 for males. The remaining columns are used to input the genotypes, including two alleles for diploid genomes or one for haploids (if other level of ploidy is enabled, three for triploids, etc.). These alleles are expected to be entered as numbers by default (from 1 to 65535), but a nucleotide notation is also accepted (see options below). Missing alleles are coded as 0. For example:

```
Pop1
N 6
Ne 6
newN 3 3
1 c1 0 23 13 24 56
2 c1 0 22 13 56 56
...
Pop2
N 6
N 6
newN 5 5
7 d1 0 13 13 56 56
8 d2 0 16 16 31 56
...
```

A complete example of *legacy* input is included in the templates folder (raw file: [input1.mtp](templates/input1.mtp)). In the simplified format version, the individual ID is not required, and the label and gender are separated from genetic information with a comma character. Individual gender is optional for population analyses, but required for management, while a label is always mandatory. Individual 1 above could be in fact declared as:

```
c1 0, 23 13 24 56
```
or for population analysis only:

```
c1, 23 13 24 56
```

A simplified input file for management is included in [templates/input2.mtp](templates/input2.mtp). Note that it will generate the same output than the file presented before. If management is not required, that same file can be further simplified (see [input3.mtp](templates/input3.mtp)).

### Management file

The default plink PED files do not specify all required information for management as the metapop legacy format does (*e.g.* the desired number of females and males in the next generation). Because of that, an additional management file can be used in order to set that information, as well as other additional information useful for management.

Management files will be called from the configuration file or with the `--mng-file=<FILE>` option. These files must begin with a line with the string `newN` (or `NewN`) followed by a line per subpopulation indicating the label of that population, as well as the desired number of females and males, in that order. For example:

```
newN
Pop1 3 3
Pop2 5 5
...
```

These values of `newN` will override not only the default values, but also any other value set in the input file (*e.g.* if the Metapop legacy format is used).

An example management file can be found in [templates/manage.txt](templates/manage.txt).

In addition, this file can contain additional, optional information useful for management, as detailed below.

The population structure can be changed over generations under the simulation mode. This can be done in terms of the population sizes, including reduction of the number of populations if their size is changed to zero, but the original number of populations cannot be increased. The keyword `t` can be used to include these structural changes. This must be followed by the implied generations, and the number of females and males in a similar format as used for `newN` parameter. For example:

```
t 2 5 10 15
4 4 5 5 6 6 7 7
6 5 7 5 8 5 9 5
...
```

means that structural changes are going to be done during generations 2, 5, 10 and 15 (apart from generation 1, which is given by the `newN` parameter directly). For the first population (in the first row), we are going to increase its population size to 4 females and 4 males in generation 2, and we will add an additional female and female in generations 5, 10 and 15 up to a total of 7 females and 7 males. For population 2 (in the second row), we will increase only the number of females by one each generation, up to a population composed by 9 females and 5 males in generation 15.

If populations are going to be extinct (for example, because we merge one population into another to create a bigger population, while setting migrants), order the populations so that the remaining populations are placed on the top rows, and the ones becoming extinct at the end. **This order must be maintained for the entire dataset, including the input datafile**. Otherwise the program will return an error.

The keyword `restrictions` allows for adding a matrix with a restricted mating schedule for all the females in the population. This will assume that all matings have already taken place, and that the offspring has been generated. Thus, running the program with this matrix will allows for selecting individuals from the offspring that optimize the management criteria chosen. This matrix has four columns: the ID of the female, the male ID, and the number of females and males in the offspring, respectively. It is mandatory to include one row per female:

```
restrictions
1 5 4 4
2 4 5 6
7 10 3 2
8 11 4 6
...
```
In the example above, the female 2 is paired with male 4, and together have 5 daughters and 6 sons.

Alternatively, the keyword `forbidden` can be used in order to restrict the males that each female can mate. This matrix contains a row for any female in the subpopulation. The first column will correspond with its ID. Successive columns can be used to append the males' ID that cannot be a mating pair.

```
forbidden
1 5 4
2 5
7
8
...
```
In the example above, female 1 cannot mate with males 5 or 4, while female 2 will be able to mate with 4 but not 5, and females 7 and 8 have no mating restrictions. Note that at least one male will be required to be available for any female within subpopulations.

**Note on restrictions and forbidden matrix formats:** These matrix require that the ID of the individuals are numerical and sorted from 1 to N, with independence of the input file format, or the individual IDs or labels used in that file.

**Note for Metapop Legacy format:** Also remember that this management file cannot be used when using the complete, legacy format of metapop. In these cases, append the above alternative `matrix`, `restrictions` and `forbidden` to the end of the datafile body.

## Relationship matrix file

An ordered genealogical coancestry matrix (triangular or square) can be specified by preceding it by the keyword `matrix`. This file can be called from the configuration file or using the `--rmatrix` option.

```
matrix
0.5
0.125 0.5
0.125 0.125 0.5
...
```

Using a genealogical coancestry matrix will override any information about molecular markers regarding coancestry, and any analysis of population genetic diversity will be performed on the basis of that matrix. In consequence, it is not possible to run a management method or any other option based on allelic information when using this matrix. Output files or sections containing locus or allele specific parameters will not be generated. Note that this matrix does not need to contain the exact values of expected genealogical coancestries, and that these values can be automatically used with the `--pedigree` option using PED files, without needing to manually enter this matrix.

## Configuration file

All the options and configurations to run metapop2 lie in a text configuration file. They consist of a parameter, followed by an equal ("=") sign and the value given to the parameters.
These parameters could be given in any order. Create/open/edit a text file with your favorite text editor. You can get [here an example of config file](templates/conf_example). Comments are allowed in the configuration file, preferably prepended by "#", though this is not mandatory (unless they contain an equal sign).

Note that Metapop can also be called using a default configuration or options entered from the terminal (see section 'Command line terminal options' below).

The following are the descriptions for all possible parameters used by metapop as found in the configuration file, with the alternative values between brackets and separated by "|" (`parameter = [option1|option2]`). The default value goes in first place. If there are not brackets in the value field, that means that you can change the value to 'any' other (see description) and the displayed value is the default. Just one option should be included in the file without any bracket or special symbol `parameter = option1` and only one parameter per line. If a parameter, or its value, is not included in the file, the program will take its default value.

### General Options
- **name = MTP** - ID for this set of configuration parameters. It will be included as part of the results' filenames as well as in some reports. Do not use whitespaces, quotation marks or special symbols are not allowed in filenames. If not provided by the user, it would use the default "MTP".

- **format = [ped|pedigree|mtp|gp]** - The format of the input datafile. By default it is the plink PED format [ped], but metapop ('mtp') and genepop ('gp') formats are also accepted. Genealogical information from pedigrees, instead of molecular, can also be used with PED files (option 'pedigree'), or with PLINK FAM format (which corresponds to the first six columns on the PED format, after removal of molecular genotypes).

- **debug = [false|true]** - Set the debugging mode [true] to print additional information to report any bug.

- **nucleotide = [false|true]** - Enter genotypes in numeric [false] or nucleotide [true] form in the input file.

- **dec = 4** - Floating point precision for the output.

- **seed = time** - Integer controlling seed for pseudorandom number generation. Time is used by default.

- **save_rmatrix = [false|true]** - If true, saves the coancestry matrix. When used with management, it also saves the coancestry matrix after management. Under the simulation mode, a matrix is outputted per replicate and generation.

- **save_tsv = [false|true]** - If true, saves tabular data from the main input file into separate TSV files.

- **ploidy = [2|1|>2]** - Sets the ploidy level: diploid [2], haploid [1], or any other level of ploidy above 2 (experimental). Please note that haploid data cannot be used for management, and only population analysis will be run.

### Population analysis

- **rarefaction = [true|false]** - Switches between the application of rarefaction method in the calculation of allelic diversity [true] or not [false].

- **rareN = 0** - Sets the value for the rarefaction sample size. It should be lower than the smallest subpopulation size. If settled to 0 (default) then automatically gets the value of the smallest subpopulation size present in the data. This parameter has no effect if `rarefaction = false`.

- **NeiChesser = [false|true]** - If true, obtains F-stats using Nei & Chesser's correction for sample size (for those cases analysing samples and not the whole populations).

- **bootstrap = 0** - Number of bootstrap replicates for computing confidence intervals for the main summary statistics of the population, as F-statistics and Ast. By default, no intervals are computed.

- **alpha = 0.05** - Level of significance for bootstrap confidence intervals.

### Optimal contributions of each population to a synthetic pool

- **synthetic = [none|H|K|all]** - It performs a simulated annealing algorithm (Kirkpatrick et al. 1983) to obtain the relative contributions (in number of individuals) from each subpopulation to produce a single pool (a synthetic population or a germplasm bank) with maximum heterozygosity (H), number of alleles (K) or both (all). The default option is none, which skips the procedure.

- **nPool = 1000** - Sets the size of the pool in number of individuals.

- **minimum = 0.0** - Sets the minimum contribution of each population.

### Management of the population

- **manage = [none|genetic|allelic|Nalleles|random]** - Manage optimizing [genetic] or [allelic] diversity. Also [Nalleles] for optimizing total expected number of alleles, [random] for random mating, or no management [none] to skip it.

- **mng_file =** - Name of the management file.

- **method = [dynamic|ompg|isolated]** - management method used for management of diversity. These are the methods:
  - *dynamic* - Dynamic method. It performs the management with the method described by Fernández et al. (2008). See Background for further details.
  - *isolated* - Isolated populations. This method searches for the contributions to minimize the coancestry (optionally the inbreeding too) in each subpopulation without any migration.
  - *ompg* - OMPG (One Migrant Per Generation). Applies the Isolated Populations method and then forces that a subpopulation randomly chosen gives a random migrant to another subpopulation and receives another one from it.

- **convergence = 50** - Minimum management steps (inside SA steps) required for convergence.

### Restrictions

These are the restrictions considered for the management mode

- **max_migrants = 0** - Maximum number of migrants. The user can specify the maximum number of migrations desired in the whole population.

- **lambda = 1** - Lambda. Is the factor balancing the relative importance of within-subpopulation coancestry (Hs or As, for gene and allelic diversity, respectively) in relation to between-subpopulation (Dg or Da) diversity for the dynamic method (Fernández J *et al.* 2008). If its value is 0, then the within-subpopulation coancestry is ignored and the method maximizes only between-subpopulation diversity. If lambda equals 1, then both components have the same weight.

- **max_delta_F = 0.0** -     It makes a constraint in the maximum per-generation rate of inbreeding (Delta\_F) allowed in the population. This option is alternative to the use of *lambda* parameter, so it may be safer to remove it if it is not desired.

- **monogamy =  [true|false]** -  Monogamy. The algorithm searches for solutions where males only mate to one female. If false, males can mate to more than one female.

- **weight_F_isol = 1** - Weighting factor (between 0 and 1) of inbreeding in relation to coancestry in order to minimize both in each subpopulation. If the factor is 0 then inbreeding is not directly controlled (for ompg and isolated methods only)

- **max_off_mate = 0** - Maximum possible number of offspring per mating pair. If set to 0 (default), there is no maximum.

### Simulated annealing parameters

The user can set different parameters affecting the performance of the simulated annealing routine used for the optimization.

- **SA_steps = 200** - Simulated annealing steps. Each step consists of 1000 evaluated solutions using the same temperature.

- **SA_temp = 0.1** - Temperature. sets the initial value for temperature.

- **SA_k = 0.9** - K. Rate of temperature change from a step to the next.

### Simulation mode

- **simMode = [false|true]** - Toggle the simulation mode of the management method.

- **simReps = 1** - Set the number of replicates in the simulation mode

- **simGens = 1** - Sets the number of generations to run in the simulation mode. Note that the current population in the data file is considered generation 0.

- **saveStats = [true|false]** - Toggle the saving of population parameters in a file.

- **saveFrequencies = [false|true]** - Toggle the saving of all allelic frequencies in a file.

- **saveMigrants = [false|true]** - Toggle the saving of all migrants between subpopulations in a file.

### Cross-software compatibility

- **convert = [none|mtp2gp|mtp2ped|gp2mtp|ped2mtp]** - Switches between the normal run [none] and the conversion of the input file into another format. [mtp2gp] converts from Metapop into [Genepop](http://genepop.curtin.edu.au/) file formats, and [mtp2ped] into [PINK PED](https://www.cog-genomics.org/plink/1.9/formats#ped) file format. The values [gp2mtp] and [ped2mtp] do the opposite conversion. If the input file is other different than the default PED files, the input file format must be declared as usual (*e.g.* by setting the *format* option). Note that in conversion mode, the program will finish without performing any analysis or management, just creating a new datafile in the specified format. The name of the new file is the ID specified above plus the corresponding ".gen", ".ped" or ".mtp" extension.

## Command line terminal options

As described in the 'Running Metapop2' section above, the program can be run calling command line terminal options, that will prevail over any other default or parameter settled in the configuration file. All these options are preceded by double-dash (--), and are analogous to other parameters present in the configuration file with a few exceptions. For example, the following command run an analysis to generate an output file as in [templates/res\_example\_FULL.md](templates/res\_example\_FULL.md):

```
metapop --config=templates/conf_example templates/input.ped
```

This usage allows to easily add more options on the go. For example, we can now decide to manage by gene diversity, instead of by allelic diversity (as it appears in [templates/conf_example](templates/conf_example)):

```
metapop --config=templates/conf_example --manage=genetic templates/input.ped
```

Note that in many cases the configuration file is not needed if we rely on the defaults, and only set a few options of interest:

```
metapop --manage=genetic --synth=all templates/input.ped
```

Of course, all of this options are compatible with other input formats. For example, to use metapop format one can enter:

```
metapop --config=templates/conf_example --mtp templates/input1.mtp
```

Note that if datafile has metapop format, the `--mtp` option will we needed. Otherwise, if it has the plink PED file format, a management file can be called, adding for example: `--mng-file=templates/manage.txt` (otherwise default `newN` would be assumed for management):

```
metapop --config=templates/conf_example --mng-file=templates/manage.txt \
templates/input.ped
```

Find a more complete description of the parameters in the 'Configuration file' section above. Some parameters require an equal sign (=) and to be followed by a value, the same as options in the configuration file, but other options are called alone. Available options are listed below:

### General options
- `--config=<FILE>` : It enables reading a configuration file and all parameters within.
- `--help` : Prints a summary of the command line terminal options.
- `--name=<NAME>` : Gives a name to the run analysis.
- `--mtp` : Specifies that the input datafile has metapop format.
- `--ped` : Input datafile has PLINK PED format (by default).
- `--pedigree` : Input datafile has PLINK PED format, and genealogical information instead of molecular is read.
- `--gp` : Input datafile has genepop format.
- `--rmatrix` : Name of the relationship matrix file.
- `--save-tsv` : Saves tabular results into TSV files.
- `--nucleotide` : Indicates that genotypes are entered with nucleotide notation in the input file.
- `--haploid` : Sets analysis for haploid genomes.
- `--debug` : Enables debug mode.
- `--dec=<INT>` : Set floating point precision.
- `--seed=<INT>` : Set a seed for pseudorandom number generation.
- `--save-rmatrix` : Saves the coancestry matrix.
- `--save-tsv` : Saves tabular results into TSV files.
- `--pairwise-stats` : Prints pairwise stats in matrix form.

### Population analysis
- `--rarefaction` : Applies a rarefaction method for allelic diversity (default).
- `--no-rarefaction` : Disables rarefaction.
- `--rareN=<INT>` : Sets the value for rarefaction sample size.
- `--nei-chesser` : Calculates F-stats using Nei & Chesser's correction.
- `--bootstrap` : Computes confidence intervals using a given number of bootstrap replicates.
- `--alpha` : When running bootstrap, it sets the level of significance for the confidence intervals.

### Synthetic pool
- `--synth=<MODE>` : Sets a method to obtain contributions to a pool
- `--synth-pool=<INT>` : Size of the synthetic pool.
- `--synth-min=<INT>` : Minimum contribution of each population.

### Management
- `--manage=<MODE>` : Sets a management strategy for the population.
- `--mng-file=<FILE>` : Name of management file.
- `--method=<MODE>` : Sets a method for the management of genetic diversity.
- `--convergence=<INT>` : Minimum management steps required for convergence.

### Restrictions
- `--max-nmigrants=<INT>` : Maximum number of migrants.
- `--lambda=<NUM>` : The factor balancing the importance of within and between diversity.
- `--max-delta-F=<NUM>` : Maximum per-generation rate of inbreeding.
- `--monogamy` : Sets monogamy behavior (default).
- `--polygamy` : Allows polygamic behavior.
- `--weight-F-isol=<NUM>` : Weighting factor of inbreeding in relation to coancestry.
- `--max-off-mate=<INT>` : Maximum number of offspring.

### Simulated annealing
- `--SA-steps=<INT>` : Number of simulated annealing steps.
- `--SA-temp=<NUM>` : Initial value of temperature in SA algorithm.
- `--SA-k=<NUM>` : Temperature change rate in SA algorithm.

### Simulation mode
- `--simMode` : Enables simulation mode, saving all relevant output files.
- `--sim-t=<INT>` : Sets number of generations.
- `--sim-r=<INT>` : Sets number of replicates.

### Cross-software Compatibility
- `--mtp2ped` : Enables conversion mode from Metapop to PLINK PED format.
- `--mtp2gp` : Enables conversion mode from Metapop to Genepop format.
- `--ped2mtp` : Enables conversion mode from PLINK PED format to Metapop.
- `--gp2mtp` : Enables conversion mode from Genepop to Metapop format.

## Output

While running, the program would show some information of the steps that are running. The results will be saved into a text file called `res_[name]_FULL.md`, where `[name]` is the name provided in the configuration file. Depending on the options, other files could be generated.

### Main output file

- **res\_[name]\_FULL.md** - This is the main output file with the full report for both the population analysis and population management, divided in several sections, depending on the options executed. It will begin with a short header showing execution details as the version number, date & hour, the seed used, the name of the data and configuration files, as well as the name given to the set of parameters in the configuration file.

  1. *Results of population analysis*: This section contains tables showing results for gene and allelic diversity within and between subpopulations, including inbreeding, coancestry, and genetic distances. Contributions of subpopulations to global diversity from the gain/loss of diversity after removal of each subpopulation, and contributions to a synthetic population are also shown. See details below.

  2. *Mating matrix*: A table with the optimal contributions from each individual to the next generation in order to maximise gene or allelic diversity within the required constraints.

#### Output parameters for gene diversity

Parameters are calculated following Caballero and Toro (2002) (Converv. Genet. 3: 289-299; and references therein). These can be obtained from coancestry values or directly calculated from allelic frequencies obtained from molecular data, and include:

**Within-subpopulation parameters:**

 - *f(ii)*: Average coancestry between individuals of subpopulation *i*.
 - *s(i)*: Average self-coancestry of individuals of subpopulation *i*.
 - *F(i) = 2s(i)-1*: Average coefficient of inbreeding of individuals of subpopulation *i*.
 - *d(ii) = [s(i)-f(ii)]*: Average Nei's distance between individuals of subpopulation *i*.
 - *G(i) = d(ii) / [1-f(ii)]*: Proportion of variation between individuals for subpopulation *i*.
 - α *(i) = [F(i)-f(ii)] / [1-f(ii)]*: Average deviation from Hardy-Weinberg equilibrium of subpopulation *i*.
 - *1-*α*(i) = 2[1-G(i)]*.

Averages:

 - *tilde(f)*: Average f(ii) for all *n* subpopulations.
 - *tilde(s)*: Average s(i) for all *n* subpopulations.
 - *tilde(F)*: Average *F(i)* for all *n* subpopulations.
 - *tilde(d)*: Average *d(ii)* for all *n* subpopulations.
 - *G*: Average proportion of variation between individuals for all *n* subpopulations.
 - α *= F(IS)*: Average α *(i)* for all *n* subpopulations.
 - *H(I) = 1-tilde(F)*: Observed heterozygosity of individuals averaged for all *n* subpopulations.

**Between-subpopulation parameters:**

 - *f(ij)*: Average coancestry between pairs of subpopulations.
 - *D(G,ij) = {[f(ii)+f(jj)]/2}-f(ij)*: Nei's minimum genetic distance between pairs of subpopulations.
 - *F(ST,ij) = [D(G,ij)/2] / {1-[f(ii)+f(jj)+2f(ij)]/4}*: Wright's fixation index between pairs of subpopulations (Wright, 1951, Ann.Eugen. 15: 323-354).

**Average gene diversity parameters:**

 - *bar(f)*: Average coancestry between pairs of subpopulations.
 - *H(S)* = 1-tilde(f): Average gene diversity within subpopulations. Equals the expected heterozygosity within subpopulations.
 - *D(G) = H(T)-H(S) = tilde(f)-bar(f)*: Average gene diversity between subpopulations (Average Nei's minimum genetic distance across all *n^2* pairs of subpopulations).
 - *H(T) = 1-bar(f)*: Total gene diversity (expected heterozygosity) in the whole population.

Wright's F-statistics (Wright, 1951, Ann. Eugen. 15: 323-354). These can be corrected for sample size if required following Nei and Chesser (1983, Ann. Hum. Genet. 47: 253-259).

 - *F(IS) = [H(S)-H(I)] / H(S) = [tilde(F)-tilde(f)] / [1-tilde(f)]*
 - *F(ST) = [H(T)-H(S)] / H(T) = [tilde(f)-bar(f)] / [1-bar(f)]*
 - *F(IT) = [H(T)-H(I)] / H(T) = [tilde(F)-bar(f)] / [1-bar(f)]*

**Partition of gene diversity variation (in proportion)**

 - Within individuals: *(1-G)[1-F(ST)]*.
 - Between individuals: *G[1-F(ST)]*.
 - Within subpopulations: *1-F(ST)*.
 - Between subpopulations: *F(ST)*.

Percentage of loss (+) or gain (-) of gene diversity after removal of each subpopulation: This is achieved by removing the data from each subpopulation in turn and reanalyzing the data, as suggested by Petit *et al*. (1998, Conserv. Biol. 12: 844-855).

#### Output parameters for allelic diversity

Parameters are calculated following Caballero and Rodríguez-Ramilo (2010) (Conserv. Genet. 11: 2219-2229; and references therein), and include:

**Within-subpopulation parameters**

 - *A(S,i)*: Within-subpopulation allelic diversity of subpopulation *i* [allelic richness with rarefaction or not as defined by El Mousadik and Petit (1996, Theor. Appl. Genet. 92: 832-839), minus 1].
 - *K(i)*: Mean number of alleles per locus of subpopulation *i*.
 - *A(P,i)*: Mean number of private alleles per locus of subpopulation *i* (Kalinowski, 2004, Conserv. Genet. 5: 539-543). 

**Between-subpopulation parameters:**

 - *D(A,ij)*: Allelic distance between pairs of subpopulations, defined as the number of alleles present in a subpopulation and absent in the other.
 - *A(ST,ij) = D(A,ij) / {[A(S,i) + A(S,j)]/2 + D(A,ij)}*: Allelic differentiation index between pairs of subpopulations.

**Average allelic diversity parameters:**

 - *K*: Average number of alleles per locus in the whole population.
 - *A(S)*: Average allelic diversity within subpopulations [averaged *A(S,i)* for all *n* subpopulations].
 - *D(A)*: Average allelic diversity between subpopulations. The averaged *D(A,ij)* between all pairs of subpopulations [for all *n^2* pairs of subpopulations, *i.e.* including *D(A,ii)=0*].
 - *A(T) = A(S)+D(A)*: Total allelic diversity. The pairwise diversity of subpopulations, *i.e.* the number of different alleles available in each pairwise grouping of subpopulations.
 - *A(ST) = D(A) / [A(S)+D(A)]*: The allelic differentiation index.

Percentage of loss (+) or gain (-) of gene diversity after removal of each subpopulation: This is achieved, as for gene diversity, by removing the data from each subpopulation in turn and reanalyzing the data, as suggested by Petit *et al*. (1998, Conserv. Biol. 12: 844-855).

#### Synthetic population

Percentage of individuals contributed from each subpopulation to a pool of individuals with maximal heterozygosity (*H*) or number of alleles (*K*).

#### Examples of use

Please note that this file is written in markdown format, so it can be conveniently converted into other different formats including HTML or PDF. You can try to use [pandoc](http://pandoc.org/) as follows, to get this file into HTML.

```
pandoc file.md -f markdown -t latex --pdf-engine=xelatex -s -o file.pdf
```

As example, see here [the full report](templates/res_example_FULL.md) created by running: metapop [templates/input1.mtp](templates/input1.mtp) [templates/conf\_example](templates/conf_example). Gitlab may not show markdown tables correctly, if so, consider to read the output example as raw plain text.

### Other output files

Other accessory files may be generated, and are aimed to be used on separated analysis. Because of that, are given in tab-separated value (TSV) format, so they can be conveniently opened by statistic programs as [R](https://www.r-project.org/).

- **res\_[name]\_p.tsv**: This file contains a table in tab-separated values (TSV) format, with a description of all loci, their alleles, and the corresponding frequencies in each subpopulation as well as in the population. See an [example file here](templates/res_example_p.tsv). If both `simMode` and `saveFrequencies` are enabled, information regarding the replicate and generation numbers will be included in the file.

- **res\_[name]\_simStats.tsv**: In case that `simMode` and `saveStats` are enabled the results for every parameter in each generation and replicate are stored in this file. These parameter are the mean inbreeding (*F*), coancestry (*f*), self-coancestry (*s*), genetic distance (*D*), genetic diversity (*Hs*, *Dg*, *Ht*), Wright's F-statistics (*Fis*, *Fst*, *Fit*), statistics of allelic diversity (*As*, *Da*, *At*, *Ast*), and the mean number of alleles per locus (*K*). It also includes the number of migrants (*nMigrants*), and contribution parameters as the mean and variance of the female contributions [*meanCont(fem)* and *VarCont(fem)*], and the number of mates (*nMates*).

- **res\_[name]\_migTable.tsv**: In case that `simMode` and `saveMigrants` are enabled the number of migrants between every pair of subpopulations in each generation and replicate are stored in this file.

- **res\_[name]\_rmatrix\*.txt**: If `save_rmatrix` is enabled, a file containing the triangular matrix of coancestry will be saved with the name **res\_[name]\_rmatrix.txt**. If management is used, an additional file **res\_[name]\_rmatrix_managed.txt** will be saved with the coancestries of the population after management. Under the management mode, a file will be saved per replicate and generation (*eg.* **res\_[name]\_rmatrix_Rep1\_Gen2.txt** for replicate 1 and generation 2).

If the `save--tsv` is used, tabular results in the main output file for genetic (G) and allelic (A) cdiversity both within and between subpopulations are saved as TSV files. In addition, the partition of genetic diversity expressed as percentage of loss or gain of diversity after removal of each subpopulation is also saved in files with the tag "contribution":

- **res\_[name]\_within_G\*.tsv**
- **res\_[name]\_between_G\*.tsv**
- **res\_[name]\_contribution_G\*.tsv**
- **res\_[name]\_within_A\*.tsv**
- **res\_[name]\_between_A\*.tsv**
- **res\_[name]\_contribution_A\*.tsv**

If additionally, the option `--simMode` is used, a file with the contributions to a synthetic population are also saved in tabular form.

- **res\_[name]\_synthetic\*.tsv**

## Analyzing simulation results

Simulation results are saved in one to three files. In each file there is data for each generation and each replicate. This makes easy to perform customized statistics on those parameters and saves some time and memory. It is intended to analyse those files externally, for instance using [R](https://www.r-project.org/).

In the directory `R/` there is an example script (`metapopResumeSims.R`) that could be used (and customized) to obtain quick tables and plots from the simulation stats (saved in `res_[name]_simStats.tsv` and `res_[name]_p.tsv`). To run it you should have R installed in your system along with the following packages: dplyr, ggplot2, reshape2 and tidyr. Then you could execute the script by copying it into your working directory and running the following command: `Rscript metapopResumeSims.R`. The script will output 3 files:

 - **metapopsimsMeans.tsv** - A TSV file with the means across replicates
 - **metapopsimsSD.tsv** - A TSV file with the standard deviations across replicates
 - **Rplots.pdf** - A pdf file with some plots

## Assistance

Please read the full `README.md` file before to run the program.
If you have found a bug or do not understand an option about metapop2, please open a new issue (with the right tag and including as much information as possible) in the gitlab [issue tracker for metapop2](https://gitlab.com/elcortegano/metapop2/issues/) (It will require an account). Please avoid email reports or questions and check the [Frequently Asked Questions (FAQ) page in the wiki.](https://gitlab.com/elcortegano/metapop2/wikis/Frequently-Asked-Questions-(FAQs))


## Citation

If you use Metapop2 in your research, please cite:  

 - [López-Cortegano E., et al. (2019). Metapop2: Re-implementation of software for the analysis and management of subdivided populations using gene and allelic diversity. *Molecular Ecology Resources* 19(4): 1095-1100.](https://onlinelibrary.wiley.com/doi/abs/10.1111/1755-0998.13015)  

The first version of Metapop was published in:

 - [Pérez-Figueroa A., et al. (2009). METAPOP - A Software for the management and analysis of subdivided populations in conservation programs. *Conservation Genetics* 10: 1097-1099.](https://link.springer.com/article/10.1007/s10592-008-9718-7)  

## License

Metapop2. Analysis of gene and allelic diversity in subdivided populations, as well as a tool for management in conservation programs.

Copyright (C) 2020  Eugenio López-Cortegano, Andrés Pérez-Figueroa and Armando Caballero (Universidad de Vigo, Spain).

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
