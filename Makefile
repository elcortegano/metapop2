# Makefile for compile metapop2

############################################################
# VARIABLES
############################################################

EXE      = metapop
INSTALL ?= /usr/local
CC       = g++
VPATH    = src
DEBUG    = -pg -ggdb
CPPFLAGS = -O3 -std=c++11
LDFLAGS  = -flto
LIBS     = -lm
SOURCES := $(wildcard src/*.cpp)
H_LIBS   = alias.h operators.h
HEADERS  = $(SOURCES:.cpp=.h) $(H_LIBS)
OBJECTS  = $(SOURCES:.cpp=.o)

############################################################
# RULES
############################################################

all: $(EXE) $(SOURCES) $(HEADERS) $(OBJECTS)

$(EXE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@ $(LIBS)

$(OBJECTS): %.o: %.cpp %.h $(H_LIBS)
	$(CC) -c $(CPPFLAGS) $< -o $@

.PHONY: install
install:
	mkdir -p $(INSTALL)/bin
	install -m 557 $(EXE) $(INSTALL)/bin
	rm $(EXE)

.PHONY: clean
clean:
	rm -f $(EXE)
	rm -f src/*.o
	rm -f src/*.gch
