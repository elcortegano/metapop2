FROM alpine:3.4

LABEL version="v2.5.3"
LABEL description="Metapop2 provides an analysis of gene and allelic diversity in subdivided populations from molecular genotype or coancestry data as well as a tool for the management of genetic diversity in conservation programs."
LABEL maintainer="elcortegano@protonmail.com"

COPY src/* src/
COPY Makefile .
RUN apk update && apk add \
    gcc \
    g++ \
    make \
    git \
    && make \
    && cp metapop /bin \
    && apk del g++ make git

WORKDIR /tmp

ENTRYPOINT ["metapop"]
