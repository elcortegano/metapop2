#ifndef ALIAS_H
#define ALIAS_H

#include <vector>
#include <string>


// ============   ALIAS    ============

// Vectors
typedef unsigned short unshort;
typedef std::vector<double> vdou1D;
typedef std::vector<vdou1D> vdou2D;
typedef std::vector<vdou2D> vdou3D;
typedef std::vector<vdou3D> vdou4D;
typedef std::vector<int> 	vint1D;
typedef std::vector<vint1D> vint2D;
typedef std::vector<vint2D> vint3D;
typedef std::vector<vint3D> vint4D;
typedef std::vector<short> vshort1D;
typedef std::vector<vshort1D> vshort2D;
typedef std::vector<bool> vbool1D;
typedef unshort allele_t;
typedef std::vector<allele_t> allele1D;
typedef std::vector<allele1D> allele2D;
typedef std::vector<std::string> vstr1D;
typedef std::vector<vstr1D> vstr2D;

// Pointers
typedef std::vector<double*> vp_double;
typedef std::vector<vdou1D*> vp_vdou1D;
typedef std::vector<vdou2D*> vp_vdou2D;
typedef std::vector<std::vector<vdou2D*>> mp_vdou2D;
typedef std::vector<std::vector<vbool1D*>> mp_vbool1D;


// =====   INLINE FUNCTIONS   =====
template<typename T> 
inline void free_1D (std::vector<T>& vector) {
	std::size_t length (vector.size());
	if (length) {
		for (auto& i: vector) {
			delete i;
		}
	}
	vector.clear();
}

template<typename T> 
inline void free_2D (std::vector<T>& matrix) {
	std::size_t length (matrix.size());
	if (length) {
		for (auto& r: matrix) {
			for (auto& i: r) {
				delete i;
			}
		}
	}
	matrix.clear();
}

inline void free_vp_vdou2D (vp_vdou2D& vector) {
	std::size_t length (vector.size());
	if (length) {
		for (std::size_t i(0); i<length; ++i) {
			delete vector[i];
		}
	}
	vector.clear();
}

inline void free_mp_vdou2D (mp_vdou2D& matrix) {
	std::size_t length (matrix.size());
	if (length) {
		for (size_t i(0); i<length; ++i) {
			for (size_t j(0); j<matrix[i].size(); ++j) {
				delete matrix[i][j];
			}
		}
	}
	matrix.clear();
}


#endif
