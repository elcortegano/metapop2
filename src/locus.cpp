/* =======================================================================================
 *                                                                                       *
 *       Filename: locus.cpp                                                             *
 *                                                                                       *
 *    Description: This file defines the object class locus, implemented for use in      *
 *                 diploid and haploid organisms.                                        *
 *                                                                                       *
 *                  -> locus::locus ()                                                   *
 *                  -> locus::get_* ()                                                   *
 *                  -> locus::status ()                                                  *
 *                                                                                       *
 ======================================================================================= */

#include "locus.h"

/* ********************************************************************************************
 * locus::locus ()
 * --------------------------------------------------------------------------------------------
 * Class locus constructor.
 * ********************************************************************************************/
locus::locus(const allele1D& as) {
	for (auto a: as) alleles.push_back(a);
	this->check();
}

locus::locus(const allele_t& a1, const allele_t& a2) {
	alleles.push_back(a1);
	alleles.push_back(a2);
	this->check();
}

/* ********************************************************************************************
 * locus::getAlleles ()
 * --------------------------------------------------------------------------------------------
 * Return information of alleles in the locus.
 * ********************************************************************************************/
allele_t locus::getA (const std::size_t& n) const {
	return alleles[n];
}

allele_t locus::getNAllels (const allele_t& a) const {
	unshort nalel (0);
	for (auto ai: alleles) if (a==ai) ++nalel;
	return nalel;
}

/* ********************************************************************************************
 * check & status ()
 * --------------------------------------------------------------------------------------------
 * Checks if the locus has any allele defined.
 * ********************************************************************************************/
void locus::check () {
	exist = true;
	for (auto a: alleles) {
		if (!a) {
			exist = false;
			break;
		}
	}
}

bool locus::status() const {
	return exist;
}
