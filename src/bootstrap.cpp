/* =======================================================================================
 *                                                                                       *
 *       Filename: boostrap.cpp                                                          *
 *                                                                                       *
 *    Description: This file contaings functions to create boostrap calculation and      *
 *                 return results corresponding to the desired significance level        *
 *                                                                                       *
 *                  -> calculations_boot ()                                              *
 *                  -> set_ci ()                                                         *
 *                                                                                       *
 ======================================================================================= */

#include "bootstrap.h"


/* ********************************************************************************************
 * Calculations_boot::Calculations_boot ()
 * --------------------------------------------------------------------------------------------
 * Class Calculations_boot constructor. Creates a vector of Calculations with size equal to the
 * number of bootstrap samples.
 * ********************************************************************************************/
Calculations_boot::Calculations_boot ( MTPConfigFile& config_file, const Metapop& metapop) : Calculations () {

	// Create sample metapopulations and save calculations
	if (DEBUG) std::cout << "CALCULATIONS BOOT:" << std::endl;
	std::cout << std::flush;
	int nboot (config_file.get_bt_n());
	n_subpops = metapop.get_n_subpops ();
	if (nboot) {
		tmp_fextra = vector<vector<vector<double>>>(nboot);
		tmp_s = vector<vector<double>>(nboot);
		tmp_d = vector<vector<vector<double>>>(nboot);
		double alpha (config_file.get_bt_alpha());
		for (int i(0); i<nboot; ++i) {
			std::cout << "\rBoostrap replicate: " << i+1 << " / " << nboot;
			std::cout << std::flush;
			Metapop mtp_boot (metapop);
			mtp_boot.compute_diversity();

			vp_vdou1D tmp_fextra_i = mtp_boot.get_f().t_par;
			for (const auto& f_i: tmp_fextra_i) tmp_fextra[i].push_back(*f_i);
			
			vp_double tmp_s_i = mtp_boot.get_s().t_par;
			for (const auto& s_i: tmp_s_i) tmp_s[i].push_back(*s_i);
			
			vp_vdou1D tmp_d_i = mtp_boot.get_d().t_par;
			for (const auto& d_i: tmp_d_i) tmp_d[i].push_back(*d_i);

			tmp_u_alleles.push_back(mtp_boot.get_unique_alleles());
			vp_vdou1D fextra (mtp_boot.get_f().t_par);
			Calculations calc_boot (config_file, mtp_boot);
			calculations_boot.push_back(calc_boot);
		}

		std::cout << std::endl;
		if (DEBUG) std::cout << "---------------------------------------" << std::endl;

		// Set Confidence Interval results
		this->set_ci (alpha);

		// Free calculations vector
		calculations_boot.clear();
	}
}

/* ********************************************************************************************
 * Calculations_boot::Calculations_boot ()
 * --------------------------------------------------------------------------------------------
 * It will declare two Calculation objects that represent the condifence intervals at the level
 * of significance chosen.
 * ********************************************************************************************/
void Calculations_boot::set_ci (double alpha) {

	std::size_t N_boot (calculations_boot.size());
	if (alpha <= 0.0) print_error ("Set a significance level for bootstrap higher than zero.");

	// Get vectors of relevant parameters
	vdou2D tmp_F;
	vdou2D tmp_G;
	vdou2D tmp_Fis_pop;

	vdou3D tmp_D_is;
	vdou3D tmp_Fst_b;

	vdou1D boot_HS;
	vdou1D boot_DG;
	vdou1D boot_HT;

	vdou1D boot_GDwi;
	vdou1D boot_GDbi;
	vdou1D boot_GDws;
	vdou1D boot_GDbs;

	vdou1D boot_Fis;
	vdou1D boot_Fst;
	vdou1D boot_Fit;

	vdou2D tmp_con_as;
	vdou2D tmp_app;

	vdou3D tmp_pop_as;
	vdou3D tmp_pop_da;

	vdou1D boot_AS;
	vdou1D boot_DA;
	vdou1D boot_AT;
	vdou1D boot_AST;

	vdou2D tmp_rem_HS;
	vdou2D tmp_rem_DG;
	vdou2D tmp_rem_HT;
	vdou2D tmp_rem_AS;
	vdou2D tmp_rem_DA;
	vdou2D tmp_rem_AT;

	for (const auto& calc: calculations_boot) {
		tmp_F.push_back(calc.get_F());
		tmp_G.push_back(calc.get_G());
		tmp_Fis_pop.push_back(calc.get_Fis_pop());
		tmp_D_is.push_back(calc.get_D().is);
		tmp_Fst_b.push_back(calc.get_Fst_b());
		boot_HS.push_back(1.0 - calc.get_bar_n().f);
		boot_DG.push_back(calc.get_bar_n().f - calc.get_tot_n().f);
		boot_HT.push_back(1.0 - calc.get_tot_n().f);
		boot_GDwi.push_back((1.0-calc.get_Gbar_n())*(1.0-calc.get_F_n().st));
		boot_GDbi.push_back(calc.get_Gbar_n()*(1.0-calc.get_F_n().st));
		boot_GDws.push_back(1.0-calc.get_F_n().st);
		boot_GDbs.push_back(calc.get_F_n().st);
		boot_Fis.push_back(calc.get_F_n().is);
		boot_Fst.push_back(calc.get_F_n().st);
		boot_Fit.push_back(calc.get_F_n().it);

		tmp_con_as.push_back(calc.get_con_a().as);
		tmp_app.push_back(calc.get_Kpop ());
		tmp_pop_as.push_back(calc.get_pop_a().as);
		tmp_pop_da.push_back(calc.get_pop_a().da);
		boot_AS.push_back(calc.get_con_n().as);
		boot_DA.push_back(calc.get_con_n().da);
		boot_AT.push_back(calc.get_con_n().as + calc.get_con_n().da);
		boot_AST.push_back(calc.get_Ast());

		G_parameters <vdou1D> totR_n (calc.get_totR_n());
		G_parameters <vdou1D> barR_n (calc.get_barR_n());
		G_parameters <double> tot_n (calc.get_tot_n());
		G_parameters <double> bar_n (calc.get_bar_n());
		A_parameters <double> A (calc.get_A());
		A_parameters <vdou1D> rem_ (calc.get_rem());
		vdou1D boot_rem_HS;
		vdou1D boot_rem_DG;
		vdou1D boot_rem_HT;
		vdou1D boot_rem_AS;
		vdou1D boot_rem_DA;
		vdou1D boot_rem_AT;
		for (std::size_t i(0); i<n_subpops; ++i) {
			double gdt (1.0 - totR_n.f[i]);
			double gdwi (-(((1.0 - barR_n.s[i]) - (1.0 - bar_n.s)) / gdt * 100));
			double gdbi (-((barR_n.s[i] - barR_n.f[i]) - (bar_n.s - bar_n.f)) / gdt * 100);
			boot_rem_HS.push_back(gdwi + gdbi);
			boot_rem_DG.push_back(-((barR_n.f[i] - totR_n.f[i]) - (bar_n.f - tot_n.f)) / gdt * 100);
			boot_rem_HT.push_back(-((1.0 - totR_n.f[i]) - (1.0 - tot_n.f)) / gdt * 100);
			boot_rem_AS.push_back((-(rem_.as[i]-A.as)/(A.as+A.da) *100));
			boot_rem_DA.push_back((-(rem_.da[i]-A.da)/(A.as+A.da) * 100));
			boot_rem_AT.push_back((-((rem_.as[i]+rem_.da[i])-(A.as+A.da))/(A.as+A.da) *100));
		}
		tmp_rem_HS.push_back(boot_rem_HS);
		tmp_rem_DG.push_back(boot_rem_DG);
		tmp_rem_HT.push_back(boot_rem_HT);
		tmp_rem_AS.push_back(boot_rem_AS);
		tmp_rem_DA.push_back(boot_rem_DA);
		tmp_rem_AT.push_back(boot_rem_AT);
	}

	// Set CIs
	std::size_t low_index (round(alpha*(double)N_boot));
	std::size_t high_index (N_boot-low_index-1);
	if (low_index > high_index) print_error ("Significance level for boostrap is too high.");

	// Process (and sort) high dimensional data
	fextra = vector<vector<CI<double>>>(n_subpops, vector<CI<double>>(n_subpops));
	d = vector<vector<CI<double>>>(n_subpops, vector<CI<double>>(n_subpops));
	s = vector<CI<double>>(n_subpops);
	u_alleles = vector<CI<double>>(n_subpops);
	F = vector<CI<double>>(n_subpops);
	G = vector<CI<double>>(n_subpops);
	Fis_pop = vector<CI<double>>(n_subpops);
	D_is = vector<vector<CI<double>>>(n_subpops, vector<CI<double>>(n_subpops));
	Fst_b = vector<vector<CI<double>>>(n_subpops, vector<CI<double>>(n_subpops));
	con_as = vector<CI<double>>(n_subpops);
	app = vector<CI<double>>(n_subpops);
	pop_as = vector<vector<CI<double>>>(n_subpops, vector<CI<double>>(n_subpops));
	pop_da = vector<vector<CI<double>>>(n_subpops, vector<CI<double>>(n_subpops));
	vdou1D boot_avg_fextra = vector<double>(N_boot);
	vdou1D boot_avg_s = vector<double>(N_boot);
	vdou1D boot_avg_d = vector<double>(N_boot);
	vdou1D boot_avg_F = vector<double>(N_boot);
	vdou1D boot_avg_G = vector<double>(N_boot);
	vdou1D boot_avg_Fis_pop = vector<double>(N_boot);
	vdou1D boot_avg_u_alleles = vector<double>(N_boot);
	vdou1D boot_avg_con_as = vector<double>(N_boot);
	vdou1D boot_avg_app = vector<double>(N_boot);
	rem_HS = vector<CI<double>>(n_subpops);
	rem_DG = vector<CI<double>>(n_subpops);
	rem_HT = vector<CI<double>>(n_subpops);
	rem_AS = vector<CI<double>>(n_subpops);
	rem_DA = vector<CI<double>>(n_subpops);
	rem_AT = vector<CI<double>>(n_subpops);
	for (std::size_t i(0); i < n_subpops; ++i) {
		vdou1D boot_s;
		vdou1D boot_F;
		vdou1D boot_G;
		vdou1D boot_Fis_pop;
		vdou1D boot_u_alleles;
		vdou1D boot_con_as;
		vdou1D boot_app;
		vdou1D boot_rem_HS;
		vdou1D boot_rem_DG;
		vdou1D boot_rem_HT;
		vdou1D boot_rem_AS;
		vdou1D boot_rem_DA;
		vdou1D boot_rem_AT;
		for (std::size_t j(0); j<tmp_fextra.size(); ++j) {
			boot_avg_fextra[j] += tmp_fextra[j][i][i];
		}
		for (std::size_t j(0); j<tmp_s.size(); ++j) {
			boot_s.push_back(tmp_s[j][i]);
			boot_avg_s[j] += tmp_s[j][i];
		}
		for (std::size_t j(0); j<tmp_d.size(); ++j) {
			boot_avg_d[j] += tmp_d[j][i][i];
		}
		for (std::size_t j(0); j<tmp_F.size(); ++j) {
			boot_F.push_back(tmp_F[j][i]);
			boot_avg_F[j] += tmp_F[j][i];
		}
		for (std::size_t j(0); j<tmp_G.size(); ++j) {
			boot_G.push_back(tmp_G[j][i]);
			boot_avg_G[j] += tmp_G[j][i];
		}
		for (std::size_t j(0); j<tmp_Fis_pop.size(); ++j) {
			boot_Fis_pop.push_back(tmp_Fis_pop[j][i]);
			boot_avg_Fis_pop[j] += tmp_Fis_pop[j][i];
		}
		for (std::size_t j(0); j<tmp_u_alleles.size(); ++j) {
			boot_u_alleles.push_back(tmp_u_alleles[j][i]);
			boot_avg_u_alleles[j] += tmp_u_alleles[j][i];
		}
		for (std::size_t j(0); j<tmp_con_as.size(); ++j) {
			boot_con_as.push_back(tmp_con_as[j][i]);
			boot_avg_con_as[j] += tmp_con_as[j][i];
		}
		for (std::size_t j(0); j<tmp_app.size(); ++j) {
			boot_app.push_back(tmp_app[j][i]);
			boot_avg_app[j] += tmp_app[j][i];
		}
		for (const auto& b: tmp_rem_HS) boot_rem_HS.push_back(b[i]);
		for (const auto& b: tmp_rem_DG) boot_rem_DG.push_back(b[i]);
		for (const auto& b: tmp_rem_HT) boot_rem_HT.push_back(b[i]);
		for (const auto& b: tmp_rem_AS) boot_rem_AS.push_back(b[i]);
		for (const auto& b: tmp_rem_DA) boot_rem_DA.push_back(b[i]);
		for (const auto& b: tmp_rem_AT) boot_rem_AT.push_back(b[i]);
		sort (boot_s.begin(), boot_s.end(), sort_vdou1D());
		sort (boot_F.begin(), boot_F.end(), sort_vdou1D());
		sort (boot_G.begin(), boot_G.end(), sort_vdou1D());
		sort (boot_Fis_pop.begin(), boot_Fis_pop.end(), sort_vdou1D());
		sort (boot_u_alleles.begin(), boot_u_alleles.end(), sort_vdou1D());
		sort (boot_con_as.begin(), boot_con_as.end(), sort_vdou1D());
		sort (boot_app.begin(), boot_app.end(), sort_vdou1D());
		sort (boot_rem_HS.begin(), boot_rem_HS.end(), sort_vdou1D());
		sort (boot_rem_DG.begin(), boot_rem_DG.end(), sort_vdou1D());
		sort (boot_rem_HT.begin(), boot_rem_HT.end(), sort_vdou1D());
		sort (boot_rem_AS.begin(), boot_rem_AS.end(), sort_vdou1D());
		sort (boot_rem_DA.begin(), boot_rem_DA.end(), sort_vdou1D());
		sort (boot_rem_AT.begin(), boot_rem_AT.end(), sort_vdou1D());
		s[i].low = boot_s[low_index]; s[i].high = boot_s[high_index];
		F[i].low = boot_F[low_index]; F[i].high = boot_F[high_index];
		G[i].low = boot_G[low_index]; G[i].high = boot_G[high_index];
		Fis_pop[i].low = boot_Fis_pop[low_index]; Fis_pop[i].high = boot_Fis_pop[high_index];
		u_alleles[i].low = boot_u_alleles[low_index]; u_alleles[i].high = boot_u_alleles[high_index];
		con_as[i].low = boot_con_as[low_index]; con_as[i].high = boot_con_as[high_index];
		app[i].low = boot_app[low_index]; app[i].high = boot_app[high_index];
		rem_HS[i].low = boot_rem_HS[low_index]; rem_HS[i].high = boot_rem_HS[high_index];
		rem_DG[i].low = boot_rem_DG[low_index]; rem_DG[i].high = boot_rem_DG[high_index];
		rem_HT[i].low = boot_rem_HT[low_index]; rem_HT[i].high = boot_rem_HT[high_index];
		rem_AS[i].low = boot_rem_AS[low_index]; rem_AS[i].high = boot_rem_AS[high_index];
		rem_DA[i].low = boot_rem_DA[low_index]; rem_DA[i].high = boot_rem_DA[high_index];
		rem_AT[i].low = boot_rem_AT[low_index]; rem_AT[i].high = boot_rem_AT[high_index];
		for (std::size_t j(0); j < n_subpops; ++j) {
			vdou1D boot_fextra;
			vdou1D boot_d;
			vdou1D boot_D_is;
			vdou1D boot_Fst_b;
			vdou1D boot_pop_as;
			vdou1D boot_pop_da;
			for (const auto& b: tmp_fextra) boot_fextra.push_back(b[i][j]);
			for (const auto& b: tmp_d) boot_d.push_back(b[i][j]);
			for (const auto& b: tmp_D_is) boot_D_is.push_back(b[i][j]);
			for (const auto& b: tmp_Fst_b) boot_Fst_b.push_back(b[i][j]);
			for (const auto& b: tmp_pop_as) boot_pop_as.push_back(b[i][j]);
			for (const auto& b: tmp_pop_da) boot_pop_da.push_back(b[i][j]);
			sort (boot_fextra.begin(), boot_fextra.end(), sort_vdou1D());
			sort (boot_d.begin(), boot_d.end(), sort_vdou1D());
			sort (boot_D_is.begin(), boot_D_is.end(), sort_vdou1D());
			sort (boot_Fst_b.begin(), boot_Fst_b.end(), sort_vdou1D());
			sort (boot_pop_as.begin(), boot_pop_as.end(), sort_vdou1D());
			sort (boot_pop_da.begin(), boot_pop_da.end(), sort_vdou1D());
			fextra[i][j].low = boot_fextra[low_index]; fextra[i][j].high = boot_fextra[high_index];
			d[i][j].low = boot_d[low_index]; d[i][j].high = boot_d[high_index];
			D_is[i][j].low = boot_D_is[low_index]; D_is[i][j].high = boot_D_is[high_index];
			Fst_b[i][j].low = boot_Fst_b[low_index]; Fst_b[i][j].high = boot_Fst_b[high_index];
			pop_as[i][j].low = boot_pop_as[low_index]; pop_as[i][j].high = boot_pop_as[high_index];
			pop_da[i][j].low = boot_pop_da[low_index]; pop_da[i][j].high = boot_pop_da[high_index];
		}
	}

	// Average samples
	for (std::size_t i(0); i<N_boot; ++i) {
		boot_avg_fextra[i] /= n_subpops;
		boot_avg_s[i] /= n_subpops;
		boot_avg_d[i] /= n_subpops;
		boot_avg_F[i] /= n_subpops;
		boot_avg_G[i] /= n_subpops;
		boot_avg_Fis_pop[i] /= n_subpops;
		boot_avg_u_alleles[i] /= n_subpops;
		boot_avg_con_as[i] /= n_subpops;
		boot_avg_app[i] /= n_subpops;
	}
	sort (boot_avg_fextra.begin(), boot_avg_fextra.end(), sort_vdou1D());
	sort (boot_avg_s.begin(), boot_avg_s.end(), sort_vdou1D());
	sort (boot_avg_d.begin(), boot_avg_d.end(), sort_vdou1D());
	sort (boot_avg_F.begin(), boot_avg_F.end(), sort_vdou1D());
	sort (boot_avg_G.begin(), boot_avg_G.end(), sort_vdou1D());
	sort (boot_avg_Fis_pop.begin(), boot_avg_Fis_pop.end(), sort_vdou1D());
	sort (boot_avg_con_as.begin(), boot_avg_con_as.end(), sort_vdou1D());
	sort (boot_avg_app.begin(), boot_avg_app.end(), sort_vdou1D());
	sort (boot_avg_u_alleles.begin(), boot_avg_u_alleles.end(), sort_vdou1D());
	avg_fextra_ii.low = boot_avg_fextra[low_index]; avg_fextra_ii.high = boot_avg_fextra[high_index];
	avg_s.low = boot_avg_s[low_index]; avg_s.high = boot_avg_s[high_index];
	avg_d_ii.low = boot_avg_d[low_index]; avg_d_ii.high = boot_avg_d[high_index];
	avg_F.low = boot_avg_F[low_index]; avg_F.high = boot_avg_F[high_index];
	avg_G.low = boot_avg_G[low_index]; avg_G.high = boot_avg_G[high_index];
	avg_Fis_pop.low = boot_avg_Fis_pop[low_index]; avg_Fis_pop.high = boot_avg_Fis_pop[high_index];
	avg_con_as.low = boot_avg_con_as[low_index]; avg_con_as.high = boot_avg_con_as[high_index];
	avg_app.low = boot_avg_app[low_index]; avg_app.high = boot_avg_app[high_index];
	avg_u_alleles.low = boot_avg_u_alleles[low_index]; avg_u_alleles.high = boot_avg_u_alleles[high_index];

	// Free calculations
	calculations_boot.clear();

	// Sort vectors
	sort (boot_HS.begin(), boot_HS.end(), sort_vdou1D());
	sort (boot_DG.begin(), boot_DG.end(), sort_vdou1D());
	sort (boot_HT.begin(), boot_HT.end(), sort_vdou1D());
	sort (boot_GDwi.begin(), boot_GDwi.end(), sort_vdou1D());
	sort (boot_GDbi.begin(), boot_GDbi.end(), sort_vdou1D());
	sort (boot_GDws.begin(), boot_GDws.end(), sort_vdou1D());
	sort (boot_GDbs.begin(), boot_GDbs.end(), sort_vdou1D());
	sort (boot_Fis.begin(), boot_Fis.end(), sort_vdou1D());
	sort (boot_Fst.begin(), boot_Fst.end(), sort_vdou1D());
	sort (boot_Fit.begin(), boot_Fit.end(), sort_vdou1D());
	sort (boot_AS.begin(), boot_AS.end(), sort_vdou1D());
	sort (boot_DA.begin(), boot_DA.end(), sort_vdou1D());
	sort (boot_AT.begin(), boot_AT.end(), sort_vdou1D());
	sort (boot_AST.begin(), boot_AST.end(), sort_vdou1D());

	HS.low = boot_HS[low_index]; HS.high = boot_HS[high_index];
	DG.low = boot_DG[low_index]; DG.high = boot_DG[high_index];
	HT.low = boot_HT[low_index]; HT.high = boot_HT[high_index];
	GDwi.low = boot_GDwi[low_index]; GDwi.high = boot_GDwi[high_index];
	GDbi.low = boot_GDbi[low_index]; GDbi.high = boot_GDbi[high_index];
	GDws.low = boot_GDws[low_index]; GDws.high = boot_GDws[high_index];
	GDbs.low = boot_GDbs[low_index]; GDbs.high = boot_GDbs[high_index];
	Fis.low = boot_Fis[low_index]; Fis.high = boot_Fis[high_index];
	Fst.low = boot_Fst[low_index]; Fst.high = boot_Fst[high_index];
	Fit.low = boot_Fit[low_index]; Fit.high = boot_Fit[high_index];
	AS.low = boot_AS[low_index]; AS.high = boot_AS[high_index];
	DA.low = boot_DA[low_index]; DA.high = boot_DA[high_index];
	AT.low = boot_AT[low_index]; AT.high = boot_AT[high_index];
	AST.low = boot_AST[low_index]; AST.high = boot_AST[high_index];

}
