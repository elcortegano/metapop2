/* =======================================================================================
 *                                                                                       *
 *       Filename:  management.cpp                                                       *
 *                                                                                       *
 *    Description: This file defines functions for the management of individuals,        *
 *                 including functions to use the Simulated Annealing algorithm.         *
 *                                                                                       *
 *                  -> management ()                                                     *
 *                  -> simulated_annealing ()                                            *
 *                  -> validSol ()                                                       *
 *                  -> evaluate ()                                                       *
 *                  -> evaluate_genetic_div ()                                           *
 *                  -> evaluate_allelic_div ()                                           *
 *                  -> evaluate_allelic_num ()                                           *
 *                                                                                       *
 ======================================================================================= */

#include "management.h"


void management (const MTPConfigFile& config, Metapop& metapop, const Calculations& calc, std::string output, int simR, int simT) {

	if (DEBUG) std::cout << "MANAGEMENT (SIMULATED ANNEALING):" << std::endl;
	else std::cout << "Computing management..." << std::endl;
	std::cout << std::flush;

	/* CONFIG VARIABLES */
	std::vector<population> deme (metapop.get_deme());
	double weight_F_isol (config.get_weight_F_isol());
	offspring = metapop.get_progeny();
	isol = config.get_isol();
	monogamy = config.get_monogamy();
	tasa = config.get_tasa();
	simMode = config.get_simMode();
	manage = config.get_manage();
	nmigra = config.get_nmigra();
	lambda = config.get_lambda();
	sa_t = config.get_SA_t();
	sa_k = config.get_SA_k();
	sa_steps = config.get_SA_steps();
	convergence = config.get_convergence();
	int maxoff = config.get_maxoff();
	if (isol) {
		lambda = weight_F_isol;
		nmigra = 0; // In OMPG, first the method is run as isolated populations, and then migration happens
	}

	/* LOCAL VARIABLES */
	pop = metapop.get_n_subpops();
	totalN = metapop.get_N(false);
	fmol = metapop.get_fmol();
	restricted = metapop.get_use_restricted_mates();
	forbidden = metapop.get_use_forbidden_mates();
	NperSex.clear();
	for (std::size_t i(0); i<pop; ++i) {
		vint1D row_npersex;
		row_npersex.push_back(deme[i].getNperSex(0));
		row_npersex.push_back(deme[i].getNperSex(1));
		NperSex.push_back(row_npersex);
	}
	struc = metapop.get_struc();
	code = metapop.get_alleles_per_locus();
	loci = code.size();

	// Maximum desired inbreeding coefficient
	double minb (0.0);
	for (std::size_t i (0); i < totalN; i++) {
		minb += (fmol[i][i]);
	}
	minb = ((minb * 2.0 / totalN) - 1.0);
	double npar (minb * (1.0 - tasa) + tasa);

	// Initialize padopt
	vint3D solOpt(totalN, vint2D (pop, vint1D(2)));
	vint2D padopt;
	if (restricted) padopt = metapop.get_padopt();
	if (forbidden) forbid_mates = metapop.get_forbidden();

	// Maximum number of offspring (limit)
	vshort2D limit;
	int males (0), females (0);
	for (std::size_t i (0); i < pop; i++) {
		females += NperSex[i][0];
		males += NperSex[i][1];
	}
	for (std::size_t i (0); i < totalN; i++) {
		vshort1D row;
		row.push_back(females);
		row.push_back(males);
		limit.push_back(row);
	}

	if (restricted) limit = metapop.get_limit();
	if (maxoff) {
		for (std::size_t i(0); i<limit.size(); ++i) {
			if (limit[i][0] > maxoff) limit[i][0] = maxoff;
			if (limit[i][1] > maxoff) limit[i][1] = maxoff;
		}
	}

	// Run Simulated Annealing algorithm
	double energyOpt = simulated_annealing(deme, solOpt, padopt, limit, npar);

	// Return management
	printSolution (config, metapop, calc, output, solOpt, fmol, minb, padopt, simR, simT, energyOpt);

	if (!DEBUG) std::cout << "Computing management...done!" << std::endl;
}

double simulated_annealing (const std::vector<population>& deme, vint3D& solOpt, vint2D& padopt, const vshort2D& limit, double npar) {

	if (DEBUG) std::cout << "\rSetting initial random solution...";

	/* MANAGEMENT-TYPE SPECIFIC VARIABLES */
	if (manage == ALLELIC || manage == N_ALLELES ) {
		initialize_allelic (deme);
	}

	/* SA VARIABLES */
	double energyOpt (0.0), energy (0.0), energyA (0.0);;
	std::size_t energyMax (100000000);
	std::size_t count (0);
	vint3D sol(totalN, vint2D (pop, vint1D(2, 0)));
	vint3D solA(totalN, vint2D (pop, vint1D(2, 0)));

	if (!restricted) {
		for (std::size_t i (0); i < pop; i++) {
			padopt.push_back(vint1D(NperSex[i][0]));
		}
	}

	vint2D vpad (padopt);
	vint2D pad (padopt);

	// RANDOM (INT) NUMBER GENERATORS
	std::uniform_int_distribution<int> sample_bin (0,1);
	std::uniform_int_distribution<int> sample_pop (0,pop-1);
	std::vector<std::vector<std::uniform_int_distribution<int>>> sample_vec_npersex;
	for (std::size_t i(0); i<pop; ++i) {
		std::vector<std::uniform_int_distribution<int>> row_dist;
		row_dist.push_back(std::uniform_int_distribution<int> (0,NperSex[i][0]-1));
		row_dist.push_back(std::uniform_int_distribution<int> (0,NperSex[i][1]-1));
		sample_vec_npersex.push_back(row_dist);
	}

	// RANDOM MATES
	if (!restricted) {
		do {
			++count;
			for (std::size_t i (0); i < pop; i++) {
				std::size_t n_fem_i (NperSex[i][0]);
				std::size_t n_mal_i (NperSex[i][1]);
				for (std::size_t j (0); j <n_fem_i; j++) {
					vpad[i][j] = 0;
				}
				for (std::size_t j (0); j < n_fem_i; j++) {
					if (j < n_mal_i) {
						vpad[i][j] = j;
					} else {
						vpad[i][j] = sample_vec_npersex[i][1](random_generator);
					}
					if (forbidden) {
						while (check_forbid(i,j,vpad, forbid_mates)) {
							vpad[i][j] = sample_vec_npersex[i][1](random_generator);
						}
					}
				}
				for (std::size_t j (0); j < n_fem_i; j++) {
					do {
						int j2 (sample_vec_npersex[i][0](random_generator));
						int temp = vpad[i][j2];
						vpad[i][j2] = vpad[i][j];
						vpad[i][j] = temp;
					} while (forbidden & check_forbid(i,j,vpad, forbid_mates));
				}
			}
			if (count > energyMax) print_error("Unable to initialize mating matrix!! Too many forbidden pairs?");
		} while (!validSol(sol, vpad));
	}

	// RANDOM INITIAL SOLUTION
	for (std::size_t i (0); i < pop; i++) {
		for (std::size_t j (0); j < 2; j++) {
			for (int l (0); l < offspring[i][j]; l++) {
				int sum (0), posm (0), posp (0);
				std::size_t try_r(0);
				do {
					sum = 0;
					int m (sample_vec_npersex[i][0](random_generator));
					posm = struc[i][0] + m;
					posp = struc[i][1] + vpad[i][m];
					for (std::size_t h (0); h < pop; h++) {
						sum += sol[posm][h][j];
					}
					++try_r;
					if (try_r > energyMax) print_error("Unable to find contributions!! Too few females?");
				} while (sum == limit[posm][j]);
				sol[posm][i][j]++;
				sol[posp][i][j]++;
			}
		}
	}

	initialize_migrants(sol);
	calculate_migrants(sol);

	if (DEBUG) {
		std::cout << "\rSetting initial random solution...done";
		std::cout << std::endl;
	}

	searchpop = 0;
	energy = evaluate(sol, vpad, npar);
	energyOpt = energy;
	solOpt = sol;
	padopt = vpad;
	int max_ivig (5000);

	// Loop over isolated populations
	do {
		double kt (sa_t / sa_k);
		bool pass (false);
		int ivig (max_ivig);
		if (DEBUG) std::cout << "Simulated annealing steps" << std::endl;
		if (!simMode || DEBUG) std::cout << "Step\tAcpt\tChanges\tStepSol\tBestSol" << std::endl;

		for (std::size_t niv (0); niv < sa_steps; niv++) {

			std::size_t camb(1 + (10 * ivig / max_ivig));
			kt *= sa_k;
			ivig = 0;
			std::deque<double> last_energy;

			for (int rep (0); rep < max_ivig; ++rep) {

				solA = sol;
				pad = vpad;
				int n (0), posm(0), pm (0), ph (0), sh (0);
				initialize_migrants(sol);

				for (std::size_t r (0); r < camb; r++) {

					//female (and mate) who loose a offspring
					int control_sum (0);
					do { //Choose population
						control_sum = 0;
						if (isol) {
							pm = searchpop;
						} else {
							pm = sample_pop(random_generator);
						}
						for (int j (struc[pm][0]); j < struc[pm][1]; j++) {
							for (std::size_t l (0); l < pop; l++) {
								control_sum += solA[j][l][0] + solA[j][l][1];
								if (control_sum) break;
							}
							if(control_sum) break;
						}
					} while (!control_sum);

					do { //Choose female in that subpop
						control_sum = 0;
						n = sample_vec_npersex[pm][0](random_generator);
						posm = n + struc[pm][0];
						for (std::size_t l (0); l < pop; l++) {
							control_sum += solA[posm][l][0] + solA[posm][l][1];
							if (control_sum) break;
						}
					} while (!control_sum);

					do { //Choose contributed subpop
						if (isol) {
							ph = pm;
						} else {
							ph = sample_pop(random_generator);
						}
					} while ((solA[posm][ph][0] + solA[posm][ph][1]) == 0);

					sh = sample_bin(random_generator);
					if (!solA[posm][ph][sh]) {
						if (sh) sh = 0;
						else sh = 1;
					}
					solA[posm][ph][sh]--;
					migra--;

					//the partner
					int posp (struc[pm][1] + pad[pm][n]);
					solA[posp][ph][sh]--;
					migra--;
                    
					// Si se acaban las contribuciones de una hembra, desligamos al macho
					control_sum = 0;

					for (std::size_t l (0); l < pop; l++) {
						control_sum += solA[posm][l][0] + solA[posm][l][1];
						if (control_sum) break;
					}
					if (!control_sum & !restricted) {
						pad[pm][n] = -1;
					}

					//choosing pair to increase contributions
					int l, antposm (posm);
					control_sum = 0;
					std::size_t over_rest (0);

					do {
						bool impossible (true);
						std::size_t over (0);
						do {
							if (!isol) {
								pm = sample_pop(random_generator);
							}

							// Evaluando si la unica hembra elegible de esa pop es posm, si lo es busca otra pop;
							for (int i (struc[pm][0]); i < struc[pm][1]; i++) {
								if ((i != antposm)) {
									impossible = false;
									break;
								}
							}
							++over;
							if (over > energyMax) print_error("Unable to find contributions!!");
						} while (impossible);

						do {
							l = sample_vec_npersex[pm][0](random_generator);
						} while (antposm == (l + struc[pm][0]));

						posm = (l + struc[pm][0]);
						control_sum = 0;
						for (std::size_t i (0); i < pop; i++) {
							control_sum += solA[posm][i][sh];
						}

						++over_rest;
						if (restricted && over_rest > energyMax) print_error("Unable to find contributions. Too few offspring in the restriction matrix?");
					} while (control_sum == limit[posm][sh]);

					if (pad[pm][l] == -1 && !restricted) {
						pad[pm][l] = sample_vec_npersex[pm][1](random_generator);
					}
					solA[posm][ph][sh]++;
					migra++;
					posp = struc[pm][1] + pad[pm][l];
					solA[posp][ph][sh]++;
					migra++;
				}

				if (validSol (solA, pad)) {

					energyA = evaluate(solA, pad, npar);

					//Annealing
					int ch (0);
					double delta (max((energyA - energy), 0.0));
					double omega (exp((-delta) / kt));
					if (omega >= 1.0) {
						ch = 1;
						if (energyA < energyOpt) {
							energyOpt = energyA;
							solOpt = solA;
							padopt = pad;
						}
						if ( manage==RANDOM && energyOpt == 0 ){
							pass = true;
							break;
						} else if ( manage==GENETIC && energyOpt == 0 ) { //Allelic and Nalleles could be below 0
							if (!simMode) std::cout << "Absolute optimun found!!! (energy=0)" << std::endl;
							if (!isol) return (0.0);
							else {
								pass = true;
								break;
							}
						}
					} else if (omega > 0.0) {
						double u (random_dist(random_generator));
						if (u < omega) {
							ch = 1;
						}
					}
					if (ch == 1) {
						sol = solA;
						vpad = pad;
						energy = energyA;
						ivig++;
					}
				}

				last_energy.push_back(energyOpt);
				if (ivig && last_energy.size() >= convergence) {
					if (!variation(last_energy)) {
						solOpt = sol;
						padopt = vpad;
						energyOpt = energy;
						break;
					}
				}
				if (last_energy.size() == convergence) last_energy.pop_front();
			}

			if (!simMode || DEBUG) std::cout << (niv + 1) << "\t" << ivig << "\t" << camb << "\t" << energy << "\t" << energyOpt << std::endl;
			if (pass) break;

			if (ivig == 0) {
				if (!simMode) {
					std::cout << "It's enough!!! (accepted changes = 0)" << std::endl;
				}
				break;
			}
		}
		searchpop++;

	} while ((isol) && (searchpop < pop));

	if (DEBUG) {
		std::cout << "Management done" << std::endl;
		std::cout << std::endl;
		std::cout << "---------------------------------------" << std::endl;
	}

	return energyOpt;
}

bool validSol (const vint3D& c, const vint2D& pad) {

	monogamy_penalty_count = 0;
	forbidden_penalty_count = 0;

	// Initialize migra
	calculate_migrants(c);
	if (migra > nmigra) return false;

	if (monogamy) {
		for (std::size_t k (0); k < pop; k++) {
			std::size_t n_fem_k (NperSex[k][0]);
			std::size_t n_mal_k (NperSex[k][1]);
			vint1D count(n_mal_k);
			for (std::size_t i (0); i < n_fem_k; i++) {
				if (pad[k][i] >= 0) {
					++count[pad[k][i]];
				}
			}
			for (std::size_t i (0); i < n_mal_k; i++) {
				if (count[i] > 1) {
					++monogamy_penalty_count;
					return false;
				}
			}
		}
	}

	if (forbidden) {
		std::size_t count (0);
		for (std::size_t i(0); i<pad.size(); ++i) {
			for (std::size_t j(0); j<pad[i].size(); ++j) {
				if (forbid_mates[count].size()>1 && pad[i][j]!=-1) {
					for (std::size_t k(1); k<forbid_mates[count].size(); ++k) {
						if (forbid_mates[count][k]==pad[i][j]) ++forbidden_penalty_count;
					}
				}
				++count;
			}
		}
		if (forbidden_penalty_count) return false;
	}

	return true;

}

double evaluate (const vint3D& c, const vint2D& pad, double npar) {

	/* LOCAL VARIABLES */
	double value (0.0), Wdiv (0.0), Bdiv (0.0), penalty (0.0);
	double small_penalty (1000.0), big_penalty (100000.0);

	/* SET PENALTIES */
	if (migra > nmigra) {
		penalty += (migra - nmigra) * big_penalty;
	} else if (migra < nmigra) {
		penalty += (nmigra - migra) * big_penalty; // Settled, by now, to force the number of migrants
	}
	if (monogamy) {
		penalty += small_penalty * monogamy_penalty_count;
	}
	if (forbidden) {
		penalty += big_penalty * forbidden_penalty_count;
	}

	// Analyzing the mean inbreeding coefficient in next generation
	double parpar (0.0);
	if (tasa>0.0) {

		for (std::size_t i (0); i < pop; i++) {
			std::size_t n_fem_i (NperSex[i][0]);
			for (std::size_t j (0); j < n_fem_i; j++) {
				if (pad[i][j] == (-1)) continue;
				int posm (struc[i][0] + j);
				int posp (struc[i][1] + pad[i][j]);
				int index_a (posm), index_b (posp);
				if (posm < posp) {
					index_a = posp;
					index_b = posm;
				}
				for (std::size_t k (0); k < pop; k++) {
					if ((!isol) || (isol && (k == searchpop))) {
						parpar += fmol[index_a][index_b] * (c[posm][k][0] + c[posm][k][1]) / (offspring[k][0] + offspring[k][1]);
					}
				}
			}
		}
		parpar /= pop;
		if (parpar > npar) {
			penalty += (parpar - npar) * big_penalty;
		}
	}

	// EVALUATE CONTRIBUTIONS BY GENETIC OR ALLELIC PARAMETERS
	if (manage == GENETIC) {
		evaluate_genetic_div (Wdiv, Bdiv, c);
		if (isol) value = Wdiv + penalty + lambda * parpar;
		else value = Bdiv + (lambda * Wdiv) + penalty;
	} else if (manage == ALLELIC) {
		evaluate_allelic_div (Wdiv, Bdiv, c);
		value = -(Bdiv + (lambda * Wdiv)) + penalty;
	} else if (manage == N_ALLELES) {
		evaluate_allelic_num (Wdiv, c);
		value = Wdiv + penalty;
	} else if (manage == RANDOM) {
		value = penalty;
	}

	return value;

}

inline void evaluate_genetic_div (double& Wdiv, double& Bdiv, const vint3D& c) {

	// Wdiv
	for (std::size_t k (0); k < pop; k++) {

		if ((!isol) || (isol && (k == searchpop))) {
			for (int s1 (0); s1 < 2; s1++) {
				for (int s2 (0); s2 < 2; s2++) {
					std::size_t total_offspring (offspring[k][s1] * offspring[k][s2]);
					for (std::size_t i (0); i < totalN; i++) {
						Wdiv += (c[i][k][s1] * c[i][k][s2]) * fmol[i][i] / total_offspring;
						for (std::size_t j (i + 1); j < totalN; j++) {
							Wdiv += 2.0 * fmol[j][i] * c[j][k][s1] * c[i][k][s2] / total_offspring;
						}
					}
				}
			}
		}

	// Bdiv
		if (!isol) {
			for (std::size_t l (0); l < pop; l++) {
				if (l == k) {
					continue;
				} else {
					for (std::size_t s1 (0); s1 < 2; s1++) {
						for (std::size_t s2 (0); s2 < 2; s2++) {
							std::size_t total_offspring (offspring[k][s1] * offspring[l][s2]);
							for (std::size_t i (0); i < totalN; i++) {
								Bdiv += (c[i][k][s1] * c[i][l][s2]) * fmol[i][i] / total_offspring;
								for (std::size_t j (i + 1); j < totalN; j++) {
									Bdiv += 2.0 * c[j][l][s2] * c[i][k][s1] * fmol[j][i] / total_offspring;
								}
							}
						}
					}
				}
			}
		}
	}

	Bdiv /= 4;
	Wdiv /= 4;

}

inline void evaluate_allelic_div (double& Wdiv, double& Bdiv, const vint3D& c) {

	for (std::size_t m (0); m < loci; m++) {
		vdou2D prob_caso (prob_caso_init[m]);
		vdou2D prob_ind (prob_ind_init[m]);

		// Calculate prob
		for (std::size_t k (0); k < pop; k++) {
			for (std::size_t i (0); i < totalN; i++) {
				int pop_coord (demeInd[i]), ind_coord (i - struc[pop_coord][0]);
				for (std::size_t a (0); a < code[m].size(); a++) {
					if (allelic_rich_map[pop_coord][ind_coord ][m][a] == 1) {
						prob_ind[a][i] = pow(0.5, c[i][k][0] + c[i][k][1]);
					} else if (allelic_rich_map[pop_coord][ind_coord ][m][a] == 2) {
						prob_ind[a][i] = max(0.0, 1.0 - (c[i][k][0] + c[i][k][1]));
					} else {
						prob_ind[a][i] = 1;
					}
						prob_caso[a][k] *= prob_ind[a][i];
					}
				}
			}

			// Calculate Wdiv and Bdiv
			for (std::size_t k (0); k < pop; k++) {
				for (std::size_t a (0); a < code[m].size(); a++) {
					double p_ak (prob_caso[a][k]);
					Wdiv += (1.0 - p_ak);
					for (std::size_t j (0); j < pop; j++) {
						Bdiv += (p_ak * (1.0 - prob_caso[a][j]) + prob_caso[a][j] * (1.0 - p_ak));
					}
				}
			}
		}

	Wdiv /= (pop * loci);
	Wdiv -= 1;
	Bdiv /= (2.0 * loci * pow(pop, 2.0));

}

inline void evaluate_allelic_num (double& Wdiv, const vint3D& c) {

	Wdiv = 0.0; // expected number of alleles LOST per locus

	vint1D sumCont;
	for (std::size_t i (0); i < totalN; i++) {
		int cont_ind (0);
		for (std::size_t k (0); k < pop; k++) {
			cont_ind += c[i][k][0] + c[i][k][1];
		}
		sumCont.push_back(cont_ind);
	}

	int pop_coord (0), ind_coord (0);
	for (std::size_t m (0); m < loci; m++) {
		for (std::size_t a (0); a < code[m].size(); a++) {
			double prob_loss (1.0);
			for (std::size_t i (0); i < totalN; i++) {
				pop_coord = demeInd[i];
				ind_coord  = i - struc[pop_coord][0];
				if (allelic_rich_map[pop_coord][ind_coord ][m][a] == 1) {
					prob_loss *= pow(0.5, sumCont[i]);
				} else if (allelic_rich_map[pop_coord][ind_coord ][m][a] == 2) {
					prob_loss *= max(0.0, 1.0 - sumCont[i]);
				}
			}
			Wdiv += prob_loss;
		}
	}

	Wdiv /= (double)loci;
}

inline void initialize_migrants ( const vint3D& solution) {
	migra = 0;
	for (std::size_t i (0); i < totalN; ++i) {
		for (std::size_t j (0); j < pop; ++j) {
			migra += solution[i][j][0] + solution[i][j][1];
		}
	}
}

inline void calculate_migrants ( const vint3D& solution) {
	migra /= 2;
	for (std::size_t k (0); k < pop; k++) {
		std::size_t n_fem_k (NperSex[k][0]);
		std::size_t pos (struc[k][0]);
		for (std::size_t i (pos); i < (pos + n_fem_k); i++) {
			migra -= (solution[i][k][0] + solution[i][k][1]);
		}

	}
}

inline void initialize_allelic ( const std::vector<population>& deme ) {

	// Allelic richness
	allelic_rich_map.clear();
	for (std::size_t i(0); i<pop; ++i) {
		vint3D allelic_rich_pop;
		for (std::size_t j(0); j<deme[i].getPopulationSize(); ++j) {
			vint2D allelic_rich_ind;
			for (std::size_t k(0); k<loci; ++k) {
					vint1D allelic_rich_loc;
					for (std::size_t h(0); h<code[k].size(); ++h) {
						allelic_rich_loc.push_back(deme[i].getIndividual(j).getLocus(k).getNAllels(code[k][h]));
					}
					allelic_rich_ind.push_back(allelic_rich_loc);
				}
			allelic_rich_pop.push_back(allelic_rich_ind);
		}
		allelic_rich_map.push_back(allelic_rich_pop);
	}

	// Deme Ind
	demeInd.clear();
	std::size_t x (0), index(0);
	if (manage == ALLELIC) {
		demeInd.push_back(x);
		index = 1;
	}
	for (int i (index); i < (int)totalN; i++) {
		if (x < pop - 1) {
			if (struc[x + 1][0] == i) x++;
		}
		demeInd.push_back(x);
	}

	// Prob case init
	if (manage == ALLELIC) {
		prob_caso_init.clear();
		prob_ind_init.clear();
		for (std::size_t m (0); m < loci; m++) {
			vdou2D caso_matrix;
			vdou2D ind_matrix;
			for (std::size_t a(0); a < code[m].size(); ++a) {
				vdou1D caso_row;
				vdou1D ind_row;
				for (std::size_t k(0); k<pop; ++k) {
					caso_row.push_back(1.0);
				}
				caso_matrix.push_back(caso_row);
				for (std::size_t k(0); k<totalN; ++k) {
					ind_row.push_back(1.0);
				}
				ind_matrix.push_back(ind_row);
			}
			prob_caso_init.push_back(caso_matrix);
			prob_ind_init.push_back(ind_matrix);
		}
	}

}

bool check_forbid ( std::size_t pop, std::size_t fem_index, vint2D& padopt, vint2D& forbid) {
	if (!forbid.size()) return false;
	int male_id (padopt[pop][fem_index]);
	std::size_t forbid_index (0);
	for (std::size_t i(0); i<padopt.size(); ++i) {
		bool exit (false);
		for (std::size_t j(0); j<padopt[i].size(); ++j) {
			if (i==pop && j== fem_index) {
				exit = true;
				break;
			}
			++forbid_index;
		}
		if (exit) break;
	}

	for (std::size_t i(1); i<forbid[forbid_index].size(); ++i) {
		if (forbid[forbid_index][i] == male_id) return true;
	}
	return false;
}

bool check_forbid (vint2D& padopt, vint2D& forbid) {
	for (std::size_t i(0); i<padopt.size(); ++i) {
		for (std::size_t j(0); j<padopt[i].size(); ++j) {
			if (check_forbid(i,j,padopt,forbid)) return true;
		}
	}
	return false;
}
