#ifndef MTP_CONFIGFILE_H
#define MTP_CONFIGFILE_H

#include <vector>
#include <time.h>
#include "ConfigFile.h"
#include "utils.h"


// =======   VARIABLE TYPES  =======
// Entry method
enum calltype {
	LEGACY, // datafile configfile
	DEFAULT, // datafile
	ADVANCED, // [options] [--config] datafile
};

// Datafile format
enum Format {
	MTP, // Metapop
	GNP, // Genepop
	PED // Plink PED
};

// Management optimization
enum Management {
	GENETIC,	// Genetic diversity
	ALLELIC,	// Allelic diversity
	N_ALLELES,	// Total expected number of alleles
	RANDOM,		// Random mating (no management)
	NONE		// Skip management
};

// Management methods
enum Method {
	DYNAMIC,	// Dynamic method
	OMPG,		// One migrant per generation
	ISOLATED,	// Isolated population
};

// Synthetic population
enum Synthetic {
	GT,		// Genetic diversity (total)
	GW,		// Genetic diversity (within subpopulations)
	GB,		// Genetic diversity (between subpopulations)
	AT,		// Allelic diversity (total)
	AW,		// Allelic diversity (within subpopulations)
	AB,		// Allelic diversity (between subpopulations)
	NA,		// Number of alleles
	HE,		// Expected heterocigosity
	ALL,	// Test all previous methods in a single run
	NO		// Random
};


// =======   DEFAULT  VALUES  =======

const std::string DEFAULT_CASO = "MTP";
const Format DEFAULT_FORMAT = PED;
const std::string DEFAULT_SFORMAT = "ped";
constexpr bool DEFAULT_DEBUG = false;
constexpr bool DEFAULT_NUCLEOTIDE = false;
constexpr int DEFAULT_DECF = 4;
constexpr bool DEFAULT_PRINTCOAN = false;
constexpr bool DEFAULT_SAVETSV = false;
constexpr bool DEFAULT_PAIRWISE_STATS = false;
constexpr int DEFAULT_PLOIDY = 2;

constexpr bool DEFAULT_ADDRARE = true;
constexpr int DEFAULT_ADDRAREN = 0;
constexpr bool DEFAULT_NCCORR = false;
constexpr int DEFAULT_BT_N = 0;
constexpr double DEFAULT_BT_ALPHA = 0.05;

const Synthetic DEFAULT_SYNTH = NO;
const std::string DEFAULT_SSYNTH = "none";
constexpr int DEFAULT_NPOOL = 1000;
constexpr double DEFAULT_MINRANK = 0.0;

const Management DEFAULT_MANAGE = NONE;
const std::string DEFAULT_SMANAGE = "none";
const Method DEFAULT_METHOD = DYNAMIC;
const std::string DEFAULT_SMETHOD = "dynamic";
const std::string DEFAULT_MNG_FILE = "";
constexpr int DEFAULT_CONVERGENCE = 50;

constexpr int DEFAULT_NMIGRA = 0;
constexpr double DEFAULT_LAMBDA = 1.0;
constexpr double DEFAULT_TASA = 0.0;
constexpr bool DEFAULT_MONOGAMY = true;
constexpr double DEFAULT_WEIGHT_F_ISOL = 1.0;
constexpr int DEFAULT_MAXOFF = 0;

constexpr int DEFAULT_SA_STEPS = 200;
constexpr double DEFAULT_SA_T = 0.1;
constexpr double DEFAULT_SA_K = 0.9;

constexpr bool DEFAULT_SIMMODE = false;
constexpr int DEFAULT_SIMREPS = 1;
constexpr int DEFAULT_SIMGENS = 1;
constexpr bool DEFAULT_SAVEFRECS = false;
constexpr bool DEFAULT_SAVESTATS = true;
constexpr bool DEFAULT_SAVEMIGRA = false;

constexpr int DEFAULT_POLIDY = 2;
constexpr double DEFAULT_LAMBDA_RANK = 1.0;

const std::string DEFAULT_CONVERT_MODE = "none";


// ====   CLASSES   ====
class MTPConfigFile: public ConfigFile {

protected:

	std::string file;
	calltype entry_method;

	// General options
	std::string caso;
	Format format;
	bool format_call;
	std::string s_format;
	bool debug;
	bool use_nucleotides;
	int decF;
	int seed;
	bool print_coan;
	bool save_tsv;
	bool pairwise_stats_matrix;
	int ploidy;

	// Configuration for analysis of population
	bool ADrare;
	int ADrareN;
	bool NCcorr;
	int bt_n;
	double bt_alpha;
	Synthetic synth;
	std::string s_synth;
	int nPool;
	double minRank;

	// Configuration for management method
	Management manage;
	std::string s_manage;
	std::string mng_file;
	std::string rm_file;
	Method method;
	std::string s_method;
	bool isol;
	int convergence;

	// Restrictions
	int nmigra;
	double lambda;
	double tasa;
	bool monogamy;
	double weight_F_isol;
	int max_off_mate;

	// Simulated annealing
	int SA_steps;
	double SA_t;
	double SA_k;

	// Configurartion for simulation
	bool simMode;
	int simReps;
	int simGens;
	bool saveFrecs;
	bool saveStats;
	bool saveMigra;

	// Not specified or in use (yet)
	double lambda_rank;

	// Compatibility options
	std::string convert_mode;

public:

	// Constructor
	MTPConfigFile ( int , char** , std::string& );

	// Reading methods
	Format readFormat (const std::string&);
	Management readManagement (const std::string&);
	Method readMethod (const std::string&);
	Synthetic readSynthetic (const std::string&);

	// Check parameters
	void check ();
	std::string check_option_config ( std::vector<std::string>& , bool& );
	void check_option ( std::string );
	bool valid_parameter (std::string);

	// "Get" methods
	void print_settings ();

	std::string get_filename () const { return file; }
	std::string get_caso () const { return caso; }
	bool get_debug () const { return debug; }
	bool get_use_nucleotides () const { return use_nucleotides; }
	int get_decF () const { return decF; }
	int get_seed () const { return seed; }
	bool get_print_coan () const { return print_coan; }
	bool get_save_tsv () const { return save_tsv; }
	bool get_pairwise_stats () const { return pairwise_stats_matrix; }
	int get_ploidy () const { return ploidy; }

	int get_bt_n () const { return bt_n; }
	double get_bt_alpha () const { return bt_alpha; }
	bool get_NCcorr () const { return NCcorr; }
	bool get_ADrare () const { return ADrare; }
	int get_ADrareN () const { return ADrareN; }
	Synthetic get_synth () const { return synth; }
	int get_nPool () const { return nPool; }
	double get_minRank () const { return minRank; }

	Management get_manage () const { return manage; }
	std::string get_mng_file () const { return mng_file; }
	std::string get_rm_file () const { return rm_file; }
	Method get_method () const { return method; }
	bool get_isol () const { return isol; }
	int get_convergence () const { return convergence; }

	int get_nmigra () const { return nmigra; }
	double get_lambda () const { return lambda; }
	bool get_monogamy () const { return monogamy; }
	double get_weight_F_isol () const { return weight_F_isol; }
	int get_maxoff () const { return max_off_mate; }

	int get_SA_steps () const { return SA_steps; }
	double get_SA_t () const { return SA_t; }
	double get_SA_k () const { return SA_k; }

	bool get_simMode () const { return simMode; }
	int get_simReps () const { return simReps; }
	int get_simGens () const { return simGens; }
	bool get_saveFrecs () const { return saveFrecs; }
	bool get_saveStats () const { return saveStats; }
	bool get_saveMigra () const { return saveMigra; }

	double get_lambda_rank () const { return lambda_rank; }
	double get_tasa () const { return tasa; }

	std::string get_convert_mode () const { return convert_mode; }
	Format get_datafile_format () const { return format; }
	std::string get_datafile_sformat () const { return s_format; }

	// "Set" methods
	void disable_rarefaction() {
		ADrare = false;
		ADrareN = 0;
	}

protected:

	Management translate_management ();
	Method translate_method ();
	Synthetic translate_synth ();
	void print_help ();
	void config_error ( const std::string& );

};

#endif
