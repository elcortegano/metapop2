/* =======================================================================================
 *                                                                                       *
 *       Filename:  output.cpp                                                           *
 *                                                                                       *
 *    Description: This file contaings functions to save output files with results,      *
 *                 including poolfiles and results from simulation.                      *
 *                                                                                       *
 *                  -> save_header ()                                                    *
 *                  -> save_freq ()                                                      *
 *                  -> output ()                                                         *
 *                  -> save_synthetic ()                                                 *
 *                  -> printSolution ()                                                  *
 *                  -> generaOff ()                                                      *
 *                                                                                       *
 ======================================================================================= */

#include "output.h"


/* ********************************************************************************************
 * save_header ()
 * --------------------------------------------------------------------------------------------
 * Prints the header of the main output file: res_[name]_FULL
 * ********************************************************************************************/
void save_header (const MTPConfigFile& config, std::string datafile, std::string output, time_t current_time) {

	/* LOCAL VARIABLES */
	std::ofstream fr;
	std::string caso (config.get_caso());
	int seed (config.get_seed());
	fr.open(output.data(), std::ios::out);

	if (!fr) print_error("Problem creating results file");
	else {
		struct tm * date = localtime( & current_time );
		print_header (fr, "METAPOP2 OUTPUT", H1);
		print_textline (fr, "Version: " + version);
		fr << date->tm_mday << "/" << date->tm_mon+1 << "/" << date->tm_year+1900 << " at " 
			<< date->tm_hour << ":" << date->tm_min << ":" << date->tm_sec << md_endl << std::endl;
		print_textline (fr, "Seed: " + std::to_string(seed));
		fr << std::endl;
		print_textline (fr, "Data file: " + datafile);
		print_textline (fr, "Configuration file: " + config.get_filename());
		print_textline (fr, "Name: " + caso);
	}
	fr.close();
}

/* ********************************************************************************************
 * save_frequencies ()
 * --------------------------------------------------------------------------------------------
 * This function saves output files for the average frequencies in the population. Two files can
 * be generated: res_[name]_FULL and res_[name]_p.dat
 * ********************************************************************************************/
void save_freq (const MTPConfigFile& config, const Metapop& metapop, std::string output, std::size_t rep, std::size_t gen) {

	/* LOCAL VARIABLES */
	bool mol_markers (metapop.get_use_molecular_markers());
	if (!mol_markers) return;
	int decF (config.get_decF());
	bool use_nucleotides (config.get_use_nucleotides());
	bool sim_results (config.get_saveFrecs());
	std::string caso (config.get_caso());
	std::size_t pop (metapop.get_n_subpops());
	std::size_t loci (metapop.get_n_loci());
	std::vector<population> deme (metapop.get_deme());
	BT_parameter <std::vector<double>> p (metapop.get_p());
	allele2D code (metapop.get_alleles_per_locus());
	vp_vdou2D ps (p.b_par);
	vp_vdou1D pt_n (p.t_par);

	/* OUTPUT: res_[name]_p.tsv */
	std::string filename ("res_" + caso + "_p.tsv");
	std::ofstream fr;
	static size_t calls (0);
	if (!rep && !gen) fr.open (filename, std::ios::out);
	else fr.open (filename, std::ios::app);
	if (!fr) print_error("Problem creating frequency file");

	/* WRITE HEADER */
	std::string header;
	if (!rep && !gen) {
		if (sim_results) header += "replicate" + delim + "generation" + delim;
		header += "locus"+ delim + "allele" + delim + "pop" + delim + "p";
	}

	/* WRITE BODY */
	std::vector<std::string> rows;
	if (sim_results || !(rep+gen)) {
		for (std::size_t m (0); m < loci; m++) {
			for (std::size_t k (0); k < code[m].size(); k++) {
				if (!code[m][k]) continue;
				std::string sim_info;
				if (sim_results) sim_info = std::to_string(rep) + delim + std::to_string(gen) + delim;
				std::string mtp_info = sim_info + std::to_string(m+1) + delim;
				if (!use_nucleotides) mtp_info += std::to_string(code[m][k]);
				else mtp_info += print_nucleotide(code[m][k]);
				mtp_info += delim + "metapop" + delim + round_s((*pt_n[m])[k], decF);
				rows.push_back(mtp_info);
				for (std::size_t i(0); i < pop; i++){
					std::string loc_info (sim_info + std::to_string(m+1) + delim);
					if (!use_nucleotides) loc_info += std::to_string(code[m][k]);
					else loc_info += print_nucleotide(code[m][k]);
					loc_info += delim + deme[i].getLabel() + delim + round_s((*ps[i])[m][k], decF);
					rows.push_back(loc_info);
				}
			}
		}
	}
	print_csv(fr, header, rows);
	fr.close();
	++calls;
}

/* ********************************************************************************************
 * save_pop_analysis ()
 * --------------------------------------------------------------------------------------------
 * It returns results from population analysis in two main sections: GENETIC and ALLELIC diversity
 * ********************************************************************************************/
void save_pop_analysis ( const MTPConfigFile& config, const Metapop& metapop, std::string output, const Calculations& calculations, const Calculations_boot& calc_boot) {

	/* LOCAL VARIABLES */
	std::string caso (config.get_caso());
	int decF (config.get_decF());
	bool pairwise_stats (config.get_pairwise_stats());
	bool NCcorr (config.get_NCcorr());
	bool ADrare (config.get_ADrare());
	bool save_tsv (config.get_save_tsv());
	std::size_t pop (metapop.get_n_subpops());
	std::size_t loci (metapop.get_n_loci());
	bool use_molecular_markers (metapop.get_use_molecular_markers());
	std::vector<population> deme (metapop.get_deme());
	vp_vdou1D fextra (metapop.get_f().t_par);
	vp_double s (metapop.get_s().t_par);
	vp_vdou1D d (metapop.get_d().t_par);
	vint1D u_alleles (metapop.get_unique_alleles());
	bool boot (false);
	if (config.get_bt_n()) boot = true;
	double alpha (config.get_bt_alpha());

	/* VARIABLES FROM CALCULATIONS */
	vdou1D F (calculations.get_F());
	vdou1D G (calculations.get_G());
	vdou1D Fis_pop (calculations.get_Fis_pop());
	vdou2D Fst_b (calculations.get_Fst_b());
	double Gbar_n (calculations.get_Gbar_n());
	G_parameters <double> bar_n (calculations.get_bar_n()); // fbar_n, sbar_n, Fbar_n, dbar_n

	Fisher <double> F_n (calculations.get_F_n()); // Fis_n, Fst_n, Fit_n
	Fisher <vdou2D> D (calculations.get_D()); // D.is -> D; st-> Ds; it-> Dr
	G_parameters <vdou1D> totR_n (calculations.get_totR_n()); // ftotR_n
	G_parameters <vdou1D> barR_n (calculations.get_barR_n()); // sbarR_n, fbarR_n
	G_parameters <double> tot_n (calculations.get_tot_n()); // Dtot_n, ftot_n
	vdou1D DNbar (calculations.get_DNbar());

	std::vector<int>  Nrare_loc (calculations.get_Nrare_loc());
	double AST (calculations.get_Ast());
	A_parameters <vdou1D> con_ (calculations.get_con_a());
	A_parameters <double> con_n (calculations.get_con_n());
	A_parameters <vdou2D> pop_ (calculations.get_pop_a());
	A_parameters <double> A (calculations.get_A()); // before: AS, DA
	A_parameters <vdou1D> rem_ (calculations.get_rem()); // before: As_rem, Da_rem
	vdou1D app (calculations.get_Kpop ());

	/* SAVE COANCESTRY */
	if (config.get_print_coan()) {
		std::ofstream fc;
		std::string sc = "res_" + caso + "_rmatrix.txt";
		fc.open(sc, std::ios::out);
		if (!fc) print_error("Problem creating coancestry file");
		save_tmatrix(fc, metapop.get_fmol());
	}

	/* OPEN POPULATION ANALYSIS FILE */
	std::ofstream fr;
	fr.open(output.data(), std::ios::app);

	if (!fr) print_error("Problem creating results file");
	print_header(fr, "1. Results of Population Analysis", H2);

	/* GENETIC DIVERSITY */
	print_header (fr, "GENE DIVERSITY", H3);

	/* Parameters of subpopulations: f, s, F, d G and alpha */
	if (!boot) print_header (fr, "Within-subpopulation parameters", H3);
	else print_header(fr, "Within-subpopulation parameters (bootstrap " + round_s(100.0*(1.0-alpha), decF) + "% limits)", H3);
	print_textline (fr, item_list + bold("$f_{ii}$") + ": Average coancestry between individuals.");
	print_textline (fr, item_list + bold("$s_{i}$") + ": Average self-coancestry of individuals.");
	print_textline (fr, item_list + bold("$F_{i}$") + ": Average coefficient of inbreeding of individuals.");
	print_textline (fr, item_list + bold("$d_{ii}$") + ": Average Nei distance between individuals.");
	print_textline (fr, item_list + bold("$G_{i}$") + ": Proportion of diversity between individuals.");
	print_textline (fr, item_list + bold("$α_{i}$") + ": Average deviation from Hardy-Weinberg equilibrium.");

	std::string header3a ("Subpop. | $f_{ii}$ | $s_{i}$ | $F_{i}$ | $d_{ii}$ | $G_{i}$ | $α_{i}$");
	std::vector<std::string> rows3a;
	for (std::size_t i (0); i < pop; i++) {
		if (!boot) {
			rows3a.push_back("  " + deme[i].getLabel() +
							"  |" + round_s((*fextra[i])[i], decF)
							+ "|" + round_s((*s[i]), decF)
							+ "|" + round_s(F[i], decF)
							+ "|" + round_s((*d[i])[i], decF)
							+ "|" + round_s(G[i], decF)
							+ "|" + round_s(Fis_pop[i], decF));
		} else {
			rows3a.push_back("  " + deme[i].getLabel() +
							"  | " + round_s((*fextra[i])[i], decF) + " (" + round_s(calc_boot.get_fextra(i,i).low, decF) + " - " + round_s(calc_boot.get_fextra(i,i).high, decF) + ") "
							+ "| " + round_s((*s[i]), decF) + " (" + round_s(calc_boot.get_s(i).low, decF) + " - " + round_s(calc_boot.get_s(i).high, decF) + ") "
							+ "| " + round_s(F[i], decF) + " (" + round_s(calc_boot.get_F(i).low, decF) + " - " + round_s(calc_boot.get_F(i).high, decF) + ") " 
							+ "| " + round_s((*d[i])[i], decF) + " (" + round_s(calc_boot.get_d(i,i).low, decF) + " - " + round_s(calc_boot.get_d(i,i).high, decF) + ") "
							+ "| " + round_s(G[i], decF) + " (" + round_s(calc_boot.get_G(i).low, decF) + " - " + round_s(calc_boot.get_G(i).high, decF) + ") " 
							+ "| " + round_s(Fis_pop[i], decF) + " (" + round_s(calc_boot.get_Fis_pop(i).low, decF) + " - " + round_s(calc_boot.get_Fis_pop(i).high, decF) + ") " );
		}
	}
	if (boot) {
		rows3a.push_back(("**Average** |" + bold(round_s(bar_n.f, decF)) + " (" + round_s(calc_boot.get_avg_fextra().low, decF) + " - " + round_s(calc_boot.get_avg_fextra().high, decF) + ") "
						"|" + bold(round_s(bar_n.s, decF)) + " (" + round_s(calc_boot.get_avg_s().low, decF) + " - " + round_s(calc_boot.get_avg_s().high, decF) + ") "
						"|" + bold(round_s(bar_n.F, decF)) + " (" + round_s(calc_boot.get_avg_F().low, decF) + " - " + round_s(calc_boot.get_avg_F().high, decF) + ") "
						"|" + bold(round_s(bar_n.d, decF)) + " (" + round_s(calc_boot.get_avg_d().low, decF) + " - " + round_s(calc_boot.get_avg_d().high, decF) + ") "
						"|" + bold(round_s(Gbar_n, decF)) + " (" + round_s(calc_boot.get_avg_G().low, decF) + " - " + round_s(calc_boot.get_avg_G().high, decF) + ") "
						"|" + bold(round_s(F_n.is, decF))) + " (" + round_s(calc_boot.get_avg_Fis_pop().low, decF) + " - " + round_s(calc_boot.get_avg_Fis_pop().high, decF) + ") ");
	} else {
		rows3a.push_back(("**Average** |" + bold(round_s(bar_n.f, decF)) +
						"|" + bold(round_s(bar_n.s, decF)) + 
						"|" + bold(round_s(bar_n.F, decF)) +
						"|" + bold(round_s(bar_n.d, decF)) +
						"|" + bold(round_s(Gbar_n, decF)) +
						"|" + bold(round_s(F_n.is, decF))));
	}
	print_table (fr, header3a, rows3a);
	if (save_tsv) {
		std::ofstream fr_tsv;
		fr_tsv.open("res_" + caso + "_within_G.tsv", std::ios::out);
		fr_tsv << "Subpop.\tf_{ii}\ts_{i}\tF_{i}\td_{ii}\tG_{i}\tf_{ii}" << std::endl;
		for (std::size_t i (0); i < pop; i++) {
			fr_tsv << deme[i].getLabel();
			fr_tsv << '\t' << (*fextra[i])[i];
			fr_tsv << '\t' << (*s[i]);
			fr_tsv << '\t' << F[i];
			fr_tsv << '\t' << (*d[i])[i];
			fr_tsv << '\t' << G[i];
			fr_tsv << '\t' << Fis_pop[i];
			fr_tsv << std::endl;
		}
		fr_tsv.close();
	}

	/* Coancestry and genetic distance between subpopulations */
	if (!boot) print_header(fr, "Between-subpopulation parameters", H3);
	else print_header(fr, "Between-subpopulation parameters (in proportion, bootstrap " + round_s(100.0*(1.0-alpha), decF) + "% limits)", H3);
	print_textline (fr, item_list + bold("$f_{ij}$") + ": Average coancestry between pairs of subpopulations.");
	//print_textline (fr, item_list + bold("$DR_{ij}$") + ": Reynold's genetic distance.");
	print_textline (fr, item_list + bold("$D_{G,ij}$") + ": Nei's minimum genetic distance between pairs of subpopulations.");
	//print_textline (fr, item_list + bold("$DS_{ij}$") + ": Nei-standard genetic distance.");
	print_textline (fr, item_list + bold("$F_{ST,ij}$") + ": Gene frequency differentiation index between pairs of subpopulations.");

	if (!pairwise_stats) {
		std::string header3b ("Subpop. | $f_{ij}$ | $D_{G,ij}$ | $F_{ST,ij}$ ");
		std::vector<std::string> rows3b;
		for (std::size_t i (0); i < pop; i++) {
			for (std::size_t j (i); j < pop; j++) {
				if (i != j) {
					if (!boot) {
						rows3b.push_back(deme[i].getLabel() + " - " + deme[j].getLabel()
						+ " | " + round_s((*fextra[i])[j], decF)
						+ " | " + /*round_s(D.it[i][j], decF)
						+ " | " +*/ round_s(D.is[i][j], decF)
						+ " | " + /*round_s(D.st[i][j], decF)*/ round_s(Fst_b[i][j], decF));
					} else {
						rows3b.push_back(deme[i].getLabel() + " - " + deme[j].getLabel()
						+ " | " + round_s((*fextra[i])[j], decF) + " (" + round_s(calc_boot.get_fextra(i,j).low, decF) + " - " + round_s(calc_boot.get_fextra(i,j).high, decF) + ")" 
						+ " | " + /*round_s(D.it[i][j], decF)
						+ " | " +*/ round_s(D.is[i][j], decF) + " (" + round_s(calc_boot.get_D_is(i,j).low, decF) + " - " + round_s(calc_boot.get_D_is(i,j).high, decF) + ")" 
						+ " | " + /*round_s(D.st[i][j], decF)*/ round_s(Fst_b[i][j], decF) + " (" + round_s(calc_boot.get_Fst_b(i,j).low, decF) + " - " + round_s(calc_boot.get_Fst_b(i,j).high, decF) + ")" );
					}
				}
			}
		}
		print_table (fr, header3b, rows3b);
	} else {
		vdou2D tmp_fextra;
		for (size_t i(0); i< fextra.size(); ++i) {
			vdou1D row;
			for (std::size_t j(0); j<(*fextra[i]).size(); ++j) {
				row.push_back((*fextra[i])[j]);
			}
			tmp_fextra.push_back(row);
		}
		print_pairwise_table (fr, "$f_{ij}$ pairwise table", tmp_fextra, deme, decF);
		//print_pairwise_table (fr, "$D_{G,ij}$ pairwise table", D.it, deme, decF);
		print_pairwise_table (fr, "$DN_{ij}$ pairwise table", D.is, deme, decF);
		//print_pairwise_table (fr, "$DS_{ij}$ pairwise table", D.st, deme, decF);
		print_pairwise_table (fr, "$F_{ST,ij}$ pairwise table", Fst_b, deme, decF);
	}
	if (save_tsv) {
		std::ofstream fr_tsv;
		fr_tsv.open("res_" + caso + "_between_G.tsv", std::ios::out);
		fr_tsv << "Subpop.\tf_{ij}\tD_{G,ij}\tF_{ST,ij}" << std::endl;
		for (std::size_t i (0); i < pop; i++) {
			for (std::size_t j (i); j < pop; j++) {
				if (i != j) {
					fr_tsv << deme[i].getLabel() + " - " + deme[j].getLabel();
					fr_tsv << '\t' << (*fextra[i])[j];
					fr_tsv << '\t' << D.is[i][j];
					fr_tsv << '\t' << Fst_b[i][j];
					fr_tsv << std::endl;
				}
			}
		}
		fr_tsv.close();
	}

	/* Contribution of each subpopulation to total gene diversity */
	/*print_header (fr, "Contribution of each subpopulation total gene diversity (expected heterozygosity) of the population :", H3);
	//print_textline (fr, item_list + bold("GD_WI") + ": Gene diversity within individuals.");
	//print_textline (fr, item_list + bold("GD_BI") + ": Gene diversity between individuals.");
	print_textline (fr, item_list + bold("$GD_{WS}$") + ": Gene diversity within subpopulations.");
	print_textline (fr, item_list + bold("$GD_{BS}$") + ": Gene diversity between subpopulations.");
	print_textline (fr, item_list + bold("$GD_{T}$") + ": Total gene diversity.");

	std::string header3c ("Subpop. | $GD_{WS}$ | $GD_{BS}$ | $GD_{T}$");
	std::vector<std::string> rows3c;
	for (std::size_t i (0); i < pop; i++) {
		rows3c.push_back("  " + deme[i].getLabel() + *//*"  |" + round_s(((1.0 - (*s[i])) * deme[i].getEffectiveSize() / totalNe), decF) + " |" + round_s((*d[i])[i] * deme[i].getEffectiveSize() / totalNe, decF)*//* + " |" + round_s((1.0 - (*fextra[i])[i]) * deme[i].getEffectiveSize() / totalNe, decF) + " |" + round_s(DNbar[i] * deme[i].getEffectiveSize() / totalNe, decF) + " |" + round_s((1.0 - ((*fextra[i])[i] - DNbar[i])) * deme[i].getEffectiveSize() / totalNe, decF));
	}
	rows3c.push_back("  Total |" *//*+ round_s(1.0 - bar_n.s, decF) + " |" + round_s(bar_n.d, decF) + " |"*//* + round_s(1.0 - bar_n.f, decF) + " |" + round_s(tot_n.d, decF) + " |" + round_s(1.0 - tot_n.f, decF));
	print_table (fr, header3c, rows3c);*/

	/* Average gene diversity parameters*/
	if (!boot) print_header(fr, "Average gene diversity parameters", H3);
	else print_header(fr, "Average gene diversity parameters (bootstrap " + round_s(100.0*(1.0-alpha), decF) + "% limits)", H3);
	if (!boot) {
		print_dataline (fr, item_list + "$H_{S}$: Average gene diversity within subpopulations", 1.0-bar_n.f, decF);
		print_dataline (fr, item_list + "$D_{G}$: Average gene diversity between subpopulations (Nei's minimum genetic distance)", bar_n.f-tot_n.f, decF);
		print_dataline (fr, item_list + "$H_{T}$: Total gene diversity", 1.0-tot_n.f, decF);
	} else {
		print_bootline (fr, item_list + "$H_{S}$: Average gene diversity within subpopulations", 1.0-bar_n.f, decF, calc_boot.get_HS());
		print_bootline (fr, item_list + "$D_{G}$: Average gene diversity between subpopulations (Nei's minimum genetic distance)", bar_n.f-tot_n.f, decF, calc_boot.get_DG());
		print_bootline (fr, item_list + "$H_{T}$: Total gene diversity", 1.0-tot_n.f, decF, calc_boot.get_HT());
	}

	/* F-statistics */
	if (!boot) print_header(fr, "F-statistics", H3);
	else print_header(fr, "F-statistics (bootstrap " + round_s(100.0*(1.0-alpha), decF) + "% limits)", H3);
	if (NCcorr) { print_textline(fr, "  (with Nei & Chesser 1983 correction for sample sizes)"); fr << std::endl; }
	if (!boot) {
		print_dataline (fr, item_list + "$F_{IS}$", F_n.is, decF);
		print_dataline (fr, item_list + "$F_{ST}$", F_n.st, decF);
		print_dataline (fr, item_list + "$F_{IT}$", F_n.it, decF);
	} else {
		print_bootline (fr, item_list + "$F_{IS}$", F_n.is, decF, calc_boot.get_Fis());
		print_bootline (fr, item_list + "$F_{ST}$", F_n.st, decF, calc_boot.get_Fst());
		print_bootline (fr, item_list + "$F_{IT}$", F_n.it, decF, calc_boot.get_Fit());
	}

	/* Partition of gene diversity variation */
	if (!boot) print_header(fr, "Partition of gene diversity variation (in proportion)", H3);
	else print_header(fr, "Partition of gene diversity variation (in proportion, bootstrap " + round_s(100.0*(1.0-alpha), decF) + "% limits)", H3);
	if (!boot) {
		print_dataline (fr, item_list + "Within individuals", (1.0-Gbar_n)*(1.0-F_n.st), decF);
		print_dataline (fr, item_list + "Between individuals", Gbar_n*(1.0-F_n.st), decF);
		print_dataline (fr, item_list + "Within subpopulations", (1.0-F_n.st), decF);
		print_dataline (fr, item_list + "Between subpopulations", F_n.st, decF);
	} else {
		print_bootline (fr, item_list + "Within individuals", (1.0-Gbar_n)*(1.0-F_n.st), decF, calc_boot.get_GDwi());
		print_bootline (fr, item_list + "Between individuals", Gbar_n*(1.0-F_n.st), decF, calc_boot.get_GDbi());
		print_bootline (fr, item_list + "Within subpopulations", (1.0-F_n.st), decF, calc_boot.get_GDws());
		print_bootline (fr, item_list + "Between subpopulations", F_n.st, decF, calc_boot.get_GDbs());
	}

	/* Total genetic diversity after removing a subpopulation */
	print_header(fr, "Percentage of loss (+) or gain (-) of gene diversity after removal of each subpopulation", H3);
	std::string header3d ("Subpop. removed | $H_{S}$ | $D_{G}$ | $H_{T}$");
	std::vector<std::string> rows3d;
	for (std::size_t i (0); i < pop; i++) {
		double gdt (1.0 - totR_n.f[i]); // this is the GD_T after removal (not the gain/loss in %)
		std::string row ("     " + deme[i].getLabel() + "       | ");
		double gdwi (-(((1.0 - barR_n.s[i]) - (1.0 - bar_n.s)) / gdt * 100));
		double gdbi (-((barR_n.s[i] - barR_n.f[i]) - (bar_n.s - bar_n.f)) / gdt * 100);
		//row += round_s(gdt, decF) + " |";
		row += round_s(gdwi+gdbi, decF);
		if (boot) row += " (" + round_s(calc_boot.get_rem_HS(i).low, decF) + " - " + round_s(calc_boot.get_rem_HS(i).high, decF) + ") ";
		row += " |";
		row += round_s(-((barR_n.f[i] - totR_n.f[i]) - (bar_n.f - tot_n.f)) / gdt * 100, decF);
		if (boot) row += " (" + round_s(calc_boot.get_rem_DG(i).low, decF) + " - " + round_s(calc_boot.get_rem_DG(i).high, decF) + ") ";
		row += " |";
		row += round_s(-((1.0 - totR_n.f[i]) - (1.0 - tot_n.f)) / gdt * 100, decF);
		if (boot) row += " (" + round_s(calc_boot.get_rem_HT(i).low, decF) + " - " + round_s(calc_boot.get_rem_HT(i).high, decF) + ") ";
		rows3d.push_back(row);
	}
	print_table (fr, header3d, rows3d);
	if (save_tsv) {
		std::ofstream fr_tsv;
		fr_tsv.open("res_" + caso + "_contribution_G.tsv", std::ios::out);
		fr_tsv << "Subpop. removed\tH_{S}\tD_{G}\tH_{T}" << std::endl;
		for (std::size_t i (0); i < pop; i++) {
			double gdt (1.0 - totR_n.f[i]);
			double gdwi (-(((1.0 - barR_n.s[i]) - (1.0 - bar_n.s)) / gdt * 100));
			double gdbi (-((barR_n.s[i] - barR_n.f[i]) - (bar_n.s - bar_n.f)) / gdt * 100);
			fr_tsv << deme[i].getLabel();
			fr_tsv << '\t' << gdwi+gdbi;
			fr_tsv << '\t' << -((barR_n.f[i] - totR_n.f[i]) - (bar_n.f - tot_n.f)) / gdt * 100;
			fr_tsv << '\t' << -((1.0 - totR_n.f[i]) - (1.0 - tot_n.f)) / gdt * 100;
			fr_tsv << std::endl;
		}
		fr_tsv.close();
	}

	/* Wright's F-statistic */
	/*print_header (fr, "$F_{ST}$ between pairs of subpopulations :", H3);
		if (!pairwise_stats) {
			std::string header3c ("Subpop. | $F_{ST}$ ");
			std::vector <std::string> rows3c;
			for (std::size_t i (0); i < pop; i++) {
				for (std::size_t j (i); j < pop; j++) {
					if (i != j) {
						rows3c.push_back(deme[i].getLabel() + " - " + deme[j].getLabel() + "| " + round_s(Fst_b[i][j], decF));
					}
				}
			}
			print_table (fr, header3c, rows3c);
		} else {
			print_pairwise_table (fr, "$F_{ST}$ pairwise table", Fst_b, deme, decF);
	}*/

	/* ALLELIC DIVERSITY (with rarefaction) */
	if (use_molecular_markers) {

		if (ADrare) {
			double mean_Nrare_loc (0.0);
			for (const auto& i: Nrare_loc) mean_Nrare_loc += i;
			mean_Nrare_loc /= Nrare_loc.size();
			print_header (fr, "ALLELIC DIVERSITY (with rarefaction)", H3);
			print_dataline (fr, "Average rarefacted sample size per locus", mean_Nrare_loc, decF);
			/*fr << std::endl;
			std::string header4a ("Locus | Nr");
			std::vector<std::string> rows4a;
			for (std::size_t m (0); m < loci; m++) {
				rows4a.push_back("  " + std::to_string(m+1) + "   | " + std::to_string(Nrare_loc[m]));
			}
			print_table (fr, header4a, rows4a);*/
		}

		/* ALLELIC DIVERSITY */
		else print_header(fr, "ALLELIC DIVERSITY ", H3);

		/* Parameters of subpopulations: A, D */
		if (!boot) print_header (fr, "Within-subpopulation parameters", H3);
		else print_header(fr, "Within-subpopulation parameters (bootstrap " + round_s(100.0*(1.0-alpha), decF) + "% limits)", H3);
		print_textline (fr, item_list + bold("$A_{S,i}$") + ": Within-subpopulation allelic diversity (allelic richness with rarefaction or not, minus 1).");
		//print_textline (fr, item_list + bold("$D_{A,ii}$") + ": Allelic distance between pairs of individuals within the subpopulation.");
		print_textline (fr, item_list + bold("$K_{i}$") + ": Mean number of alleles per locus in the subpopulation.");
		print_textline (fr, item_list + bold("$A_{p,i}$") + ": Mean number of private alleles per locus in the subpopulation.");
		fr << std::endl;

		std::string header4b ("Subpop. | $A_{S,i}$ | $K_{i}$ | $A_{p,i}$");
		std::vector<std::string> rows4b;
		double meanK (0.0);
		for (std::size_t i (0); i < pop; i++) {
			meanK += app[i];
			if (!boot) {
				rows4b.push_back("  " + deme[i].getLabel() +
								 "  |" + round_s(con_.as[i], decF) +
								 "| " /*+ round_s(con_.da[i], decF) + "| "*/ + round_s(app[i], decF) +
								 "| " + round_s((double)u_alleles[i]/loci, decF));
			} else {
				rows4b.push_back("  " + deme[i].getLabel() +
								 "  |" + round_s(con_.as[i], decF) + " (" + round_s(calc_boot.get_con_as(i).low, decF) + " - " + round_s(calc_boot.get_con_as(i).high, decF) + ") "
								 "| " /*+ round_s(con_.da[i], decF) + "| "*/ + round_s(app[i], decF)  + " (" + round_s(calc_boot.get_app(i).low, decF) + " - " + round_s(calc_boot.get_app(i).high, decF) + ") "
								 "| " + round_s((double)u_alleles[i]/loci, decF) + " (" + round_s((double)calc_boot.get_u_alleles(i).low/loci, decF) + " - " + round_s((double)calc_boot.get_u_alleles(i).high/loci, decF) + ") ");
			}
		}
		meanK /= pop;
		if (boot) {
			rows4b.push_back("  **Average** |" + bold(round_s(con_n.as, decF)) + " (" + round_s(calc_boot.get_avg_con_as().low, decF) + " - " + round_s(calc_boot.get_avg_con_as().high, decF) + ") "
							"| " /*+ round_s(con_n.da, decF) +
							"| "*/ + bold(round_s(meanK, decF)) + " (" + round_s(calc_boot.get_avg_app().low, decF) + " - " + round_s(calc_boot.get_avg_app().high, decF) + ") "
							"| " + bold(round_s(mean(u_alleles)/loci, decF)) + " (" + round_s(calc_boot.get_avg_u_alleles().low/loci, decF) + " - " + round_s(calc_boot.get_avg_u_alleles().high/loci, decF) + ") ");
		} else {
			rows4b.push_back("  **Average** |" + bold(round_s(con_n.as, decF)) +
							"| " /*+ round_s(con_n.da, decF) +
							"| "*/ + bold(round_s(meanK, decF)) +
							"| " + bold(round_s(mean(u_alleles)/loci, decF)));
		}
		print_table (fr, header4b, rows4b);
		if (save_tsv) {
			std::ofstream fr_tsv;
			fr_tsv.open("res_" + caso + "_within_A.tsv", std::ios::out);
			fr_tsv << "Subpop.\tA_{S,i}\tK_{i}\tA_{p,i}" << std::endl;
			for (std::size_t i (0); i < pop; i++) {
				fr_tsv << deme[i].getLabel();
				fr_tsv << '\t' << con_.as[i];
				fr_tsv << '\t' << app[i];
				fr_tsv << '\t' << (double)u_alleles[i]/loci;
				fr_tsv << std::endl;
			}
			fr_tsv.close();
		}

		/* Allelic distance between subpopulations */
		if (!boot) print_header(fr, "Between-subpopulation parameters", H3);
		else print_header(fr, "Between-subpopulation parameters (bootstrap " + round_s(100.0*(1.0-alpha), decF) + "% limits)", H3);
		print_textline (fr, item_list + bold("$D_{A,ij}$") + ": Allelic distance between pairs of subpopulations.");
		print_textline (fr, item_list + bold("$A_{ST,ij}$") + ": Allelic differentiation index between pairs of subpopulations.");

		if (!pairwise_stats) {
			std::string header4c ("Subpop. | $D_{A,ij}$ | $A_{ST,ij}$ ");
			std::vector<std::string> rows4c;
			for (std::size_t i (0); i < pop; i++) {
				for (std::size_t j (i); j < pop; j++) {
					if (i != j) {
						if (!boot) {
							rows4c.push_back(deme[i].getLabel() + " - " +
											 deme[j].getLabel() + " | " +
						                     round_s(pop_.da[i][j], decF) + " | " +
						                     round_s(pop_.as[i][j], decF));
						} else {
							rows4c.push_back(deme[i].getLabel() + " - " +
											 deme[j].getLabel() +
											 " | " + round_s(pop_.da[i][j], decF) + " (" + round_s(calc_boot.get_pop_da(i,j).low, decF) + " - " + round_s(calc_boot.get_pop_da(i,j).high, decF) + ") "
											 " | " + round_s(pop_.as[i][j], decF) + " (" + round_s(calc_boot.get_pop_as(i,j).low, decF) + " - " + round_s(calc_boot.get_pop_as(i,j).high, decF) + ") ");
						}
					}
				}
			}
			print_table (fr, header4c, rows4c);
		} else {
			print_pairwise_table (fr, "$D_{A,ij}$ pairwise table", pop_.da, deme, decF);
			print_pairwise_table (fr, "$A_{ST,ij}$ pairwise table", pop_.as, deme, decF);
		}
		if (save_tsv) {
			std::ofstream fr_tsv;
			fr_tsv.open("res_" + caso + "_between_A.tsv", std::ios::out);
			fr_tsv << "Subpop.\tD_{A,ij}\tA_{ST,ij}" << std::endl;
			for (std::size_t i (0); i < pop; i++) {
				for (std::size_t j (i); j < pop; j++) {
					if (i != j) {
						fr_tsv << deme[i].getLabel() + " - " + deme[j].getLabel();
						fr_tsv << '\t' << pop_.da[i][j];
						fr_tsv << '\t' << pop_.as[i][j];
						fr_tsv << std::endl;
					}
				}
			}
			fr_tsv.close();
		}

		/* Contribution of each subpopulation to total allelic diversity */
		/*print_header (fr, "Contribution of each subpopulation to total allelic diversity of the population :", H3);
		print_textline (fr, item_list + bold("$AD_{WS}$") + ": Allelic diversity within populations.");
		print_textline (fr, item_list + bold("$AD_{BS}$") + ": Allelic diversity between populations.");
		print_textline (fr, item_list + bold("$AD_{T}$") + ": Total allelic diversity.");
		std::string header4c ("Subpop. | $AD_{WS}$ | $AD_{BS}$ | $AD_{T}$ ");
		std::vector<std::string> rows4c;
		for (std::size_t i (0); i < pop; i++) {
			rows4c.push_back("  " + deme[i].getLabel() + "  |   " + round_s(con_.as[i] / pop, decF) + "   |   " + round_s(con_.da[i] / pop, decF) + "   |  " + round_s((con_.as[i] + con_.da[i]) / pop, decF));
		}
		rows4c.push_back(" Total  |   " + round_s(A.as, decF) + "   |   " + round_s(A.da, decF) + "   |  " + round_s(A.as + A.da, decF));
		print_table (fr, header4c, rows4c);*/

		/* Average allelic diversity parameters */
		if (!boot)  print_header(fr, "Average allelic diversity parameters", H3);
		else print_header(fr, "Average allelic diversity parameters (bootstrap " + round_s(100.0*(1.0-alpha), decF) + "% limits)", H3);
		if (!boot) {
			print_dataline (fr, item_list + "$A_{S}$: Average allelic diversity within subpopulations", con_n.as, decF);
			print_dataline (fr, item_list + "$D_{A}$: Average allelic diversity between subpopulations", con_n.da, decF);
			print_dataline (fr, item_list + "$A_{T}$: Total allelic diversity", con_n.as+con_n.da, decF);
		} else {
			print_bootline (fr, item_list + "$A_{S}$: Average allelic diversity within subpopulations", con_n.as, decF, calc_boot.get_AS());
			print_bootline (fr, item_list + "$D_{A}$: Average allelic diversity between subpopulations", con_n.da, decF, calc_boot.get_DA());
			print_bootline (fr, item_list + "$A_{T}$: Total allelic diversity", con_n.as+con_n.da, decF, calc_boot.get_AT());
		}

		/* Ast-statistics */
		if (!boot) {
			print_header(fr, "$A_{ST}$-statistic", H3);
			print_dataline (fr, item_list + "$A_{ST}$", AST, decF);
		} else {
			print_header(fr, "$A_{ST}$-statistic (bootstrap " + round_s(100.0*(1.0-alpha), decF) + "% limits)", H3);
			print_bootline (fr, item_list + "$A_{ST}$", AST, decF, calc_boot.get_AST());
		}

		/* Allelic diversity loss or gain removing a subpopulation */
		print_header (fr, "Percentage of loss (+) or gain (-) of allelic diversity after removal of each subpopulation", H3);
		std::string header4d ("Subpop. removed | $A_{S}$ | $D_{A}$ | $A_{T}$ ");
		std::vector<std::string> rows4d;
		for (std::size_t i (0); i < pop; i++) {
			if (!boot) {
				rows4d.push_back("      " + deme[i].getLabel() +
								 "      |  " + round_s((-(rem_.as[i]-A.as)/(A.as+A.da) *100), decF) +
								 "   |  " + round_s((-(rem_.da[i]-A.da)/(A.as+A.da) * 100), decF) +
								 "    |  " + round_s((-((rem_.as[i]+rem_.da[i])-(A.as+A.da))/(A.as+A.da) *100), decF));
			} else {
				rows4d.push_back("      " + deme[i].getLabel() +
								 "      |  " + round_s((-(rem_.as[i]-A.as)/(A.as+A.da) *100), decF) + " (" + round_s(calc_boot.get_rem_AS(i).low, decF) + " - " + round_s(calc_boot.get_rem_AS(i).high, decF) + ") " +
								 "   |  " + round_s((-(rem_.da[i]-A.da)/(A.as+A.da) * 100), decF) + " (" + round_s(calc_boot.get_rem_DA(i).low, decF) + " - " + round_s(calc_boot.get_rem_DA(i).high, decF) + ") " +
								 "    |  " + round_s((-((rem_.as[i]+rem_.da[i])-(A.as+A.da))/(A.as+A.da) *100), decF) + " (" + round_s(calc_boot.get_rem_AT(i).low, decF) + " - " + round_s(calc_boot.get_rem_AT(i).high, decF) + ") ");
			}
		}
		print_table (fr, header4d, rows4d);
		if (save_tsv) {
			std::ofstream fr_tsv;
			fr_tsv.open("res_" + caso + "_contribution_A.tsv", std::ios::out);
			fr_tsv << "Subpop. removed\tA_{S}\tD_{A}\tA_{T}" << std::endl;
			for (std::size_t i (0); i < pop; i++) {
				fr_tsv << deme[i].getLabel();
				fr_tsv << '\t' << (-(rem_.as[i]-A.as)/(A.as+A.da) *100);
				fr_tsv << '\t' << (-(rem_.da[i]-A.da)/(A.as+A.da) * 100);
				fr_tsv << '\t' << (-((rem_.as[i]+rem_.da[i])-(A.as+A.da))/(A.as+A.da) *100);
				fr_tsv << std::endl;
			}
			fr_tsv.close();
		}

		/* Ast and allelic distance between subpopulations */
		/*print_header (fr, "$A_{ST}$ between pairs of subpopulations :", H3);
		if (!pairwise_stats) {
			std::string header4e ("Subpop. | $A_{ST}$ ");
			std::vector <std::string> rows4e;
			for (std::size_t i (0); i < pop; i++) {
				for (std::size_t j (i); j < pop; j++) {
					if (i != j) {
						rows4e.push_back(deme[i].getLabel() + " - " + deme[j].getLabel() + "| " + round_s(pop_.as[i][j], decF));
					}
				}
			}
			print_table (fr, header4e, rows4e);
		} else {
			print_pairwise_table (fr, "$A_{ST}$ pairwise table", pop_.as, deme, decF);
		}*/
	}

	fr.close();

}

/* ********************************************************************************************
 * save_synthetic ()
 * --------------------------------------------------------------------------------------------
 * Saves the contributions of each subpopulation to a synthetic pool.
 * ********************************************************************************************/
void save_synthetic (std::string filename, const MTPConfigFile& config, const std::vector<population>& deme, std::vector<Synthetic> synth, std::size_t nPool, vdou1D eneopt, const vint2D& solutionOpt) {

	std::size_t pop (deme.size ());
	std::string caso (config.get_caso());
	int decF (config.get_decF());
	bool save_tsv (config.get_save_tsv());
	std::ofstream fr;
	fr.open(filename, std::ios::app);
	if (!fr) std::cout << "Problem creating results file" << std::endl;

	// Print Header
	print_header (fr, "SYNTHETIC POPULATION", H3);
	fr << std::endl << "Percentage of individuals contributing from each subpopulation";
	fr << " to a pool of " << nPool << " individuals with maximal ";

	if (synth.size() > 1) {
		fr << "expected heterozygosity ($H$) and total number of alleles ($K$)";
	/*} else if (synth[0] == GW) {
		fr << "gene diversity within subpopulations (GD_WS)";
	} else if (synth[0] == GB) {
		fr << "gene diversity between subpopulations (GD_BS)";
	} else if (synth[0] == GT) {
		fr << "total gene diversity (GD_T)";
	} else if (synth[0] == AW) {
		fr << "allelic diversity within subpopulations (AD_WS)";
	} else if (synth[0] == AB) {
		fr << "allelic diversity between subpopulations (AD_BS)";
	} else if (synth[0] == AT) {
		fr << "total allelic diversity (AD_T)";*/
	} else if (synth.size() && synth[0] == NA) {
		fr << "total number of alleles ($K$)";
	} else if (synth.size() && synth[0] == HE) {
		fr << "expected heterozygosity ($H$)";
	}
	fr << "." << md_endl << std::endl << std::endl;

	// Print table
	std::string header;
	std::vector<std::string> rows;
	if (synth.size() > 1) {
		header = "Subpopulation | $H$ | $K$ ";
		for (std::size_t i (0); i < pop; i++) {
			std::string contrib ("      " + deme[i].getLabel() + "   ");
			for (std::size_t j(0); j < synth.size(); ++j) {
				double percentage (((double)solutionOpt[j][i]/nPool)*100.0);
				contrib += " | " + round_s(percentage, decF);
			}
			rows.push_back(contrib);
		}
	} else {
		header = "Subpopulation | Contribution";
		for (std::size_t i (0); i < pop; i++) {
			double percentage (((double)solutionOpt[0][i]/nPool)*100.0);
			rows.push_back("      " + deme[i].getLabel() + "    |    " + round_s(percentage, decF));
		}
	}
	print_table (fr, header, rows);
	if (save_tsv) {
		std::ofstream fr_tsv;
		fr_tsv.open("res_" + caso + "_synthetic.tsv", std::ios::out);
		if (synth.size() > 1) {
			fr_tsv << "Subpop. removed\tH\tK" << std::endl;
			for (std::size_t i (0); i < pop; i++) {
				fr_tsv << deme[i].getLabel();
				for (std::size_t j(0); j < synth.size(); ++j) {
					double percentage (((double)solutionOpt[j][i]/nPool)*100.0);
					fr_tsv << '\t' << percentage;
				}
				fr_tsv << std::endl;
			}
		} else {
			fr_tsv << "Subpop. removed\tContribution" << std::endl;
			for (std::size_t i (0); i < pop; i++) {
				double percentage (((double)solutionOpt[0][i]/nPool)*100.0);
				fr_tsv << deme[i].getLabel();
				fr_tsv << '\t' << percentage;
			}
			fr_tsv << std::endl;
		}
		fr_tsv.close();
	}

	// Print energy
	/*fr << "Expected values in the pool :  " << std::endl;
	if (synth.size() > 1) {
		fr << "H: " << -eneopt[0] << md_endl << std::endl;
		fr << "GD_WS: " << -eneopt[1] << md_endl << std::endl;
		fr << "GD_BS: " << -eneopt[2] << md_endl << std::endl;
		fr << "AD_T: " << -eneopt[3] << md_endl << std::endl;
		fr << "AD_WS: " << -eneopt[4] << md_endl << std::endl;
		fr << "AD_BS: " << -eneopt[5] << md_endl << std::endl;
		fr << "Mean number of alleles per locus (K): " << -eneopt[6] << md_endl << std::endl;
	} else if (synth[0] == GT || synth[0] == HE) {
		fr << "H: ";
    } else if (synth[0] == GW) {
		fr << "GD_WS: ";
    } else if (synth[0] == GB) {
		fr << "GD_BS: ";
    } else if (synth[0] == AT) {
		fr << "AD_T: ";
    } else if (synth[0] == AW) {
		fr << "AD_WS: ";
    } else if (synth[0] == AB) {
		fr << "AD_BS: ";
    } else if (synth[0] == NA) {
		fr << "Mean number of alleles per locus (K): ";
    }
	if (synth.size()==1) {
		fr << -eneopt[0] << md_endl << std::endl;
	}*/
    fr.close();
}

/* ********************************************************************************************
 * printSolution ()
 * --------------------------------------------------------------------------------------------
 * Neceista <energyOpt>, y muchímas variables de tipo <F>
 * ********************************************************************************************/
void printSolution ( const MTPConfigFile& config, Metapop& metapop, const Calculations& calc, std::string output, vint3D& sol, const vdou2D& f, double minb, vint2D& padopt, int simR, int simT, double energyOpt) {

	/* LOCAL VARIABLES */
	std::string caso (config.get_caso());
	std::size_t decF (config.get_decF());
	bool simMode (config.get_simMode());
	bool use_molecular_markers (metapop.get_use_molecular_markers());
	bool pairwise_stats (config.get_pairwise_stats());
	std::size_t pop (metapop.get_n_subpops());
	std::size_t loci (metapop.get_n_loci());
	int totalN (metapop.get_N(false));
	vshort2D offspring (metapop.get_progeny());
	vshort2D struc (metapop.get_struc());
	std::vector<population> deme (metapop.get_deme());

	std::size_t nmigrants (0); // total number of migrants
	vint2D migraTable; // matrix of migrants, with rows indicating origin population, and destination in columns
	vint1D femContrib; // a vector containing (ordered) contribution of the females (only those who actually contribute)
	vint1D fam(totalN, -1);
	vint1D indexMo;
	vint1D indexFa;
	std::vector<std::string> genMo;
	std::vector<std::string> genFa;

	/* VARIABLES FROM CALCULATIONS */
	double old_f (calc.get_tot_n().f);
	Fisher <double> F_n (calc.get_F_n());
	double AS (calc.get_A().as);
	double DA (calc.get_A().da);
	double AST (calc.get_Ast());
	double Kmed (calc.get_Kmed());
	G_parameters <double> bar_n (calc.get_bar_n());

	/* OPEN FILE */
	std::ofstream fr;
	fr.open(output.data(), ios::app);
	if (!fr) print_error("Problem creating results file");

	/* COMPUTE MIGRATION TABLE AND FEMALE CONTRIBUTIONS */
	migraTable.assign (pop, std::vector<int> (pop, 0));
	for (std::size_t p0 (0); p0 < pop; p0++) {
		int x (struc[p0][0]);
		for (std::size_t dam (0); dam < deme[p0].getNperSex(0); ++dam) {
			int sum (0);
			for (std::size_t p (0); p < pop; p++) {
				int cc (sol[x + dam][p][0] + sol[x + dam][p][1]);
				sum += cc;
				if (p0!=p) {
					nmigrants += cc;
				}
				if (simMode) {
					migraTable[p0][p] += cc;
				}
			}
			if (sum) {
				femContrib.push_back (sum);
			}
		}
	}
	double meanCont (mean(femContrib));
	vdou1D diff (femContrib - meanCont);
	double varCont (var(diff));

	/* 4. MATING MATRIX */
	std::string header_m ("Subpop. | Family | fem. | mal. ");
	std::vector<std::string> rows_m;
	if (!simMode) {
		print_header (fr, "2. Mating matrix", H2);
		for (size_t p (0); p < pop; p++) {
			header_m += " | " + deme[p].getLabel() + " | ";
			for (std::size_t j(0); j<deme[p].getLabel().size(); ++j) {
				header_m += ' ';
			}
		}
	}

	int n_family (0); // family number
	for (std::size_t p0 (0); p0 < pop; p0++) {
		int x (struc[p0][0]);
		for (std::size_t dam (0); dam < deme[p0].getNperSex(0); ++dam) {
			int sum (0);
			for (std::size_t p (0); p < pop; p++) {
				sum += (sol[x + dam][p][0] + sol[x + dam][p][1]);
			}
			if (sum) {
				std::string row;
				indexMo.push_back(x+dam);
				indexFa.push_back(padopt[p0][dam] + deme[p0].getNperSex(0));
				genMo.push_back( deme[p0].getIndividual(dam).getGenAsStr() );
				genFa.push_back( deme[p0].getIndividual(padopt[p0][dam] + deme[p0].getNperSex(0)).getGenAsStr() );
				if (!simMode) {
					row = "  " + deme[p0].getLabel() + "  |   " + std::to_string(n_family+1) + "    |  " + std::to_string(deme[p0].getIndividual(dam).getID()) + "  |   " + std::to_string(deme[p0].getIndividual(padopt[p0][dam] + deme[p0].getNperSex(0)).getID());
					for (std::size_t p (0); p < pop; p++) {
						row += "  |   " + std::to_string(sol[x + dam][p][0]) + "  |   " + std::to_string(sol[x + dam][p][1]);
					}
				}
				fam[x + dam] = n_family;
				++n_family;
				rows_m.push_back(row);
			}
		}
	}

	if (!simMode) {
		print_table (fr, header_m, rows_m);
		print_dataline (fr, "Energy", energyOpt, decF);
		print_dataline (fr, "Migrations", nmigrants, decF);
		print_dataline (fr, "Mean (female) contributions", meanCont, decF);
		print_dataline (fr, "Variance of (female) contributions", varCont, decF);
	}

	/* 5. GENERATE NEW GENEALOGY (Input) */
	std::ostringstream dat;
	std::vector<std::vector<std::size_t>> geneal;
	if (simMode || config.get_print_coan()) {

		std::size_t pos (0);
		std::size_t familyacc2 (0);
		vshort2D new_offspring (offspring);
		std::vector<bool> new_subpops (deme.size(),true);
		int new_n_subpops (deme.size());
		std::deque<int> ts (metapop.get_mng_t());
		if (ts.size() && ts[0]==(simT+1)) {
			new_n_subpops = 0;
			std::deque<vshort2D> offspring_list (metapop.get_mng_t_offspring());
			new_offspring = offspring_list[0];
			for (std::size_t i(0); i<new_offspring.size(); ++i) {
				if (new_offspring[i][0]+new_offspring[i][1]) ++new_n_subpops;
				else new_subpops[i] = false;
			}
			metapop.pop_mng_t();
		}

		dat.str("");
		dat << "n " << new_n_subpops << "\nnloci " << loci << std::endl;

		for (std::size_t m (0); m < deme.size(); m++) {

			if (!new_subpops[m]) continue;
			dat << deme[m].getLabel() << std::endl << "N " << (offspring[m][0] + offspring[m][1]) << std::endl;
			dat << "Ne " << (offspring[m][0] + offspring[m][1]) << std::endl;
			dat << "newN " << new_offspring[m][0] << " " << new_offspring[m][1] << std::endl;

			int familyacc (familyacc2);
			for (std::size_t j (0); j < 2; j++) {
				std::size_t family2 (familyacc);

				for (std::size_t i (0); i < deme.size(); i++) {
					std::size_t family (0);

					for (std::size_t k (struc[i][0]); k < (struc[i][0] + deme[i].getNperSex(0)); k++) {

						++family;
						if ((m == i) || (sol[k][m][j] > 0)) ++family2;
						for (int l (0); l < (sol[k][m][j]); l++) {
							std::vector<std::size_t> geneal_row;
							geneal_row.push_back(indexMo[fam[k]]);
							geneal_row.push_back(indexFa[fam[k]]);
							geneal.push_back(geneal_row);
							++pos;
							dat << pos << " [" << fam[k]+1 << "] " << j << generaOff(loci, genMo[fam[k]], genFa[fam[k]]) << std::endl;
						}
					}
				}
				if (family2 > familyacc2) familyacc2 = family2;
			}
		}
	}

	if ((simMode & !use_molecular_markers) || config.get_print_coan()) {

		std::ofstream nf_file;
		if (config.get_print_coan()) {
			std::string sc = "res_" + caso + "_rmatrix_managed.txt";
			if (simMode) sc = "res_" + caso + "_rmatrix_Rep" + std::to_string(simR+1) + "_Gen" + std::to_string(simT+1) + ".txt";
			nf_file.open(sc, std::ios::out);
			nf_file << "matrix" << std::endl;
			if (!nf_file) print_error("Problem creating rmatrix file");
		}

		// Generate new matrix and print it
		vdou2D neoF;
		int a, b, c, d;
		int ac, ad, bc, bd, ca, cb, da, db;
		double sumf (0.0), sumd (0.0);
		if (simMode & !use_molecular_markers) dat << std::endl << "matrix" << std::endl;

		for (std::size_t i (0); i < geneal.size(); ++i) {
			vdou1D row_neoF;
			for (std::size_t j (0); j < i; ++j) {

				a = geneal[i][0];
				b = geneal[i][1];
				c = geneal[j][0];
				d = geneal[j][1];

				//To ensure the half matrix is not overflown
				if (a > c) {
					ac = a;
					ca = c;
				} else {
					ac = c;
					ca = a;
				}
				if (a > d) {
					ad = a;
					da = d;
				} else {
					ad = d;
					da = a;
				}
				if (b > c) {
					bc = b;
					cb = c;
				} else {
					bc = c;
					cb = b;
				}
				if (b > d) {
					bd = b;
					db = d;
				} else {
					bd = d;
					db = b;
				}

				double neof_ij (0.25 * (f[ac][ca] + f[ad][da] + f[bc][cb] + f[bd][db]));
				row_neoF.push_back(neof_ij);
				sumf += 2.0*neof_ij;
				if (simMode & !use_molecular_markers) dat << neof_ij << " ";
				if (config.get_print_coan()) {
					nf_file << neof_ij;
					if (j<i) nf_file << " ";
				}
			}
			double neof_ii (0.0);
			if (geneal[i][0] > geneal[i][1]) {
				neof_ii = 0.5 * (1.0 + f[geneal[i][0]][geneal[i][1]]);
			} else {
				neof_ii = 0.5 * (1.0 + f[geneal[i][1]][geneal[i][0]]);
			}
			row_neoF.push_back(neof_ii);
			sumf += neof_ii;
			sumd += neof_ii;
			neoF.push_back(row_neoF);
			if (simMode & !use_molecular_markers) dat << neof_ii << std::endl;
			if (config.get_print_coan()) nf_file << neof_ii << std::endl;
		}
		nf_file.close();
	}

	//Establish the input for the new generation
	if (simMode) metapop.update (dat.str(), true);
	fr.close();

	/* SAVE SIMULATION RESULTS */
	if (simMode) {

		std::ofstream sf;
		std::string caso (config.get_caso());
		bool saveStats (config.get_saveStats());
		bool saveMigra (config.get_saveMigra());

		/* SAVE SIMULATION STATS (res_[name]_simStats.dat) */
		if (saveStats) {
			std::string ss = "res_" + caso + "_simStats.tsv";
			if (!simR && !simT) sf.open(ss.data(), ios::out);
			else sf.open(ss.data(), std::ios::app);
			if (!sf) print_error("Problem creating results file for simulations");

			std::string header;
			if (!simR && !simT) {
				sf << "Rep" + delim + "Gen" + delim + "F" + delim + "f" + delim + "s" + delim + "D" + delim + "Hs" + delim + "Dg" + delim + "Ht" + delim + "Fis" + delim + "Fst" + delim + "Fit" + delim + "As" + delim + "Da" + delim + "At" + delim + "Ast" + delim + "K" + delim + "nMigrants" + delim + "meanCont(fem)" + delim + "VarCont(fem)" + delim + "nMates" << std::endl;
			}

			sf << simR << delim;
			sf << simT << delim;
			sf << bar_n.F << delim;
			sf << bar_n.f << delim;
			sf << bar_n.s << delim;
			sf << bar_n.d << delim;
			sf << (1.0-bar_n.f) << delim;
			sf << bar_n.f-old_f << delim;
			sf << (1.0 - old_f) << delim;
			sf << F_n.is << delim;
			sf << F_n.st << delim;
			sf << F_n.it << delim;
			sf << AS << delim;
			sf << DA << delim;
			sf << (AS+DA) << delim;
			sf << AST << delim;
			sf << Kmed << delim;
			sf << nmigrants << delim;
			sf << meanCont << delim;
			sf << varCont << delim;
			sf << femContrib.size();
			sf << std::endl;
			sf.close();
		}

		/* SAVE MIGRATION TABLE (res_[name]_migTable.dat) */
		if (saveMigra) {
			std::string ss ("res_" + caso + "_migTable.tsv");
			if (!simR && !simT) sf.open(ss.data(), std::ios::out);
			else sf.open(ss.data(), ios::app);
			if (!sf) print_error("Problem creating results file for simulations");

			if (!pairwise_stats) {

				// Header
				if (!simR && !simT) {
					sf << "Rep" << delim << "Gen";
					for (std::size_t i(0); i<pop; i++){
						for(std::size_t j(0); j<pop; j++) {
							sf << delim << deme[i].getLabel() + "_" + deme[j].getLabel();
						}
					}
					sf << std::endl;
				}

				// Body (rep=R, gen=T)
				sf << simR << delim << simT;
				for(std::size_t i(0); i<pop; i++) {
					for(std::size_t j(0); j<pop; j++) {
						sf << delim << migraTable[i][j];
					}
				}
				sf << std::endl;
				sf.close();

			} else {

				// Header
				if (!simR && !simT) {
					sf << "Rep" << delim << "Gen" << delim << "From/To";
					for (std::size_t i(0); i<pop; ++i) {
						sf << delim << deme[i].getLabel();
					}
					sf << std::endl;
				}

				// Body
				for (size_t i(0); i<migraTable.size(); ++i) {
					if (i==0) {
						sf << simR << delim << simT;
					} else {
						sf << delim; 
					}
					sf << delim << deme[i].getLabel() << delim;
					for (size_t j(0); j<migraTable[i].size(); ++j) {
						sf << migraTable[i][j] << delim;
					}
					sf << std::endl;
				}
				sf.close();
			}
		}
	}

}

/* ********************************************************************************************
 * save_tmatrix ()
 * --------------------------------------------------------------------------------------------
 * Saves a triangular matrix
 * ********************************************************************************************/
void save_tmatrix (std::ofstream& out, const vdou2D& matrix) {
	out << "matrix" << std::endl;
	for (std::size_t i(0); i<matrix.size(); ++i) {
		out << matrix[i][0];
		for (std::size_t j(1); j<=i; ++j) {
			out << " " << matrix[i][j];
		}
		out << std::endl;
	}
}

/* ********************************************************************************************
 * generaOff ()
 * --------------------------------------------------------------------------------------------
 * 
 * ********************************************************************************************/
std::string generaOff (std::size_t loci, std::string genMo, std::string genFa) {

	std::string cad ("");
	int *m1 = new int[loci];
	int *m2 = new int[loci];
	int *f1 = new int[loci];
	int *f2 = new int[loci];

	if (loci < 1) return cad;

	std::stringstream input(genMo, std::stringstream::in);
	for (std::size_t l (0); l < loci; l++) {
		input >> m1[l];
		input >> m2[l];
	}
	std::stringstream input2(genFa, stringstream::in);
	for (std::size_t l (0); l < loci; l++) {
		input2 >> f1[l];
		input2 >> f2[l];
	}

	for (std::size_t l (0); l < loci; l++) {
		int a;
		if (random_dist(random_generator) < 0.5) a = m1[l];
		else a = m2[l];
		int b;
		if (random_dist(random_generator) < 0.5) b = f1[l];
		else b = f2[l];
		char *add = new char[18];
		sprintf(add, " %d %d", a, b);
		cad = cad.append(add);
		delete [] add;
	}

    delete [] m1;
    delete [] m2;
    delete [] f1;
    delete [] f2;

    return cad;
}
