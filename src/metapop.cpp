/* =======================================================================================
 *                                                                                       *
 *       Filename:  metapop.cpp                                                          *
 *                                                                                       *
 *    Description: This file defines the object class Metapop, which represents a deme   *
 *                 of subpopulations.                                                    *
 *                                                                                       *
 *                 Methods are defined to perform genetic analysis of diversity in       *
 *                 subdivided populations following Caballero & Toro 2002                *
 *                                                                                       *
 *                  -> Metapop::Metapop ()                                               *
 *                  -> Metapop::input_* ()                                               *
 *                  -> Metapop::calculate ()                                             *
 *                  -> Metapop::informative* ()                                          *
 *                  -> Metapop::get_* ()                                                 *
 *                  -> Metapop::free_* ()                                                *
 *                                                                                       *
 ======================================================================================= */

#include "metapop.h"


/* ********************************************************************************************
 * Metapop::Metapop ()
 * --------------------------------------------------------------------------------------------
 * Class MTPInput constructor. Checks and read the input datafile
 * ********************************************************************************************/
Metapop::Metapop (std::string file_name, const MTPConfigFile& config) {

	// Check and read input file
	std::ifstream file (file_name);
	if (!file.is_open()) {
		std::cerr << "INPUT ERROR. Expected data file called '"<< file_name.data() << "' but it doesn't exist." << std::endl;
		exit(-1);
	}

	// Config
	for_management = false;
	input_format = config.get_datafile_format();
	ploidy = config.get_ploidy();
	if (ploidy > 2) print_warning("Use of poliploid data is experimental.");
	if (config.get_manage() != NONE) for_management = true;
	fmatrix_read = false;
	temporal_marks = false;
	restricted_mates = false;
	forbidden_mates = false;
	read_pedigree = false;
	progeny_set = false;
	for_simulation = config.get_simMode();
	sim_gens = config.get_simGens();
	if (config.get_datafile_sformat()=="pedigree") read_pedigree = true;

	// Read and format input file
	bool compute_coancestry (true);
	if (config.get_convert_mode() != "none") compute_coancestry = false;
	use_nucleotides = config.get_use_nucleotides();
	std::string data ((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
	file.close();
	clean_datafile(data);
	filename = file_name;

	if (input_format == GNP) input_genepop (data);
	else if (input_format == PED) input_pedfile (data, read_pedigree, config.get_rm_file());
	else this->update (data, compute_coancestry); // input_metapop
	if (for_simulation) use_nucleotides = false;

	// Read management file
	std::string mng_file (config.get_mng_file());
	if (mng_file != "") {
		std::ifstream mng (mng_file);
		if (!mng.is_open()) {
			std::cerr << "INPUT ERROR. Expected management file called '"<< mng_file.data() << "' but it doesn't exist." << std::endl;
			exit(-1);
		}
		std::string mng_data ((std::istreambuf_iterator<char>(mng)), std::istreambuf_iterator<char>());
		mng.close();
		clean_datafile(mng_data);
		input_management (mng_data);
	} else if (for_management) {
		if (!progeny.size()) default_mng();
		if (progeny.size() != n_subpops) print_error (ERROR_MTP_NEXTG);
	}
	test_mng();

	// Read relationship matrix file
	std::string rm_file (config.get_rm_file());
	if (rm_file != "") {
		std::ifstream rm (rm_file);
		if (!rm.is_open()) {
			std::cerr << "INPUT ERROR. Expected relationship matrix file called '"<< rm_file.data() << "' but it doesn't exist." << std::endl;
			exit(-1);
		}
		std::string rm_data ((std::istreambuf_iterator<char>(rm)), std::istreambuf_iterator<char>());
		rm.close();
		clean_datafile(rm_data);
		std::stringstream fr_data (rm_data, std::stringstream::in);
		read_rm (fr_data);
		test_rm();
	}
	test_final();
}

Metapop::Metapop ( std::string file_name, const MTPConfigFile& config, std::string mode) { // constructor used for conversion

	// Check and read input file
	std::ifstream file (file_name);
	if (!file.is_open()) {
		std::cerr << "INPUT ERROR. Expected data file called '"<< file_name.data() << "' but it doesn't exist." << std::endl;
		exit(-1);
	}

	// Local options
	for_management = false;

	// Read and format input file
	ploidy = config.get_ploidy();
	if (ploidy > 2) print_warning("Use of poliploid data is experimental.");
	use_nucleotides = config.get_use_nucleotides();
	std::string data ((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
	file.close();
	rm_char_in_str(data,'\r');
	std::replace (data.begin(), data.end(), '\t', ' ');
	data = std::regex_replace(data, std::regex("^ +| +$|( ) +"), "$1"); // remove leading, trailing and duplicated spaces
	filename = file_name;
	input_format = config.get_datafile_format();

	if (mode == "gp2mtp") input_genepop (data);
	else if (mode == "ped2mtp") input_pedfile (data, false,"");
	else {
		std::cerr << "Unknown conversion mode. Select one of 'none', 'mtp2gp' or 'mtp2ped'." << std::endl;
		exit(-1);
	}
}

Metapop::Metapop ( const Metapop& original) { // Copy constructor for bootstrap

	// Copy most basic attributes
	input_format = original.input_format;
	for_management = original.for_management;
	ploidy = original.ploidy;
	if (ploidy > 2) print_warning("Use of poliploid data is experimental.");
	filename = original.filename;
	n_subpops = original.n_subpops;
	n_loci = 0;
	std::size_t tmp_loci (original.n_loci);
	N = original.N;
	Ne = original.Ne;
	progeny = original.progeny;
	use_molecular_markers = true;

	// Deme bootstrap sample (loci sampling)
	vint1D loci_sequence;
	std::uniform_int_distribution<int> sample_loci (0, tmp_loci-1);
	for (std::size_t i(0); i<tmp_loci; ++i) {
		loci_sequence.push_back(sample_loci(random_generator));
	}
	for (std::size_t i(0); i<original.deme.size(); ++i) {
		std::vector<individual> v_Inds;
		for (std::size_t j(0); j<original.deme[i].getPopulationSize(); ++j) {
			individual Ind (original.deme[i].getIndividual(j));
			std::size_t IDi (Ind.getID());
			std::string GIDi (Ind.getLabel());
			unshort sex (Ind.getSex());
			std::vector <locus> loci;
			for (std::size_t k(0); k<Ind.getLociNumber(); ++k) {
				append_alleles (Ind.getLocus(loci_sequence[k]).getAllels(), loci.size());
				loci.push_back (locus(Ind.getLocus(loci_sequence[k]).getAllels()));
			}
			n_loci = loci.size();
			v_Inds.push_back(individual(GIDi, IDi, sex, loci));
		}
		deme.push_back(population(original.deme[i].getLabel(),v_Inds));
	}

	// Additional calculations
	if (n_loci != tmp_loci) print_error (ERROR_MTP_BOOTL);
	n_loci = original.n_loci;
	clean_alleles ();
	//this->coancestry_matrix();
	set_struc ();

}

/* ********************************************************************************************
 * MTPInput::update ()
 * --------------------------------------------------------------------------------------------
 * Read a string with the content of the datafile, and update its attributes.
 * ********************************************************************************************/
void Metapop::update (std::string file, bool compute_coancestry) {

	// Initialize variables
	n_subpops = 0;
	n_loci = 0;
	N = 0;
	Ne = 0;
	use_molecular_markers = true;
	progeny.clear();
	deme.clear();
	alleles_per_locus.clear();

	// Start debug output
	if (DEBUG) {
		std::string format (translate_format());
		std::cout << "READING DATAFILE:" << std::endl;
		std::cout << "Format: " << format << std::endl;
	}

	// Read metapop file
	this->input_metapop(file);
	//if (!compute_coancestry) return;

	// Calculate coancestry matrix from molecular markers (if it hasn't been inputed)
	// should this be part of metapop class?
	if (use_molecular_markers) {
		//this->coancestry_matrix();
	}

	// Set struc for management
	set_struc ();

}

/* ********************************************************************************************
 * Metapop::check ()
 * --------------------------------------------------------------------------------------------
 * This function looks for incompatibilities between the config and input data file.
 * ********************************************************************************************/
void Metapop::check (const MTPConfigFile& config) {

	// Test options for analysis using input coancestry matrix
	if (!use_molecular_markers) {
		this->test_synth_inpmatrix (config.get_synth());
		this->test_management_inpmatrix (config.get_manage());
	}

	if (config.get_bt_n() && ((!use_molecular_markers || n_loci < 2))) print_error("At least two loci are required for running bootstrap.");

}

/* ********************************************************************************************
 * Metapop::Metapop ()
 * --------------------------------------------------------------------------------------------
 * Class Metapop constructor. Creates a deme from the input files and calculates genetic
 * parameters in the metapopulation
 * ********************************************************************************************/
void Metapop::compute_diversity ( ) {

	/* CALCULATE INDIVIDUAL FREQUENCIES () */
	if (use_molecular_markers) {
		calculate_ind_freq ();
		coancestry_from_freq();
	}

	/* CALCULATE GENETIC PARAMETERS */
	if (use_molecular_markers) {
		calculate_p ();
		calculate_unique_alleles ();
	}

	calculate_f ();
	calculate_s ();
	calculate_d ();

	if (use_molecular_markers) {
		free_mp_vdou2D (ind_freq);
		free_2D (eval_ind_locus);
	}

}

/* ********************************************************************************************
 * Metapop::append_alleles ()
 * --------------------------------------------------------------------------------------------
 * It adds alleles to the 'alleles per locus' matrix
 * ********************************************************************************************/
void Metapop::append_alleles (const allele1D& v_alleles, std::size_t pos) {
	if (!n_loci) {
		alleles_per_locus.push_back(v_alleles);
	} else {
		for (auto ai: v_alleles) alleles_per_locus[pos].push_back(ai);
	}
}

/* ********************************************************************************************
 * Metapop::clean_alleles ()
 * --------------------------------------------------------------------------------------------
 * Remove duplicates in 'alleles per locus' matrix
 * ********************************************************************************************/
void Metapop::clean_alleles () {
	for (auto& locus: alleles_per_locus) {
		std::sort(locus.begin(), locus.end());
		auto last = std::unique(locus.begin(), locus.end());
		locus.erase(last, locus.end());
	}
}

/* ********************************************************************************************
 * Metapop::struc ()
 * --------------------------------------------------------------------------------------------
 * It sets the struc matrix used for management
 * ********************************************************************************************/
void Metapop::set_struc () {
	struc.clear();
	for (std::size_t i (0); i < n_subpops; i++) {
		std::vector<short> struc_row;
		if (!i) {
			struc_row.push_back(0);
		} else {
			struc_row.push_back(struc[i - 1][1] + deme[i - 1].getNperSex(1));
		}
		struc_row.push_back(struc_row[0] + deme[i].getNperSex(0));
		struc.push_back(struc_row);
	}
}

/* ********************************************************************************************
 * Metapop::~Metapop ()
 * --------------------------------------------------------------------------------------------
 * Class Metapop destructor.
 * ********************************************************************************************/
Metapop::~Metapop () {
	this->reset_genetic_parameters();
}

/* ********************************************************************************************
 * Metapop::reset_genetic_parameters ()
 * --------------------------------------------------------------------------------------------
 * Method that frees memory of all genetic parameters.
 * ********************************************************************************************/
void Metapop::reset_genetic_parameters () {
	this->free_bt_all();
}

/* ********************************************************************************************
 * Metapop::calculate_ind_freq ()
 * --------------------------------------------------------------------------------------------
 * Calculates the allele frequencies for every individual in the metapopulation
 * ********************************************************************************************/
void Metapop::calculate_ind_freq () {

	// A matrix of individual frequencies by locus and allele
	// ind_freq[i][l][m][k]: frequency of allele [k] of locus [m] at individual [l] of subpopulation [i]
	// eval_ind_locus[m][i][l]: can evaluate locus [m] of subpopulation [i] and individual [l]
	for (std::size_t i (0); i < n_subpops; i++) {
		vp_vdou2D row_pop;
		std::vector<std::vector<bool>*> eval_row_pop;
		for (std::size_t l (0); l < deme[i].getPopulationSize(); l++) {
			vdou2D row_ind;
			vbool1D eval_row_ind;
			individual Ind_i (deme[i].getIndividual(l));
			for (std::size_t m (0); m < n_loci; m++) {
				vdou1D row_locus;
				allele1D alleles (Ind_i.getLocus(m).getAllels());
				if (!is_in(alleles,(allele_t)0)) {
					for (std::size_t k (0); k < alleles_per_locus[m].size(); k++){
						std::size_t n_a (Ind_i.getLocus(m).getNAllels(alleles_per_locus[m][k]));
						row_locus.push_back((double)n_a/ploidy);
					}
					eval_row_ind.push_back(true);
				} else eval_row_ind.push_back(false);
				row_ind.push_back(row_locus);
			}
			row_pop.push_back(new vdou2D (row_ind));
			eval_row_pop.push_back(new vbool1D (eval_row_ind));
		}
		ind_freq.push_back(row_pop);
		eval_ind_locus.push_back(eval_row_pop);
	}
}

/* ********************************************************************************************
 * MTPInput::read_fmatrix ()
 * --------------------------------------------------------------------------------------------
 * Reads the matrix of coancestry between individuals from the input file
 * ********************************************************************************************/
void Metapop::read_fmatrix ( std::stringstream& str) {

	use_molecular_markers = false;
	n_loci = 0;
	fmatrix_read = true;
	if (DEBUG) std::cout << "Reading coancestry matrix..." << endl;
	f_mol.clear();
	bool triangular (false);

	// Read coancestry matrix
	std::string row;
	std::size_t row_count (0);

	while (getline (str,row)) {
		if (row=="") continue;
		std::stringstream str_row (row);
		std::string value;
		std::size_t col_count (0);
		vdou1D f_row;
		while (str_row >> value) {
			f_row.push_back(atof(value.c_str()));
			++col_count;
		}
		++row_count;
		if (row_count == col_count) triangular = true;
		if (col_count > N || (triangular && col_count > row_count)) large_cols (row_count);
		f_mol.push_back(f_row);
	}
	if (f_mol.size() < N) short_frow();
	else if (f_mol.size() > N) large_frow();

	// Get a triangular matrix
	if (!triangular) {
		vdou2D tmp_matrix (f_mol);
		f_mol.clear();
		for (std::size_t i(0); i<N; ++i) {
			vdou1D f_row;
			for (std::size_t j(0); j<=i; ++j) {
				if (tmp_matrix[i][j] != tmp_matrix[j][i]) {
					print_error(ERROR_MTP_FMSYM);
				} else if (tmp_matrix[i][j] < 0.0 || tmp_matrix[i][j] > 1.0) {
					print_error(ERROR_MTP_FMRNG);
				} else {
					f_row.push_back(tmp_matrix[i][j]);
				}
			}
			f_mol.push_back(f_row);
		}
	}
	if (DEBUG) {
		std::cout << std::endl;
		std::cout << "Done" << std::endl;
		std::cout << std::endl;
	}
}

/* ********************************************************************************************
 * MTPInput:: coancestry ()
 * --------------------------------------------------------------------------------------------
 * Calculates the matrix of molecular coancestry between individuals from individual genotypes
 * NOTE: This function is disabled, and coancestries are now computed from allele frequencies
 * ********************************************************************************************/
/*void Metapop::coancestry_matrix () {

	if (DEBUG) std::cout << "Calculating coancestry matrix..." << endl;
	f_mol.clear();
	double partial_f (1.0/(ploidy*ploidy));

	std::size_t ind_ID (1);
	for (std::size_t i(0); i < n_subpops; ++i) {
		for (std::size_t ind_i (0), N_i (deme[i].getPopulationSize()); ind_i < N_i; ++ind_i) {
			vdou1D f_vector;
			individual Ind_i (deme[i].getIndividual(ind_i));
			if (DEBUG) std::cout << "\r  Individual " << ind_ID << " / " << N;
			for (std::size_t j(0); j<i+1; ++j) {
				for (std::size_t ind_j(0), N_j(deme[j].getPopulationSize()); ind_j < N_j; ++ind_j) {
					double f (0.0);
					size_t eval_loci (n_loci);
					individual Ind_j (deme[j].getIndividual(ind_j));
					for (std::size_t k(0); k<n_loci; ++k) {
						bool all_loci (true);
						double tmpf (0.0);
						for (std::size_t a_i(0); a_i<ploidy; ++a_i) {
							if (!Ind_i.getLocus(k).getA(a_i)) { all_loci = false; break; }
							for (std::size_t a_j(0); a_j<ploidy; ++a_j) {
								if (!Ind_j.getLocus(k).getA(a_j)) { all_loci = false; break; }
								else if (Ind_i.getLocus(k).getA(a_i) == Ind_j.getLocus(k).getA(a_j)) tmpf += partial_f;
							}
						}
						if (!all_loci) --eval_loci; // If no allele, no calculation for that loci
						else f += tmpf;
					}
					f /= eval_loci;
					f_vector.push_back(f);
				}
			}
			++ind_ID;
			f_mol.push_back(f_vector);
		}
	}

	if (DEBUG) {
		std::cout << std::endl;
		std::cout << "Done" << std::endl;
		std::cout << std::endl;
	}
}*/

/* ********************************************************************************************
 * MTPInput:: coancestry_from_freq ()
 * --------------------------------------------------------------------------------------------
 * Calculates the matrix of molecular coancestry from allele frequencies
 * ********************************************************************************************/
void Metapop::coancestry_from_freq () {

	f_mol.clear();
	for (std::size_t i(0); i < n_subpops; ++i) {
		for (std::size_t ind_i (0), N_i (deme[i].getPopulationSize()); ind_i < N_i; ++ind_i) {
			vdou1D f_vector;
			for (std::size_t j(0); j<i+1; ++j) {
				for (std::size_t ind_j(0), N_j(deme[j].getPopulationSize()); ind_j < N_j; ++ind_j) {
					double f (0.0);
					size_t eval_loci (n_loci);
					for (std::size_t k(0); k<n_loci; ++k) {
						bool all_loci (true);
						double tmpf (0.0);
						for (std::size_t a(0); a<alleles_per_locus[k].size(); ++a) {
							if (!(*eval_ind_locus[i][ind_i])[k]) { all_loci = false; break; }
							else if (!(*eval_ind_locus[j][ind_j])[k]) { all_loci = false; break; }
							else if ((*ind_freq[i][ind_i])[k][a]== 0.0 || (*ind_freq[j][ind_j])[k][a] == 0.0) continue;
							else tmpf += (*ind_freq[i][ind_i])[k][a] * (*ind_freq[j][ind_j])[k][a];
						}
						if (!all_loci) --eval_loci; // If no allele, no calculation for that loci
						else f += tmpf;
					}
					if (eval_loci) f /= eval_loci;
					f_vector.push_back(f);
				}
			}
			f_mol.push_back(f_vector);
		}
	}
}

/* ********************************************************************************************
 * MTPInput:: genealogical_matrix ()
 * --------------------------------------------------------------------------------------------
 * Calculates the matrix of genealogical coancestry between individuals
 * ********************************************************************************************/
void Metapop::genealogical_matrix (const vint2D& pedigree) {

	if (DEBUG) std::cout << "Calculating coancestry matrix..." << endl;
	f_mol.clear();

	// Initialize coancestry matrix
	for (std::size_t i(0); i<pedigree.size(); ++i) {
		std::size_t ind_ID (i+1);
		vdou1D row;
		for (std::size_t j(0); j<ind_ID; ++j) {
			if (j!=i) row.push_back(0.0);
			else row.push_back(0.5);
		}
		f_mol.push_back(row);
	}

	// Evaluate coancestry
	for (std::size_t i(0); i<pedigree.size(); ++i) {
		std::size_t ind_ID (i+1);
		std::size_t id1 (pedigree[i][1]), id2 (pedigree[i][2]);
		for (std::size_t j(0); j<ind_ID; ++j) {
			if (i==j) {
				if (!id1 || !id2) continue;
				else if (id1 <= id2) f_mol[i][j] = 0.5*(1.0+f_mol[id2-1][id1-1]);
				else f_mol[i][j] = 0.5*(1.0+f_mol[id1-1][id2-1]);
			} else {
				std::size_t id_j (pedigree[j][0]);
				double f_dad (0.0);
				if (!id1) f_dad = 0.0;
				else if (id_j<=id1) f_dad = f_mol[id1-1][id_j-1];
				else f_dad = f_mol[id_j-1][id1-1];
				double f_mom (0.0);
				if (!id2) f_mom = 0.0;
				else if (id_j<=id2) f_mom = f_mol[id2-1][id_j-1];
				else f_mom = f_mol[id_j-1][id2-1];
				f_mol[i][j] = 0.5*(f_dad+f_mom);
			}
		}
	}

	if (DEBUG) {
		std::cout << std::endl;
		std::cout << "Done" << std::endl;
		std::cout << std::endl;
	}
}

/* ********************************************************************************************
 * Metapop::calculate_p ()
 * --------------------------------------------------------------------------------------------
 * Calculate the average allele frequency in the subpopulations and metapopulation.
 * Check equation 10 in Caballero & Toro (2002).
 * ********************************************************************************************/
void Metapop::calculate_p ( ) {

	// p.b_par[i][m][k]: The average frequency of allele [k], at locus [m] in subpopulation [i]
	// This is shown in output Section 2 (average frequencies of subpopulations)
	for (std::size_t i (0); i < n_subpops; i++) {
		vdou2D freq_subpop_i;
		for (std::size_t m (0); m < n_loci; m++){
			vdou1D freq_locus;
			for (std::size_t k (0); k < alleles_per_locus[m].size(); k++) {
				double freq_allele (0.0);
				for (std::size_t l (0); l < deme[i].getPopulationSize(); l++) {
					if ((*eval_ind_locus[i][l])[m]) {
						freq_allele += (*ind_freq[i][l])[m][k] / (double) deme[i].getIndPerLocus(m);
					}
				}
				freq_locus.push_back(freq_allele);
			}
			freq_subpop_i.push_back(freq_locus);
		}
		p.b_par.push_back(new vdou2D (freq_subpop_i));
	}

    /* p.t_par[m][k]: Average frequency of allele [k] at locus[m] */
    // This is shown in output Section 2 (average frequencies of metapopulation)
    std::vector<std::size_t> sum_L (this->informative_inds());
	for (std::size_t m (0); m < n_loci; m++) {
		vdou1D freq_locus;
		for (std::size_t k (0); k < alleles_per_locus[m].size(); k++) {
			double freq_allele (0.0);
			for (std::size_t i (0); i < n_subpops; i++) {
				freq_allele += (*p.b_par[i])[m][k] * deme[i].getIndPerLocus(m) / sum_L[m];
			}
			freq_locus.push_back(freq_allele);
		}
		p.t_par.push_back(new vdou1D (freq_locus));
	}
}

/* ********************************************************************************************
 * Metapop::calculate_f ()
 * --------------------------------------------------------------------------------------------
 * Calculate the average coancestry between subpopulations.
 * Check equation 11 in Caballero & Toro (2002).
 * ********************************************************************************************/
void Metapop::calculate_f () {

	// f.b_par[m][i][j]: The average coancestry between population [i] and [j] for locus [m]
	if (use_molecular_markers) {
		for (std::size_t m (0); m < n_loci; m++) {
			vdou2D f_bpops;
			for (std::size_t i (0); i < n_subpops; i++) {
				vdou1D f_wpops;
				for (std::size_t j (0); j < n_subpops; j++) {
					double f_pop(0.0);
					for (std::size_t k (0); k < alleles_per_locus[m].size(); k++) {
						f_pop += ((*p.b_par[i])[m][k] * (*p.b_par[j])[m][k]);
					}
					f_wpops.push_back(f_pop);
				}
				f_bpops.push_back(f_wpops);
			}
			f.b_par.push_back(new vdou2D (f_bpops));
		}

	// f.t_par[i][j]: The average coancestry between population [i] and [j] (for all loci)
	// This is shown in GENE DIVERSITY output section as 'f_ii' and 'f_ij'
		std::vector<std::size_t> sum_P (this->informative_loci());
		for (std::size_t i (0); i < n_subpops; i++) {
			std::vector<double> f_pop;
			for (std::size_t j (0); j < n_subpops; j++) {
				double fij (0.0);
				std::size_t tot_P (sum_P[i]+sum_P[j]);
				for (std::size_t m (0); m < n_loci; m++) {
					std::size_t P_m (deme[i].getIndPerLocus(m)+deme[j].getIndPerLocus(m));
					fij += (*f.b_par[m])[i][j] * P_m / tot_P;
				}
				f_pop.push_back(fij);
			}
			f.t_par.push_back(new vdou1D(f_pop));
		}

	} else {
		std::size_t inds_i (0);
		for (std::size_t i(0); i<n_subpops; ++i) {
			std::size_t inds_j (0);
			std::vector<double> f_pop;
			for (std::size_t j(0); j < n_subpops; ++j) {
				double fij (0.0);
				for (std::size_t k(inds_i); k < (inds_i + deme[i].getPopulationSize()); ++k) {
					for (std::size_t l(inds_j); l < (inds_j + deme[j].getPopulationSize()); ++l) {
						if (k > l) {
							fij += f_mol[k][l];
						} else {
							fij += f_mol[l][k];
						}
					}
				}
				fij /= (deme[i].getPopulationSize()*deme[j].getPopulationSize());
				f_pop.push_back(fij);
				inds_j += deme[j].getPopulationSize();
			}
			inds_i += deme[i].getPopulationSize();
			f.t_par.push_back(new vdou1D(f_pop));
		}
	}

}

/* ********************************************************************************************
 * Metapop::calculate_s ()
 * --------------------------------------------------------------------------------------------
 * Calculate the average self-coancestry of the subpopulations.
 * Check equation 12 in Caballero & Toro (2002).
 * 
 * |||| // added for polyploids only, no other use (removable if we calculate polyploid F other way
 * |||| // polyploid_f[m][i]
 * ********************************************************************************************/
void Metapop::calculate_s () {

	// s.b_par[m][i]: The average self-coancestry of subpopulation [i] at locus [m]
	if (use_molecular_markers) {
		for (std::size_t m (0); m < n_loci; m++) {
			vdou1D row_sloc;
			vdou1D row_p_f; // ||||
			for (std::size_t i (0); i < n_subpops; i++) {
				double s_allele (0.0);
				double p_f_ind (0.0);
				for (std::size_t l (0); l < deme[i].getPopulationSize(); l++) {
					double max_freq (0.0); // ||||
					for (std::size_t k (0); k < alleles_per_locus[m].size(); k++) {
						if ((*eval_ind_locus[i][l])[m]) {
							s_allele += pow((*ind_freq[i][l])[m][k], 2.0) / deme[i].getIndPerLocus(m);
							if ((*ind_freq[i][l])[m][k] > max_freq) max_freq = (*ind_freq[i][l])[m][k]; // ||||
						}
					}
					if (max_freq == 1.0) p_f_ind += 1.0; // ||||
				}
				p_f_ind /= deme[i].getPopulationSize(); // ||||
				row_p_f.push_back(p_f_ind); // ||||
				row_sloc.push_back(s_allele);
			}
			s.b_par.push_back(new vdou1D (row_sloc));
			polyploid_f.push_back(row_p_f); // ||||
		}

	// s.t_par[i]: The average self-coancestry of subpopulation [i] (all loci)
	// This is shown in GENE DIVERSITY output section as 's_i'
		std::vector<std::size_t> sum_P (this->informative_loci());
		for (std::size_t i (0); i < n_subpops; i++) {
			double s_locus(0.0);
			for (std::size_t m (0); m < n_loci; m++) {
				s_locus += (*s.b_par[m])[i] * deme[i].getIndPerLocus(m) / sum_P[i];
			}
			s.t_par.push_back(new double (s_locus));
		}

	} else {
		std::size_t inds_i (0);
		for (std::size_t i(0); i<n_subpops; ++i) {
			double s_pop (0.0);
			for (std::size_t k(inds_i); k < (inds_i + deme[i].getPopulationSize()); ++k) {
				s_pop += f_mol[k][k];
			}
			s_pop /= (deme[i].getPopulationSize());
			inds_i += deme[i].getPopulationSize();
			s.t_par.push_back(new double(s_pop));
		}
	}

}

/* ********************************************************************************************
 * Metapop::calculate_d ()
 * --------------------------------------------------------------------------------------------
 * Calculate the average genetic distance between individuals of subpopulations.
 * Check equation 13 in Caballero & Toro (2002).
 * ********************************************************************************************/
void Metapop::calculate_d () {

	// d.b_par[m][i][j]: The average genetic distance between subpopulation [i] and [j] at locus [m]
	if (use_molecular_markers) {
		for (std::size_t m (0); m < n_loci; m++) {
			vdou2D d_bpop;
			for (std::size_t i (0); i < n_subpops; i++) {
				int ind_i_locus (deme[i].getIndPerLocus(m));
				vdou1D d_wpop;
				for (std::size_t j (0); j < n_subpops; j++) {
					double d_loc_pop(0.0);
					int ind_j_locus (deme[j].getIndPerLocus(m));
					for (std::size_t l (0), N_i (deme[i].getPopulationSize()); l < N_i; l++) {
						if ((*eval_ind_locus[i][l])[m]) {
							for (std::size_t ll (0), N_j (deme[j].getPopulationSize()); ll < N_j; ll++) {
								if ((*eval_ind_locus[j][ll])[m]) {
									for (std::size_t k (0); k < alleles_per_locus[m].size(); k++) {
										d_loc_pop += pow((*ind_freq[i][l])[m][k] - (*ind_freq[j][ll])[m][k], 2.0) / (2.0 * ind_i_locus * ind_j_locus);
									}
								}
							}
						}
					}
					d_wpop.push_back(d_loc_pop);
				}
				d_bpop.push_back(d_wpop);
			}
			d.b_par.push_back(new vdou2D (d_bpop));
		}

	// d.t_par[i][j]: The average genetic distance between subpopulations [i] and [j] (all loci)
	// This is shown in GENE DIVERSITY output section as 'd_ii'
		std::vector<std::size_t> sum_P (this->informative_loci());
		for (std::size_t i (0); i < n_subpops; i++) {
			std::vector<double> d_pop;
			for (std::size_t j (0); j < n_subpops; j++) {
				double d_locus (0.0);
				std::size_t tot_P (sum_P[i]+sum_P[j]);
				for (std::size_t m (0); m < n_loci; m++) {
					std::size_t P_m (deme[i].getIndPerLocus(m)+deme[j].getIndPerLocus(m));
					d_locus += (*d.b_par[m])[i][j] * P_m / tot_P;
				}
				d_pop.push_back(d_locus);
			}
			d.t_par.push_back(new vdou1D (d_pop));
		}

	} else {
		for (std::size_t i(0); i<n_subpops; ++i) {
			std::vector<double> d_pop;
			for (std::size_t j(0); j < n_subpops; ++j) {
				double dij = (((*s.t_par[i]) + (*s.t_par[j]))/2.0) - (*f.t_par[i])[j];
				d_pop.push_back(dij);
			}
			d.t_par.push_back(new vdou1D(d_pop));
		}
	}

}

/* ********************************************************************************************
 * Metapop::calculate_unique_alleles ()
 * --------------------------------------------------------------------------------------------
 * Searches alleles that are unique of each subpopulation
 * ********************************************************************************************/
void Metapop::calculate_unique_alleles () {

	unique_alleles.assign(n_subpops,0);
	for (std::size_t m(0); m<alleles_per_locus.size(); ++m) {
		for (std::size_t k(0); k<alleles_per_locus[m].size(); ++k) {
			int is_unique (0);
			int index (0);
			for (std::size_t i(0); i<n_subpops; ++i) {
				if ( (*p.b_par[i])[m][k] > 0.0) {
					++is_unique;
					index = i;
				}
			}
			if (is_unique==1) ++unique_alleles[index];
		}
	}

}

/* ********************************************************************************************
 * Metapop:: informative_inds ()
 * --------------------------------------------------------------------------------------------
 * Returns a vector of size "n_loci" with the number of individuals that have informative loci
 * ********************************************************************************************/
std::vector<std::size_t> Metapop::informative_inds () const {
	std::vector<std::size_t> sum_L;
	for (std::size_t m (0); m < n_loci; m++) {
		int tmp_sum (0);
		for (std::size_t i (0); i < n_subpops; i++) {
			if (use_molecular_markers) {
				tmp_sum += deme[i].getIndPerLocus(m);
			} else {
				tmp_sum += deme[i].getPopulationSize();
			}
		}
		sum_L.push_back(tmp_sum);
	}
	return sum_L;
}

/* ********************************************************************************************
 * Metapop:: informative_loci ()
 * --------------------------------------------------------------------------------------------
 * Returns a vector of size "n_subpops" with the number of informative loci in each subpopulation
 * (max value = n_loci*Ni).
 * ********************************************************************************************/
std::vector<std::size_t> Metapop::informative_loci () const {
	std::vector<std::size_t> sum_P;
	// For each subpopulation, the number of informative locus
	for (std::size_t i (0); i < n_subpops; i++) {
		int tmp_sum (0);
		for (std::size_t m (0); m < n_loci; m++) {
			tmp_sum += deme[i].getIndPerLocus(m);
		}
		sum_P.push_back(tmp_sum);
	}
	return sum_P;
}

/* ********************************************************************************************
 * Metapop::input_metapop ()
 * --------------------------------------------------------------------------------------------
 * This function reads the legacy metapop input file, initializing class metapop
 * ********************************************************************************************/
void Metapop::input_metapop (std::string file) {

	std::string str;
	std::stringstream fr(file, std::stringstream::in);
	N = 0;
	std::string label;
	std::size_t nrow (0);
	std::size_t Nfemales (0);
	N_females = 0;
	std::size_t Nsubpop (0);

	// Reading metapop header (optional)
	if (DEBUG) std::cout << "\rReading Metapop header...";
	std::size_t header_nsubpops;
	std::size_t header_nloci;
	bool read_nsubpops (false);
	bool read_nloci (false);
	legacy_inds = true;
	std::string row;

	while (getline(fr,row)) {

		++nrow;

		// Each row is converted in a vector of strings
		if (!row.size()) continue;
		row = std::regex_replace(row, std::regex("^ +| +$|( ) +"), "$1");
		std::vector<std::string> v_row (split_string(row, ' '));

		//  Settings have length 2
		if (v_row.size() == 2) {
			if (v_row[0] == "n") {
				header_nsubpops = atoi(v_row[1].c_str());
				read_nsubpops = true;
			} else if (v_row[0] == "nloci") {
				header_nloci = atoi(v_row[1].c_str());
				read_nloci = true;
			} else {
				print_error(ERROR_MTP_LABEL);
			}
		} else if (v_row.size() > 2) {
				print_error(ERROR_MTP_LABEL);
		} else {
			label = v_row[0];
			break;
		}
	}

	if (DEBUG) {
		std::cout << "\rReading Metapop header...done";
		std::cout << std::endl;
		std::cout << "\rReading Metapop body...";
	}

	// Reading metapop body
	n_loci = 0;
	std::vector<individual> v_Inds;
	std::size_t Ni, Nei, body_N (0), body_Ne (0);
	progeny_set = false;
	bool Ne_set (false);
	size_set = false;
	set_sex = true;
	std::size_t n_Ne (0);
	std::size_t n_N (0);
	restricted_mates = false;

	while (getline(fr,row)) {

		++nrow;

		// Each row is converted in a vector of strings. Empty rows are ignored
		row = std::regex_replace(row, std::regex("^ +| +$|( ) +"), "$1");
		if (!row.size()) continue;
		std::vector<std::string> v_row (split_string(row, ' '));

		// Input subpopulation label
		if (v_row.size() == 1) {

			// Reserved keywords
			if (v_row[0] == OPT_MATRIX) {
				end_body (Ni, Nfemales, Nsubpop, label, v_Inds);
				read_fmatrix (fr);
				break;
			} else if (v_row[0] == OPT_RESTRI) {
				end_body (Ni, Nfemales, Nsubpop, label, v_Inds);
				read_mates (fr);
				break;
			} else if (v_row[0] == OPT_FORBID) {
				end_body (Ni, Nfemales, Nsubpop, label, v_Inds);
				read_forbid (fr);
				break;
			}

			// Input label
			if (size_set && Ni != v_Inds.size()) print_error(ERROR_MTP_SUBPS);
			if (for_management && (!Nfemales || Nsubpop == Nfemales))  print_error(ERROR_MTP_NUMFM);
			N_females += Nfemales;
			Nfemales = 0;
			Nsubpop = 0;
			deme.push_back(population(label,v_Inds));
			v_Inds.clear();
			label = v_row[0];
			continue;

		// Input optional subpopulation information
		} else if (v_row.size() == 2 && v_row[0] == "N") {
			size_set = true;
			Ni = atoi(v_row[1].c_str());
			body_N += Ni;
			++n_N;
		} else if (v_row.size() == 2 && v_row[0] == "Ne") {
			Ne_set = true;
			Nei = atoi(v_row[1].c_str());
			body_Ne += Nei;
			++n_Ne;
		} else if (v_row.size() == 3 && (v_row[0] == "newN" || v_row[0] == "NewN")) {
			progeny_set = true;
			vshort1D progeny_row;
			short nNm (atoi(v_row[1].c_str()));
			short nNf (atoi(v_row[2].c_str()));
			progeny_row.push_back(nNm);
			progeny_row.push_back(nNf);
			progeny.push_back(progeny_row);

		// Input individuals information and genotypes
		} else if (v_row.size() >= 3) {
			std::size_t IDi (atoi(v_row[0].c_str()));
			std::string GIDi (v_row[1]);
			unshort sex (atoi(v_row[2].c_str()));
			std::vector <locus> loci;

			// Input legacy entry for individuals
			if (!(row.find(',') != std::string::npos)) {
				if (!legacy_inds) print_error (ERROR_MTP_MXFOR);
				if (sex != 0 && sex != 1) print_error(ERROR_MTP_SEXFM);
				if ((v_row.size()-3) %ploidy != 0) print_error (ERROR_MTP_PLOAL);
				for (std::size_t i(3); i<v_row.size(); i+=ploidy) {
					allele1D v_alleles;
					for (std::size_t a(0); a<ploidy; ++a) v_alleles.push_back(get_allele(v_row[i+a]));
					append_alleles (v_alleles, loci.size());
					loci.push_back(locus(v_alleles));
				}
			// Input simplified entry
			} else {
				if (legacy_inds && N) print_error (ERROR_MTP_MXFOR);
				legacy_inds = false;
				std::vector<std::string> tmp_row (split_string(row, ','));
				if (tmp_row.size() > 2) print_error (ERROR_MTP_COMMA);
				tmp_row[0] = std::regex_replace(tmp_row[0], std::regex("^ +| +$|( ) +"), "$1");
				std::vector<std::string> tmp_ID (split_string(tmp_row[0], ' '));

				IDi = N+1;
				GIDi = tmp_ID[0];
				if (tmp_ID.size() == 1) {
					set_sex = false;
					sex = 0;
					if (for_management) print_error (ERROR_MTP_SXMNG);
				} else if (tmp_ID.size() == 2) {
					if (!set_sex) print_error (ERROR_MTP_OPTSX);
					sex = atoi(tmp_ID[1].c_str());
					if (sex != 0 && sex != 1) print_error(ERROR_MTP_SEXFM);
				} else if (tmp_ID.size()>2) print_error (ERROR_MTP_HEADS);

				tmp_row[1] = std::regex_replace(tmp_row[1], std::regex("^ +| +$|( ) +"), "$1");
				v_row = split_string(tmp_row[1], ' ');
				if (v_row.size() %ploidy != 0) print_error (ERROR_MTP_PLOAL);

				for (std::size_t i(0); i<v_row.size(); i+=ploidy) {
					allele1D v_alleles;
					for (std::size_t a(0); a<ploidy; ++a) v_alleles.push_back(get_allele(v_row[i+a]));
					append_alleles (v_alleles, loci.size());
					loci.push_back(locus(v_alleles));
				}
			}
			if (!sex) ++Nfemales;
			++Nsubpop;
			if (!n_loci) n_loci = loci.size();
			else if (n_loci != loci.size()) print_error(ERROR_MTP_NLOCI);
			v_Inds.push_back(individual(GIDi, IDi, sex, loci));
			++N;

		// Return error
		} else {
			row_error(nrow);
		}
	}

	if (use_molecular_markers & !restricted_mates & !forbidden_mates) {
		end_body (Ni, Nfemales, Nsubpop, label, v_Inds);
	}

	if (DEBUG) {
		std::cout << "\rReading Metapop body...done";
		std::cout << std::endl;
	}

	// Final checks
	if (Ne_set && n_Ne != n_subpops) print_error(ERROR_MTP_NESZP);
	if (size_set && n_N != n_subpops) print_error(ERROR_MTP_SIZEP);

	if (DEBUG) cout << endl;

	// Check header parameters
	if (read_nsubpops && header_nsubpops != n_subpops) {
		print_error(ERROR_MTP_MTCHN);
	}

	if (use_molecular_markers && read_nloci && (header_nloci != n_loci)) {
		print_error(ERROR_MTP_MTCHL);
	}

	// Optional input
	read_rm (fr);
	read_manage (fr);

	if (DEBUG) {
		std::cout << "Optional specifications" << std::endl;
		std::string inp_matrix ("absent");
		if (!use_molecular_markers) inp_matrix = "present";
		std::cout << " - Coancestry matrix: " << inp_matrix << std::endl;
		std::cout << std::endl;
	}

}

/* ********************************************************************************************
 * Metapop::input_genepop ()
 * --------------------------------------------------------------------------------------------
 * This function creates an intern and simplified metapop input file from a genepop input file
 * ********************************************************************************************/
void Metapop::input_genepop (std::string str) {

	use_molecular_markers = true;

	// Check format and read input parameters
	std::stringstream fr(str, std::stringstream::in);

	// First line is information, to be descarded by Metapop
	std::string information;
	getline(fr, information);

	// Read locus names (header)
	if (DEBUG) std::cout << "\rReading Genepop header...";
	std::vector<std::string> locus_names;
	std::string header;
	while (getline(fr,header)) {
		header = std::regex_replace(header, std::regex("^ +| +$|( ) +"), "$1");
		if (test_pop(header)) break;
		std::vector<std::string> tmp_header (split_string(header, ','));
		for (const auto& locus: tmp_header) {
			locus_names.push_back(locus);
		}
	}

	if (DEBUG) {
		std::cout << "\rReading Genepop header...done";
		std::cout << std::endl;
		std::cout << "\rReading Genepop body...";
	}

	// Read individuals (body)
	std::vector<individual> v_Inds;
	std::string label ("Pop1");
	n_subpops = 1;
	n_loci = 0;
	N = 0;
	std::string ind_string;

	while (getline (fr,ind_string)) {
		ind_string = std::regex_replace(ind_string, std::regex("^ +| +$|( ) +"), "$1");
		if (test_pop(ind_string)) {
			deme.push_back(population(label,v_Inds));
			v_Inds.clear();
			++n_subpops;
			label = "Pop" + std::to_string(n_subpops);
			continue;
		}

		std::vector<std::string> split_ind (split_string(ind_string, ','));
		if (split_ind.size() != 2) print_error (ERROR_GNP_DELIM);

		std::stringstream loci_string (split_ind[1]);
		std::string ind_locus;
		std::size_t IDi (N+1);
		std::size_t sex (0);
		std::string GIDi = split_ind[0];
		rm_char_in_str(GIDi,'\t');
		rm_char_in_str(GIDi,' ');
		if (GIDi.empty()) GIDi = label + "_" + std::to_string(IDi);
		std::vector<locus> loci;

		while (loci_string >> ind_locus) {
			std::size_t size_locus (ind_locus.size());
			if (size_locus % 2 != 0) print_error (ERROR_GNP_DELIM);
			allele_t a1 (atoi(ind_locus.substr(0,size_locus/2).c_str()));
			allele_t a2 (atoi(ind_locus.substr(size_locus/2,size_locus/2).c_str()));
			append_alleles ({a1, a2}, loci.size());
			loci.push_back(locus(a1,a2));
		}

		if (!n_loci) n_loci = loci.size();
		else if (loci.size() != n_loci) {
			std::string last_loci;
			while (getline (fr,last_loci)) {
				last_loci = std::regex_replace(last_loci, std::regex("^ +| +$|( ) +"), "$1");
				if (last_loci.find(',') < last_loci.size()) print_error (ERROR_GNP_DELIM);
				std::stringstream last_loci_str (last_loci);
				while (last_loci_str >> ind_locus) {
					std::size_t size_locus (ind_locus.size());
					if (size_locus % 2 != 0) print_error (ERROR_GNP_DELIM);
					allele_t a1 (atoi(ind_locus.substr(0,size_locus/2).c_str()));
					allele_t a2 (atoi(ind_locus.substr(size_locus/2,size_locus/2).c_str()));
					append_alleles ({a1, a2}, loci.size());
					loci.push_back(locus(a1,a2));
				}
				if (loci.size() == n_loci) {
					break;
				}
			}
		}

		//else if (n_loci != loci.size()) print_error (ERROR_DLOCI);
		v_Inds.push_back(individual(GIDi, IDi, sex, loci));
		++N;

	}

	if (!N) print_error ("Couldn't read individuals from the input genepop file. Does it have the right format?");
	deme.push_back(population(label,v_Inds));
	v_Inds.clear();
	Ne = N;
	clean_alleles ();
	if (DEBUG) {
		std::cout << "\rReading Genepop body...done";
		std::cout << std::endl;
	}

}

/* ********************************************************************************************
 * Metapop::input_pedfile ()
 * --------------------------------------------------------------------------------------------
 * This function creates an intern and simplified metapop input file from a plink PED file
 * ********************************************************************************************/
void Metapop::input_pedfile (std::string str, bool calc_pedigree, std::string rmatrix_filename) {

	if (!calc_pedigree) use_molecular_markers = true;
	else {
		use_molecular_markers = false;
		print_warning("Reading pedigree information: molecular markers will be ignored.");
	}

	// Check format and read input parameters
	std::stringstream fr(str, std::stringstream::in);

	// Read individuals
	if (DEBUG) std::cout << "\rReading Plink PED file...";
	std::vector<individual> v_Inds;
	std::string label ("");
	n_subpops = 0;
	N = 0;
	N_females = 0;
	n_loci = 0;
	std::string ind_string;
	std::vector<std::string> saved_labels;
	std::string last_label;
	std::vector<std::string> saved_unique_id;
	std::map<std::string,std::size_t> ped_index;
	vint2D pedigree;

	while (getline (fr,ind_string)) {

		ind_string = std::regex_replace(ind_string, std::regex("^ +| +$|( ) +"), "$1");
		std::vector<std::string> split_ind (split_string(ind_string, ' '));
		if ((split_ind.size() %2 != 0) && (ploidy==2)) print_error (ERROR_PAIRS);
		else if (rmatrix_filename=="" && (((split_ind.size() < 7) && !calc_pedigree) || ((split_ind.size() < 6) && calc_pedigree))) print_error ("Couldn't read individuals from the input plink PED file. Does it have the right format?");
		else if (rmatrix_filename!="" && split_ind.size()<6) print_error("Couldn't read individuals from the input plink PED file. Does it have the right format?");

		if (N && split_ind[0] != label) {
			if (!is_in(saved_labels, label)) {
				deme.push_back(population(label,v_Inds));
				saved_labels.push_back(label);
				++n_subpops;
			} else {
				std::size_t index (deme.size());
				for (std::size_t i(0); i<deme.size(); ++i) {
					if (deme[i].getLabel() == last_label) {
						index = i;
						break;
					}
				}
				deme[index].addIndividuals(v_Inds);
			}
			v_Inds.clear();
		}
		label = split_ind[0];
		std::string GIDi = split_ind[1];
		if (GIDi=="0") print_error ("Within-family ID cannot be '0'.");
		std::string unique_id (label+GIDi);
		if (is_in(saved_unique_id, unique_id)) print_error ("Individuals must have unique IDs (composed by Family and within-family IDs)");
		else saved_unique_id.push_back(unique_id);

		std::size_t IDi (N+1);
		ped_index[unique_id] = IDi;
		std::size_t sex (atoi(split_ind[4].c_str()));
		if (sex==2) {
			++N_females;
			sex = 0;
		} else if (sex==0) {
			 if (for_management) print_error ("Cannot run a management method with individuals of unknown gender.");
		} else if (sex!=1) {
			print_error ("Unknown gender in plink ped file (not 0, 1 or 2).");
		}

		std::vector<locus> loci;
		if (calc_pedigree) {
			vint1D ped_row;
			ped_row.push_back(IDi);
			std::string dad_id (label+split_ind[2]), mom_id (label+split_ind[3]);
			if (!ped_index[dad_id] && (split_ind[2]!="0")) print_error ("Undefined father ID: it has not been declared as within-family ID, or individuals are not sorted from older to younger.");
			else if (!ped_index[mom_id] && (split_ind[3]!="0")) print_error ("Undefined mother ID: it has not been declared as within-family ID, or individuals are not sorted from older to younger.");
			else if ((split_ind[2]!="0") && (dad_id == mom_id)) print_error ("Self-fertilization is not supported. Check parents of " + label + " " + GIDi);
			else if (for_management && (split_ind[2]!="0") && (pedigree[pedigree[ped_index[dad_id]-1][0]-1][3]==0)) {
				print_error ("Father of individual " + label + " " + GIDi + " was declared as a female");
			}
			else if (for_management && (split_ind[3]!="0") && (pedigree[pedigree[ped_index[mom_id]-1][0]-1][3]==1)) {
				print_error ("Mother of individual " + label + " " + GIDi + " was declared as a male");
			}
			ped_row.push_back(ped_index[dad_id]);
			ped_row.push_back(ped_index[mom_id]);
			ped_row.push_back(sex);
			pedigree.push_back(ped_row);
		} else {
			for (std::size_t i(6); i<split_ind.size(); i+=ploidy) {
				allele_t a1 (get_allele(split_ind[i]));
				allele_t a2 (0);
				if (ploidy==2) a2 = get_allele(split_ind[i+1]);
				append_alleles ({a1, a2}, loci.size());
				loci.push_back(locus(a1,a2));
			}
			if (!n_loci) n_loci = loci.size();
			else if (n_loci != loci.size()) print_error (ERROR_DLOCI);
		}

		v_Inds.push_back(individual(GIDi, IDi, sex, loci));
		++N;
		last_label = label;

	}

	if (!is_in(saved_labels, label)) {
		deme.push_back(population(label,v_Inds));
		saved_labels.push_back(label);
		++n_subpops;
	} else {
		std::size_t index (deme.size());
		for (std::size_t i(0); i<deme.size(); ++i) {
			if (deme[i].getLabel() == last_label) {
				index = i;
				break;
			}
		}
		deme[index].addIndividuals(v_Inds);
	}
	v_Inds.clear();
	Ne = N;
	clean_alleles ();
	if (DEBUG) {
		std::cout << "\rReading Plink PED file...done";
		std::cout << std::endl;
	}

	// Test pedigree
	if (calc_pedigree) test_pedigree(pedigree);

	// Calculate coancestry
	if (calc_pedigree) this->genealogical_matrix(pedigree);

	// Set struc and prgeny for management
	set_struc ();
	default_mng ();

}

/* ********************************************************************************************
 * Metapop::default_mng ()
 * --------------------------------------------------------------------------------------------
 * Sets a default progeny matrix that retains population structure
 * ********************************************************************************************/
void Metapop::default_mng () {
	if (progeny.size()) print_error ("Trying to set default newN values, but values have been already set");
	else if (!deme.size()) print_error ("Trying to set default newN values, but no population have been read");
	else {
		for (std::size_t i(0); i<n_subpops; ++i) {
			vshort1D row;
			row.push_back(deme[i].getNperSex(0));
			row.push_back(deme[i].getNperSex(1));
			progeny.push_back(row);
		}
	}
}

/* ********************************************************************************************
 * Metapop::input_management ()
 * --------------------------------------------------------------------------------------------
 * Read the management file
 * ********************************************************************************************/
void Metapop::input_management ( std::string str) {
	if (!for_management) print_error("Cannot read management file without choosing a management method.");
	else if (progeny.size()) progeny.clear();

	// Check format and read input parameters
	std::stringstream fr (str, std::stringstream::in);
	std::string line;
	getline (fr, line);
	if (line != "newN" && line != "NewN") print_mng_error ("Management file must begin with a 'newN' declaration.");

	// Initialize progeny
	progeny = vshort2D (n_subpops, vshort1D(2));
	std::vector<std::string> read_labels;

	for (std::size_t i(0); i<n_subpops; ++i) {
		if (!getline (fr, line)) print_mng_error ("Incomplete management file. Set the desired number of females and males for every subpopulation.");
		std::vector<std::string> v_line (split_string(line, ' '));
		if (v_line.size() != 3) print_mng_error ("Declaration of individuals for next generation must be: [Population label] [N females] [N males]");
		bool found (false);
		for (std::size_t j(0); j<n_subpops; ++j) {
			if (v_line[0] == deme[j].getLabel()) {
				if (is_in(read_labels, v_line[0])) print_mng_error ("Repeated population label in management file.");
				read_labels.push_back(v_line[0]);
				if (!isInt(v_line[1]) || !isInt(v_line[2])) print_mng_error ("Invalid number of females or males in management file.");
				progeny[j][0] = atoi(v_line[1].c_str());
				progeny[j][1] = atoi(v_line[2].c_str());
				found = true;
				break;
			}
		}
		if (!found) print_mng_error ("At least one population label in the management file does not match labels in the input datafile.");
	}
	progeny_set = true;

	// Optional input
	read_manage (fr);

}

/* ********************************************************************************************
 * Metapop::end_body ()
 * --------------------------------------------------------------------------------------------
 * This function checks parameters at the end of the metapop input file body, and appends the
 * last subpopulation
 * ********************************************************************************************/
void Metapop::end_body (std::size_t& Ni, std::size_t& Nfemales, std::size_t& Nsubpop, std::string& label, std::vector<individual>& v_Inds) {
	if (!legacy_inds & progeny_set & !for_management) print_warning("Simplified metapop input declares contributions to next generation, but no management has been set. They will be ignored.");
	if (!legacy_inds & set_sex & !for_management) print_warning("Simplified metapop input declares individual sexes, but no management has been set. Sex will be ignored.");
	if (size_set && Ni != v_Inds.size()) print_error(ERROR_MTP_SUBPS);
	if (for_management && (!Nfemales || Nsubpop == Nfemales))  print_error(ERROR_MTP_NUMFM);

	deme.push_back(population(label,v_Inds));
	n_subpops = deme.size();
	v_Inds.clear();
	Ne = N;
	N_females += Nfemales;
	clean_alleles ();
}

/* ********************************************************************************************
 * Metapop::opt_tmp_mng ()
 * --------------------------------------------------------------------------------------------
 * Reads temporal marks for management to change population structure over generations
 * ********************************************************************************************/
void Metapop::opt_tmp_mng (std::stringstream& fr, std::string key) {
	temporal_marks = true;
	// Check generations
	std::vector<std::string> v_key (split_string(key, ' '));
	if (v_key.size() < 2) print_error ("No generations read from temporal marks in management file.");

	// Get generations of population structure change
	for (std::size_t i(1); i<v_key.size(); ++i) {
		if (isInt(v_key[i])) {
			if (mng_t.size() && (atoi(v_key[i].c_str()) < mng_t[mng_t.size()-1])) {
				print_error ("Generations must be entered in increasing order for temporal marks in management file");
			} else mng_t.push_back(atoi(v_key[i].c_str()));
		} else print_error ("Error reading temporal marks in management file: no integer values detected.");
	}
	if (mng_t[0] < 2) print_error ("Minimum generation number for temporal marks in management file is 2 (generation 1 is given by 'newN')");
	else if (mng_t[mng_t.size()-1] > sim_gens) print_error ("Maximum generation number for temporal marks in management file exceeds the defined number of generations for simulation.");

	// Initialize next offspring matrix
	mng_t_offspring.clear();
	mng_t_offspring = std::deque<vshort2D> (mng_t.size(), vshort2D (deme.size(),vshort1D (2,0)));

	for (std::size_t i(0); i<deme.size(); ++i) {
		std::string row;
		getline(fr,row);
		std::vector<std::string> v_row (split_string(row, ' '));
		if (v_row.size() != 2*mng_t.size()) print_error ("Error reading population structure changes in management file: the numbers of values for new population sizes must be twice the numbers of temporal marks entered");
		std::size_t t_count (0);
		for (std::size_t j(0); j<v_row.size(); j+=2) {
			if (!isInt(v_row[j])) print_error ("Error reading population structure changes in management file: no integer values detected.");
			else {
				mng_t_offspring[t_count][i][0] = atoi(v_row[j].c_str());
				mng_t_offspring[t_count][i][1] = atoi(v_row[j+1].c_str());
			}
			++t_count;
		}
	}
}

/* ********************************************************************************************
 * Metapop::read_rm ()
 * --------------------------------------------------------------------------------------------
 * Reads a relationship matrix file
 * ********************************************************************************************/
void Metapop::read_rm (std::stringstream& fr) {
	std::string keyword;
	while (getline(fr,keyword)) {
		// Genealogical coancestry matrix
		if (keyword == "") continue;
		else if (keyword == OPT_MATRIX) {
			if (fmatrix_read) print_error ("Reading relationship matrix more than once.");
			else if (read_pedigree) print_error ("Conflictive use on relationship matrix: cannot be used together with pedigree information.");
			read_fmatrix (fr);
		} else print_error("Unknown format for relationship matrix. Does it start with the 'matrix' keyword?");
	}
}

/* ********************************************************************************************
 * Metapop::read_manage ()
 * --------------------------------------------------------------------------------------------
 * Call methods to read optional management input
 * ********************************************************************************************/
void Metapop::read_manage (std::stringstream& fr) {
	std::string keyword;
	while (getline(fr,keyword)) {

		// Restricted mates
		if (keyword == OPT_RESTRI) {
			if (restricted_mates) print_error ("Reading 'restrictions' matrix more than once.");
			read_mates (fr);

		} else if (keyword == OPT_FORBID) {
			if (forbidden_mates) print_error ("Reading 'forbidden' matrix more than once.");
			read_forbid (fr);

		} else if (keyword.size() && split_string (keyword, ' ')[0] == OPT_GENERATIONS) {
			if (!for_simulation) print_error ("Temporal marks for management ('t' in management file) should be only used under simulation mode");
			else if (temporal_marks) print_error ("Reading temporal marks for management ('t' in management file) more than once.");
			opt_tmp_mng (fr, keyword);

		} else if (keyword.size()) {
			rm_charseq_in_str (keyword, " \t");
			if (keyword.size()) wrong_optional (keyword);
		}
	}
}

/* ********************************************************************************************
 * Metapop::read_mates ()
 * --------------------------------------------------------------------------------------------
 * Read the restriction matrix, with yet formed pairs of mates and their offspring
 * ********************************************************************************************/
void Metapop::read_mates (std::stringstream& fr) {

	restricted_mates = true;
	if (DEBUG) std::cout << "Reading restrictions matrix..." << endl;
	mates.clear();
	std::string fem_string;

	// Initialize limit
	mates_limit.resize(N, vshort1D(2));

	// Read mate restrictions
	while (getline (fr,fem_string) && mates.size() < N_females) {
		fem_string = std::regex_replace(fem_string, std::regex("^ +| +$|( ) +"), "$1");
		std::vector<std::string> split_fem (split_string(fem_string, ' '));
		if (split_fem.size() !=4) print_error (ERROR_OPT_NCOLS);
		vint1D row_mates;
		row_mates.push_back(atoi(split_fem[0].c_str()));
		row_mates.push_back(atoi(split_fem[1].c_str()));
		row_mates.push_back(atoi(split_fem[2].c_str()));
		row_mates.push_back(atoi(split_fem[3].c_str()));
		mates.push_back(row_mates);
	}

	if (mates.size() != N_females) print_error (ERROR_OPT_MSIZE);

	// Check individuals IDs, and cast mates matrix into padopt
	std::size_t count (0), ind(0);
	std::size_t nfemales (0), nmales (0), sfemales (0), smales (0);
	vint1D vfemales, vmales;
	for (std::size_t i(0); i<n_subpops; ++i) {
		vint1D row_padopt;
		sfemales = 0;
		smales = 0;
		for (std::size_t j(0); j<deme[i].getPopulationSize(); ++j) {
			if (!deme[i].getIndividual(j).getSex()) {
				if (deme[i].getIndividual(j).getID() != mates[count][0]) {
					print_error (ERROR_OPT_FEMID);
				} else {
					std::size_t sir_index (check_sir (deme[i], mates[count][1]));
					row_padopt.push_back(sir_index);
					mates_limit[ind][0] = mates[count][2];
					mates_limit[ind][1] = mates[count][3];
					nfemales += mates_limit[ind][0];
					nmales += mates_limit[ind][1];
					sfemales += mates_limit[ind][0];
					smales += mates_limit[ind][1];
					++count;
				}
			} else {
				mates_limit[ind][0] = check_sir_contrib (deme[i].getIndividual(j).getID(), false);
				mates_limit[ind][1] = check_sir_contrib (deme[i].getIndividual(j).getID(), true);
			}
			++ind;
		}
		vfemales.push_back(sfemales);
		vmales.push_back(smales);
		mates_padopt.push_back(row_padopt);
	}

	// Check offspring and number of individuals in next generation
	std::size_t wfemales (0), wmales (0);
	for (std::size_t i(0); i<progeny.size(); ++i) {
		wfemales += progeny[i][0];
		wmales += progeny[i][1];
	}
	if ((nfemales < wfemales) || (nmales < wmales)) print_error (ERROR_OPT_NEWID);

	// Check if migrants are required
	bool need_migrants (false);
	int nmig (0);
	for (std::size_t i(0); i<progeny.size(); ++i) {
		if (progeny[i][0] > vfemales[i] || progeny[i][1] > vmales[i]) {
			need_migrants = true;
		}
		nmig -= (progeny[i][0]-vfemales[i]);
		nmig -= (progeny[i][1]-vmales[i]);
	}
	if (need_migrants) print_error ("Some subpopulations have delcared a desired number of offspring (set with 'newN') lower than the actual number of descendants declared in the restriction matrix.");

	if (DEBUG) {
		std::cout << std::endl;
		std::cout << "Done" << std::endl;
		std::cout << std::endl;
	}
}

/* ********************************************************************************************
 * Metapop::read_forbid ()
 * --------------------------------------------------------------------------------------------
 * Reads the forbid matrix, which determines forbidden mates for some or all females
 * ********************************************************************************************/
void Metapop::read_forbid (std::stringstream& fr) {

	forbidden_mates = true;
	if (DEBUG) std::cout << "Reading forbid matrix..." << endl;
	mates_forbid.clear();
	vstr2D smates_forbid;
	std::string fem_string;

	// Read forbidden mates
	while (getline (fr,fem_string) && mates_forbid.size() < N_females) {
		fem_string = std::regex_replace(fem_string, std::regex("^ +| +$|( ) +"), "$1");
		vstr1D split_fem (split_string(fem_string, ' '));
		if (repetition(split_fem)) print_error (ERROR_OPT_REPID);
		smates_forbid.push_back(split_fem);
		vint1D row_forbid;
		for (const auto& i: split_fem) row_forbid.push_back(atoi(i.c_str()));
		mates_forbid.push_back(row_forbid);
	}
	if (smates_forbid.size() != N_females) print_error (ERROR_OPT_FSIZE);

	// Check individuals IDs
	std::size_t count (0);
	for (std::size_t i(0); i<n_subpops; ++i) {
		for (std::size_t j(0); j<deme[i].getPopulationSize(); ++j) {
			vint1D row_forbid;
			if (!deme[i].getIndividual(j).getSex()) {
				if (deme[i].getIndividual(j).getID() != mates_forbid[count][0]) {
					print_error (ERROR_OPT_FEMIF);
				} else {
					for (std::size_t k(1); k<mates_forbid[count].size(); ++k) {
						mates_forbid[count][k] = check_sir(deme[i], mates_forbid[count][k]);
						row_forbid.push_back(mates_forbid[count][k]);
					}
					++count;
				}
			}
		}
	}

	// Check if males are available for all females
	count = 0;
	for (std::size_t i(0); i<n_subpops; ++i) {
		for (std::size_t j(0); j<deme[i].getPopulationSize(); ++j) {
			if (!deme[i].getIndividual(j).getSex()) {
				vint1D row_forbid (mates_forbid[count]);
				if (row_forbid.size() >= (deme[i].getNperSex(1)+1)) print_error (ERROR_OPT_MALEN);
				++count;
			}
		}
	}

	if (DEBUG) {
		std::cout << std::endl;
		std::cout << "Done" << std::endl;
		std::cout << std::endl;
	}
}

/* ********************************************************************************************
 * Metapop::check_sir ()
 * --------------------------------------------------------------------------------------------
 * This function returns the relative index of males in their subpopulation, given their ID
 * ********************************************************************************************/
std::size_t Metapop::check_sir (population pop, int id) {
	std::size_t index (0);
	for (std::size_t i(0); i<pop.getPopulationSize(); ++i) {
		if (pop.getIndividual(i).getSex()) {
			if (id == pop.getIndividual(i).getID()) return index;
			else ++index;
		}
	}
	print_error (ERROR_OPT_SIRID);
	return index;
}

/* ********************************************************************************************
 * Metapop::check_sir_contrib ()
 * --------------------------------------------------------------------------------------------
 * This function returns the offspring cotribution of a male given its ID
 * ********************************************************************************************/
std::size_t Metapop::check_sir_contrib (int id, bool gender) {
	std::size_t contrib (0);
	for (std::size_t i(0); i<mates.size(); ++i) {
		if (mates[i][1] == id) contrib += mates[i][2+gender];
	}
	return contrib;
}

/* ********************************************************************************************
 * Metapop::check_allele ()
 * --------------------------------------------------------------------------------------------
 * Check if alleles are entered in a correct format
 * ********************************************************************************************/
allele_t Metapop::get_allele ( std::string& s) {
	if (s=="0") return 0;
	if (!use_nucleotides && !isInt(s)) print_error ("Genotypes are expected to be entered as numbers, but characters are detected.");
	else if (use_nucleotides) {
		if (s=="A" || s=="a") return 1;
		else if (s=="C" || s=="c") return 2;
		else if (s=="T" || s=="t") return 3;
		else if (s=="G" || s=="g") return 4;
		else print_error ("Genotypes are expected to be entered as nucleotides, but non-nucleotide characters or numbers are detected.");
	}
	return atoi(s.c_str());
}

/* ********************************************************************************************
 * Metapop:: FREE methods ()
 * --------------------------------------------------------------------------------------------
 * List of methods to free memory of BT_parameter struct
 * ********************************************************************************************/
void Metapop::free_bt (std::string name) {

	if (name == "p") {
		for (auto& i: p.b_par) {
			delete i;
		}
		for (auto& i: p.t_par) {
			delete i;
		}
		p.b_par.clear();
		p.t_par.clear();
	} else if (name == "f") {
		for (auto& i: f.b_par) {
			delete i;
		}
		for (auto& i: f.t_par) {
			delete i;
		}
		f.b_par.clear();
		f.t_par.clear();
	} else if (name == "s") {
		for (auto& i: s.b_par) {
			delete i;
		}
		for (auto& i: s.t_par) {
			delete i;
		}
		s.b_par.clear();
		s.t_par.clear();
	} else if (name == "d") {
		for (auto& i: d.b_par) {
			delete i;
		}
		for (auto& i: d.t_par) {
			delete i;
		}
		d.b_par.clear();
		d.t_par.clear();
	} else {
		std::cerr << "ERROR: No BT_parameter with name: " << name << std::endl;
		exit(-1);
	}

}

void Metapop::free_bt_all () { // called after population analysis

	this->free_bt("p");
	this->free_bt("f");
	this->free_bt("s");
	this->free_bt("d");

}

void Metapop::print_error (std::string message) {
	std::string file_type (translate_format());
	std::cerr << std::endl;
	print_stars(message.size() + file_type.size() + 23);
	std::cerr << "INPUT ERROR (" << file_type << " format). " << message << std::endl;
	std::cerr << ">>> Check datafile format." << std::endl;
	print_stars(message.size() + file_type.size() + 23);
	exit(-1);
}

void Metapop::print_mng_error (std::string message) {
	std::cerr << std::endl;
	print_stars(message.size() + 38);
	std::cerr << "INPUT ERROR (management file format). " << message << std::endl;
	std::cerr << ">>> Check mng file format." << std::endl;
	print_stars(message.size() + 38);
	exit(-1);
}

std::string Metapop::translate_format () {
	std::string format ("metapop");
	if (input_format == GNP) return "genepop";
	if (input_format == PED) return "plink pedfile";
	return format;
}

void Metapop::wrong_optional (std::string word) {
	std::string error_message ("One or more optional inputs are invalid.");
	error_message += "\nStop at unexpected line containing: '" + word + "'.";
	print_error (error_message);
}

void Metapop::row_error (std::size_t row) {
	std::string error_message ("An error has been detected in row ");
	error_message += std::to_string(row) + ".";
	print_error (error_message);
}

void Metapop::short_frow () {
	std::string error_message ("Check matrix format: Expected more rows in relationship matrix file.");
	print_error (error_message);
}

void Metapop::large_frow () {
	std::string error_message ("Check matrix format: Found more rows than expected in relationship matrix file.");
	print_error (error_message);
}

void Metapop::large_cols (std::size_t row) {
	std::string error_message ("Check matrix format: Found more coancestry values than expected in row ");
	error_message += row + ".";
	print_error (error_message);
}

void Metapop::test_synth_inpmatrix ( Synthetic synth ) {
	if ( synth==AT || synth==AB || synth==AW || synth==NA || synth==ALL) {
		std::cerr << "ERROR: Cannot calculate population contributions to a pool using allelic information." << std::endl;
		std::cerr << "Remember that when a coancestry matrix is inputed, allelic information from molecular markers is ignored." << std::endl;
		exit (-1);
	}
}

void Metapop::test_management_inpmatrix ( Management manage) {
	if ( manage == ALLELIC || manage == N_ALLELES ) {
		std::cerr << "ERROR: Cannot run a management method using allelic information." << std::endl;
		std::cerr << "Remember that when a coancestry matrix is inputed, allelic information from molecular markers is ignored." << std::endl;
		exit (-1);
	}
}

void Metapop::test_mng () {
	if (!for_management) return;
	for (std::size_t i(0); i<n_subpops; ++i) {
		bool females (false), males (false);
		for (std::size_t j(0); j<deme[i].getPopulationSize(); ++j) {
			if (deme[i].getIndividual(j).getSex() == 0) females = true;
			else males = true;
			if (females & males) break;
		}
		if (!females | !males) print_error ("At least one female and male are required per subpopulation in order to run management methods.");
	}
}

void Metapop::test_rm () {
	if (n_loci > 0 || alleles_per_locus.size()) {
		print_warning("Molecular markers detected in input file, but a relationship matrix will be used instead (ignoring loci information).");
		n_loci = 0;
		alleles_per_locus.clear();
	}
}

void Metapop::test_pedigree (const vint2D& ped) {
	if (!ped.size()) print_error("Pedigree is empty?");
	vint1D pat,mat;
	for (const auto& i: ped) {
		pat.push_back(i[1]);
		mat.push_back(i[2]);
	}
	if (!variation(pat) | !variation(mat)) print_error("No variation found for at least one of the parent pedigree identities");
}

void Metapop::test_final () {
	if (read_pedigree && (n_loci > 0 || alleles_per_locus.size())) {
		print_warning("Molecular markers detected in input file, but pedigree information will be used instead (ignoring loci).");
		n_loci = 0;
		alleles_per_locus.clear();
	}
}
