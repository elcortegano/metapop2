#ifndef POPULATION_H
#define	POPULATION_H


#include <string>
#include <vector>
#include "individual.h"
#include <iostream>
using namespace std;

class population {

	std::size_t N; // Size
	std::size_t Ne; // Effective size
	std::vector <individual> pop; // Population
	std::string label;
	std::vector <std::size_t> indLocus;
	std::vector <std::size_t> NperSex; // vector of size 2 with the number of females and males

public:

	population ( std::string , std::vector<individual>& );
	void addIndividuals ( std::vector<individual> );

	individual getIndividual ( std::size_t i ) const { return pop[i]; }
	std::size_t getPopulationSize() const { return N; }
	std::size_t getEffectiveSize() const {  return Ne; }
	std::string getLabel() const { return label; }
	int getIndPerLocus ( std::size_t l ) const { return indLocus[l]; }
	std::size_t getNperSex ( std::size_t i ) const {  return NperSex[i]; }

protected:

	void sortInds ();
	void calcIndPerLoci ( int );
	void calcNperSex();
};

#endif
