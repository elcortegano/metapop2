/* =======================================================================================
 *                                                                                       *
 *       Filename:  convert.cpp                                                          *
 *                                                                                       *
 *    Description: This file defines functions to convert Metapop format into others     *
 *                 such as Genepop (Rousset F. 2008) or PLINK (Purcell S. et al. 2007)   *
 *                                                                                       *
 ======================================================================================= */

#include "convert.h"


/* ********************************************************************************************
 * convert2gnp ()
 * --------------------------------------------------------------------------------------------
 * Convert Metapop files into Genepop files
 * ********************************************************************************************/
void convert2gnp (std::string filename, std::vector<population>& deme, const allele2D& alleles) {

	std::size_t loci (deme[0].getIndividual(0).getLociNumber());
	std::ofstream genepop_file;
	genepop_file.open(filename, ios::out);

	if (!genepop_file) {
		std::cerr << "Problem creating output file" << std::endl;
		exit(-1);
	}

	genepop_file << "File " << filename << " converted by metapop2 " << endl;
	for (std::size_t i(0); i<loci; i++){
		genepop_file << "Locus_" << i+1 << std::endl;
	}

	std::size_t digits (1);
	allele_t max (0);
	for (std::size_t l(0);l < alleles.size(); ++l) {
		if (DEBUG) std::cout << "\r  Reading locus " << l+1 << " / " << alleles.size();
		for (std::size_t a(0); a < alleles[l].size(); ++a) {
			if (alleles[l][a] > max ) {
				max = alleles[l][a];
			}
		}
	}

	while (max /= 10) ++digits;
	if (DEBUG) std::cout << std::endl;
	for (std::size_t p (0); p < deme.size(); ++p) {
		if (DEBUG) std::cout << "\r  Writting population " << p+1 << " / " << deme.size();
		genepop_file << "Pop " << deme[p].getLabel() << std::endl;
		for (std::size_t i(0); i<deme[p].getPopulationSize(); ++i) {
			individual ind (deme[p].getIndividual(i));
			genepop_file << deme[p].getLabel() << p << i << ", ";
			for (std::size_t l(0); l< loci; l++){
				allele_t a (ind.getLocus(l).getA(0));
				allele_t b (ind.getLocus(l).getA(1));
				genepop_file << std::setfill('0') << setw(digits) << a << setw(digits) << b << "\t";
			}
			genepop_file << std::endl;
		}
	}

	genepop_file.close();

}

/* ********************************************************************************************
 * convert2ped ()
 * --------------------------------------------------------------------------------------------
 * Convert Metapop files into PLINK PED files
 * ********************************************************************************************/
void convert2ped (std::string filename, std::vector<population>& deme, bool use_nucleotides, int ploidy, bool set_sex) {

	std::size_t loci (deme[0].getIndividual(0).getLociNumber());
	std::ofstream ped_file;
	ped_file.open(filename, ios::out);

	if (!ped_file) {
		std::cerr << "Problem creating output file" << std::endl;
		exit(-1);
	}

	for (std::size_t p(0); p < deme.size(); ++p) {
		std::string FID (deme[p].getLabel());
		for (std::size_t i(0); i < deme[p].getPopulationSize(); ++i) {
			individual ind (deme[p].getIndividual(i));
			std::string IDi (ind.getLabel());
			int gender (0);
			if (set_sex) {
				if (!ind.getSex()) gender = 2; // in plink format, females=2 and males=1
				else gender = 1;
			}
			std::string IDs ("0"), IDd ("0"), phe ("-9"); // parents and phenotype are unkown
			ped_file << FID << " " << IDi << " " << IDs << " " << IDd << " " << gender << " " << phe;
			for (std::size_t l(0); l < loci; ++l) {
				locus gen (ind.getLocus(l));
				if (!use_nucleotides) {
					ped_file << " " << gen.getA(0);
					if (ploidy==2) ped_file << " " << gen.getA(1);
				} else {
					ped_file << " " << print_nucleotide(gen.getA(0));
					if (ploidy==2) ped_file << " " << print_nucleotide(gen.getA(1));
				}
			}
			ped_file << std::endl;
		}
	}
	ped_file.close();
}

/* ********************************************************************************************
 * convert_deme2mtp ()
 * --------------------------------------------------------------------------------------------
 * Convert Metapop files from Deme objects
 * ********************************************************************************************/
void convert_deme2mtp (std::string filename, std::vector<population>& deme, bool use_nucleotides, int ploidy) {

	std::size_t loci (deme[0].getIndividual(0).getLociNumber());
	std::ofstream mtp_file;
	mtp_file.open(filename, ios::out);

	if (!mtp_file) {
		std::cerr << "Problem creating output file" << std::endl;
		exit(-1);
	}

	for (std::size_t i(0); i<deme.size(); ++i) {
		mtp_file << deme[i].getLabel() << std::endl;
		for (std::size_t j(0); j<deme[i].getPopulationSize(); ++j) {
			individual ind (deme[i].getIndividual(j));
			mtp_file << ind.getLabel() << ',';
			for (std::size_t k(0); k<loci; ++k) {
				if (!use_nucleotides) {
					mtp_file << ' ' << ind.getLocus(k).getA(0);
					if (ploidy==2) mtp_file << ' ' << ind.getLocus(k).getA(1);
				} else {
					mtp_file << ' ' << print_nucleotide(ind.getLocus(k).getA(0));
					if (ploidy==2) mtp_file << ' ' << print_nucleotide(ind.getLocus(k).getA(1));
				}
			}
			mtp_file << std::endl;
		}
	}

	mtp_file.close();
}

/* ********************************************************************************************
 * convert ()
 * --------------------------------------------------------------------------------------------
 * General function to call specific conversion functions
 * ********************************************************************************************/
void convert (const Metapop& input, const MTPConfigFile& config) {

	if (DEBUG) std::cout << "---------------------------------------" << std::endl;
	std::vector<population> deme (input.get_deme());
	bool set_sex (input.get_setsex());
	std::string caso (config.get_caso());
	std::string mode (config.get_convert_mode());
	bool use_nucleotides (config.get_use_nucleotides());
	int ploidy (config.get_ploidy());
	allele2D alleles (input.get_alleles_per_locus());

	if (mode == "mtp2gp") {
		if (use_nucleotides) {
			std::cerr << "Genepop format require genotypes in numeric (non-nucleotide) format." << std::endl;
			exit(-1);
		}
		std::string genepop_filename (caso + ".gen");
		std::cout << "Converting " << input.get_filename() << " (Metapop format) into " << genepop_filename << " (Genepop format)" << std::endl;
		convert2gnp (genepop_filename, deme, alleles);
	} else if (mode == "mtp2ped") {
		std::string ped_filename (caso + ".ped");
		std::cout << "Converting " << input.get_filename() << " (Metapop format) into " << ped_filename << " (Plink PED format)" << std::endl;
		convert2ped (ped_filename, deme, use_nucleotides, ploidy, set_sex);
	} else if (mode == "gp2mtp" || mode == "ped2mtp") {
		if (use_nucleotides && mode == "gp2mtp") {
			std::cerr << "Genepop format require genotypes in numeric (non-nucleotide) format." << std::endl;
			exit(-1);
		}
		std::string mtp_filename (caso + ".mtp");
		std::cout << "Converting " << input.get_filename() << " (Genepop format) into " << mtp_filename << " (Metapop format)" << std::endl;
		convert_deme2mtp (mtp_filename, deme, use_nucleotides, ploidy);
	} else {
		std::cerr << "Unknown conversion mode. Select one of 'none', 'mtp2gp', 'mtp2ped', 'gp2mtp' or 'ped2mtp'." << std::endl;
		exit(-1);
	}

	std::cout << "Conversion done!" << std::endl;
	if (DEBUG) std::cout << std::endl;

}
