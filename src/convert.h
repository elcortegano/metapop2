#ifndef CONVERT_H
#define CONVERT_H

#include <iomanip>
#include "metapop.h"
#include "output.h"

// =====   FUNCTION'S PROTOTYPES   =====
void convert2gnp (std::string, const std::vector<population>&, const allele2D&);
void convert2ped (std::string, const std::vector<population>&, bool, int);
void convert_deme2mtp (std::string, const std::vector<population>&, bool);
void convert (const Metapop&, const MTPConfigFile&);


#endif
