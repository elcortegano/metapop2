/* =======================================================================================
 *                                                                                       *
 *       Filename:  MTPConfigFile.cpp                                                    *
 *                                                                                       *
 *    Description: This file contaings methods to operate with class MTPConfigFile,      *
 * 			       which inherites from ConfigFile class, and defines METAPOP config     *
 *                 file.                                                                 *
 *                                                                                       *
 *                  -> MTPConfigFile::MTPConfigFile ()                                   *
 *                  -> MTPConfigFile::read* ()                                           *
 *                  -> MTPConfigFile::check ()                                           *
 *                  -> MTPConfigFile::get_* ()                                           *
 *                                                                                       *
 ======================================================================================= */

#include "MTPConfigFile.h"


/* ********************************************************************************************
 * MTPConfigFile::MTPConfigFile ()
 * --------------------------------------------------------------------------------------------
 * Class MTPConfigFile constructor. Defines default variables for config parameters, and change
 * them after reading the configuration file. It also checks that the format of the config file
 * is correct.
 * ********************************************************************************************/
MTPConfigFile::MTPConfigFile (int argc, char** argv, std::string& datafilename) {

	if (argc<2) config_error("No options or data file entered.");
	std::vector<std::string> arguments;
	for (int i(1); i<argc; ++i) {
		arguments.push_back(argv[i]);
	}

	/* ENTRY METHOD */
	bool options (false);
	bool config_option (false);
	file_is_read = false;
	format_call = false;
	for (std::size_t i(0); i<(arguments.size()-1); ++i) {
		std::string opt (arguments[i]);
		if (opt[0] == '-' && opt[1] == '-') {
			options = true;
		} else if (options) {
			config_error ("Expected double-dash options, except for the data file. Check the README file to learn more about the input methods.");
		}
	}

	// Current version allows the use of default settings, calling only a datafile
	if (argc==2) {
		if (arguments[0] == "--help") print_help();
		entry_method = DEFAULT;
		datafilename = arguments[0];
		arguments.pop_back();
	} else if (options) {
		entry_method = ADVANCED;
		// The last argument must be the datafile
		datafilename = arguments[arguments.size()-1];
		arguments.pop_back();
		// Check if 'config' option is invoqued
		file = check_option_config (arguments, config_option);
	} else if (argc == 3) {
		entry_method = LEGACY;
		datafilename = arguments[0];
		file = arguments[1];
		arguments.pop_back();
		arguments.pop_back();
	} else if (argc < 2) {
		config_error ("There are some arguments missing. Check the README file to learn more about the input methods.");
	} else if (argc > 3) {
		config_error ("There are more arguments than expected. Check the README file to learn more about the input methods.");
	}

	/* CONFIGFILE VARIABLES */
	myDelimiter = "=";
	myComment = "#";
	mySentry = "EndConfigFile";

	/* READ CONFIGURATION FILE */
	if (entry_method == LEGACY || config_option) {
		std::ifstream in( file.c_str() );
		if (!in) {
			throw file_not_found (file);
		} else {
			in >> (*this);
		}
	}

	/* IN THE DEFAULT METHOD, OR IF A PARAMETER IS MISSING, A DEFAULT VALUE IS GIVEN */

	// General options
	if (!this->readInto(caso, "name")) caso = DEFAULT_CASO;
	format = this->readFormat ("format");
	this->readInto(debug, "debug", DEFAULT_DEBUG);
	this->readInto(use_nucleotides, "nucleotide", DEFAULT_NUCLEOTIDE);
	this->readInto(decF, "dec", DEFAULT_DECF);
	this->readInto(seed, "seed", (int)time(0));
	this->readInto(print_coan, "save_coancestry", DEFAULT_PRINTCOAN);
	this->readInto(save_tsv, "save_tsv", DEFAULT_SAVETSV);
	this->readInto(pairwise_stats_matrix, "pairwise_stats", DEFAULT_PAIRWISE_STATS);
	this->readInto(ploidy, "ploidy", DEFAULT_POLIDY);

	// Configuration for analysis of population
	this->readInto(NCcorr, "NeiChesser", DEFAULT_NCCORR);
	this->readInto(ADrare, "rarefaction", DEFAULT_ADDRARE);
	this->readInto(ADrareN, "rareN", DEFAULT_ADDRAREN);
	this->readInto(bt_n, "bootstrap", DEFAULT_BT_N);
	this->readInto(bt_alpha, "alpha", DEFAULT_BT_ALPHA);

	// Configuration for a synthetic population
	synth = this->readSynthetic ("synthetic");
	this->readInto(nPool, "nPool", DEFAULT_NPOOL);
	this->readInto(minRank, "minimum", DEFAULT_MINRANK);

	// Configuration for management method
	manage = this->readManagement ("manage");
	this->readInto(mng_file, "mng_file", DEFAULT_MNG_FILE);
	method = this->readMethod ("method");
	this->readInto(convergence, "convergence", DEFAULT_CONVERGENCE);

	// Restrictions
	this->readInto(nmigra, "max_migrants", DEFAULT_NMIGRA);
	this->readInto(lambda, "lambda", DEFAULT_LAMBDA);
	this->readInto(monogamy, "monogamy", DEFAULT_MONOGAMY);
	this->readInto(weight_F_isol, "weight_F_isol", DEFAULT_WEIGHT_F_ISOL);
	this->readInto(max_off_mate, "max_off_mate", DEFAULT_MAXOFF);

	// Simulated annealing
	this->readInto(SA_steps, "SA_steps", DEFAULT_SA_STEPS);
	this->readInto(SA_t, "SA_temp", DEFAULT_SA_T);
	this->readInto(SA_k, "SA_k", DEFAULT_SA_K);

	// Configurartion for simulation
	this->readInto(simMode, "simMode", DEFAULT_SIMMODE);
	if (simMode) {
		this->readInto(saveStats, "saveStats", DEFAULT_SAVESTATS);
		this->readInto(saveFrecs, "saveFrequencies", DEFAULT_SAVEFRECS);
		this->readInto(saveMigra, "saveMigrants", DEFAULT_SAVEMIGRA);
	}
	this->readInto(simReps, "simReps", DEFAULT_SIMREPS);
	this->readInto(simGens, "simGens", DEFAULT_SIMGENS);

	// Compatibility options
	if (!this->readInto(convert_mode, "convert")) convert_mode = DEFAULT_CONVERT_MODE;

	// Not specified or in use (yet)
    this->readInto(lambda_rank, "lambda_synth", DEFAULT_LAMBDA_RANK);
	this->readInto(tasa, "max_delta_F", DEFAULT_TASA);

	// Check read of configuration file
	if ((entry_method == LEGACY || config_option) && !file_is_read) config_error ("Configuration file set, but no options have been read");

	// Read parameters from terminal
	while (arguments.size()) {
		check_option (arguments[0]);
		arguments.erase(arguments.begin());
	}

	// Check input parameters
	this->check();
	if (debug) this->print_settings();

}

/* ********************************************************************************************
 * MTPConfigFile::read methods ()
 * --------------------------------------------------------------------------------------------
 * Methods that read special variables from the config file
 * ********************************************************************************************/
Format MTPConfigFile::readFormat (const std::string& key) {

	Format value (DEFAULT_FORMAT);
	s_format = DEFAULT_SFORMAT;
	mapci p = myContents.find(key);
	bool found = ( p != myContents.end() );
	if (found) {
		s_format = p->second;
	} else {
		return DEFAULT_FORMAT;
	}

	if (s_format == "mtp") {
		value = MTP;
	} else if (s_format == "ped" || s_format == "pedigree") {
		value = PED;
	} else if (s_format == "gp") {
		value = GNP;
	} else {
		config_error ("Unidentified 'format' parameter value.");
	}

	return value;
}

Management MTPConfigFile::readManagement (const std::string& key) {

	// Read the management method parameter. If none is entered, default is returned
	Management value;
	s_method = DEFAULT_SMANAGE;
	mapci p = myContents.find(key);
	bool found = ( p != myContents.end() );
	if (found) {
		s_manage = p->second;
	} else {
		return DEFAULT_MANAGE;
	}

	// Translation
	value = translate_management();

	return value;
}

Management MTPConfigFile::translate_management () {
	Management value (DEFAULT_MANAGE);
	if (s_manage == "genetic") {
		value = GENETIC;
	} else if (s_manage == "allelic") {
		value = ALLELIC;
	} else if (s_manage == "Nalleles") {
		value = N_ALLELES;
	} else if (s_manage == "random") {
		value = RANDOM;
	} else if (s_manage == "none") {
		value = NONE;
	} else {
		config_error ("Unidentified 'manage' population parameter.");
	}
	return value;
}

/* -------------------------------------------------------------------------------------------- */
Method MTPConfigFile::readMethod (const std::string& key) {

	// Read the management method parameter. If none is entered, default is returned
	Method value;
	s_method = DEFAULT_SMETHOD;
	mapci p = myContents.find(key);
	bool found = ( p != myContents.end() );
	if (found) {
		s_method = p->second;
	} else {
		return DEFAULT_METHOD;
	}

	// Translation
	value = translate_method();

	return value;
}

Method MTPConfigFile::translate_method () {
	Method value (DEFAULT_METHOD);
	if (s_method == "dynamic") {
		value = DYNAMIC;
	} else if (s_method == "ompg") {
		value = OMPG;
	} else if (s_method == "isolated") {
		value = ISOLATED;
	} else {
		config_error("Unidentified 'method' population parameter.");
	}
	return value;
}

/* -------------------------------------------------------------------------------------------- */
Synthetic MTPConfigFile::readSynthetic (const std::string& key) {

	// Read the management method parameter. If none is entered, default is returned
	Synthetic value;
	s_synth = DEFAULT_SSYNTH;
	mapci p = myContents.find(key);
	bool found = ( p != myContents.end() );
	if (found) {
		s_synth = p->second;
	} else {
		return DEFAULT_SYNTH;
	}

	// Translation
	value = translate_synth ();

	return value;
}

Synthetic MTPConfigFile::translate_synth () {
	Synthetic value (DEFAULT_SYNTH);
	/*if (s_synth == "Gt") {
		value = GT;
	} else if (s_synth == "Gw") {
		value = GW;
	} else if (s_synth == "Gb") {
		value = GB;
	} else if (s_synth == "At") {
		value = AT;
	} else if (s_synth == "Aw") {
		value = AW;
	} else if (s_synth == "Ab") {
		value = AB;
	} else*/ if (s_synth == /*"NA"*/"K") {
		value = NA;
	} else if (s_synth == "H") {
		value = HE;
	} else if (s_synth == "all") {
		value = ALL;
	} else if (s_synth == "none") {
		value = NO;
	} else {
		config_error("Unidentified 'synthetic' population parameter.");
	}
	return value;
}

/* ********************************************************************************************
 * MTPConfigFile:: check ()
 * --------------------------------------------------------------------------------------------
 * Checks the correct format of the config file, and modifies some parameters
 * ********************************************************************************************/
void MTPConfigFile::check () {

	// Deprecate options
	if (pairwise_stats_matrix != DEFAULT_PAIRWISE_STATS) config_error("'pairwise_stats' option was deprecated in v2.4.2");

	// Check ranges and invalid values
	if (convergence < 50 || convergence > 5000) config_error ("Management convergent steps must be between 50 and 5000.");

	// Some paraters are defined in function of the values of others
	if (method == DYNAMIC) isol = false;
	else if (method == OMPG) isol = true;
	else if (method == ISOLATED) isol = true;

	// The configuration for the simulation mode has some restrictions
	if (simMode && manage==NONE){
		config_error ("Cannot run simulation mode without management information.");
	}
	if (simMode) {
		simGens ++; // Add 1 to the generations to end in the desired generation
		if (!saveStats && !saveFrecs && !saveMigra) {
			print_warning("You have set simMode = true in the configuration file, but disabled all the output from simulations (saveFrecs, saveStats and saveMigra) = false.");
			print_warning("Simulation mode without saving any result is non-sense. This run will skip the simulation.");
			simMode=false;
		}
	}
	if (!simMode) {
		if (simReps != DEFAULT_SIMREPS) {
			print_warning("Simulation replicates set, but simulation mode has not been enabled.");
			simReps = DEFAULT_SIMREPS;
		}
		if (simGens != DEFAULT_SIMGENS) {
			print_warning("Simulation generations set, but simulation mode has not been enabled.");
			simGens = DEFAULT_SIMGENS;
		}
		saveFrecs = false;
		saveStats = false;
		saveMigra = false;
	}

	// If there's a readable string out of the list of validad config parameters, the format is wrong
	for (std::map<std::string, std::string>::iterator iter = myContents.begin(); iter != myContents.end(); ++iter) {
		if (!this->valid_parameter(iter->first)) {
			std::string error_message ("Parameter \"" + iter->first + "\" in file " + file + " does not match a valid METAPOP configuration parameter.");
			config_error (error_message);
		}
	}

	// Compatibility options
	if ((format==GNP) && ((manage != NONE) || (mng_file!=""))) config_error ("Cannot run management method with Genepop input format.");
	if (manage != NONE && ploidy==1) config_error ("Cannot run management for haploid genotypes. Compute contributions to a synthetic pool instead.");
	if (manage != NONE && ploidy>2) config_error ("Cannot run management for polyploid genotypes. Compute contributions to a synthetic pool instead.");
	if (mng_file != "" && manage == NONE) config_error ("Cannot read management file without specifying a management method.");
	if ((format==GNP) && (ploidy==1)) config_error ("Genepop format is not supported for haploid genomes.");
	if ((format==GNP) && (ploidy>2)) config_error ("Genepop format is not supported for polyploid genomes.");
	if ((format==GNP) && use_nucleotides) config_error ("Genepop format require genotypes in numeric (non-nucleotide) format.");
	if ((format==PED) && (ploidy>2)) config_error ("PLINK PED format is not supported for polyploid genomes. Convert it to Metapop format.");
	if ((format==GNP) && (format==PED)) config_error ("Cross-software compatibility error. Enable Genepop or PLINK input option, but not both.");
	if (s_format=="pedigree" && ploidy != 2) config_error ("Cannot read pedigree for non-diploid data");

	// Conversion mode only accepts Metapop input files
	if (convert_mode != "none") {
		if (ploidy>2) config_error ("Conversion mode is not allowed for polyploids.");
		if (((format==GNP) || (format==PED)) && (convert_mode == "mtp2gp" || convert_mode == "mtp2ped")) {
			config_error ("Conversion mode to Genepop/PLINK requires input files in Metapop format.");
		} else if (convert_mode == "gp2mtp" && !(format==GNP)) {
			config_error ("Conversion mode to Metapop from Genepop requires to specify that Genepop format is being used");
		} else if (convert_mode == "ped2mtp" && !(format==PED)) {
			config_error ("Conversion mode to Metapop from PLINK requires to specify that PLINK format is being used");
		}
		if (ploidy==1 && (convert_mode == "mtp2gp" || convert_mode == "gp2mtp")) {
			config_error ("Genepop format cannot be used for haploid genomes.");
		}
	}
}

/* ********************************************************************************************
 * MTPConfigFile:: check_option_config ()
 * --------------------------------------------------------------------------------------------
 * In the advanced entry method, it checks if there is a config option, and returns the file.
 * ********************************************************************************************/
std::string MTPConfigFile::check_option_config ( std::vector<std::string>& arguments, bool& use_option) {

	std::string config_filename ("");
	bool use_config (false);

	for (std::size_t i(0); i<arguments.size(); ++i) {
		std::string s_arg (arguments[i]);
		std::vector<std::string> v_argument = split_string (s_arg, '=') ;
		std::string argument (v_argument[0]);
		if (argument == "--config") {
			if (use_config) {
				config_error ("Config option declared more than once.");
			}
			use_config = true;
			if (v_argument.size() > 1) {
				config_filename = v_argument[1];
				use_option = true;
				arguments.erase(arguments.begin()+i);
			}
		} else {
			continue;
		}
	}

	return config_filename;
}

void MTPConfigFile::check_option ( std::string argument) {

	/* LOCAL VARIABLES */
	std::vector<std::string> v_argument = split_vector (argument, '=');
	std::string arg (v_argument[0]);
	std::string value;
	if (v_argument.size() > 1) value = v_argument[1];

	// Read non-boolean options
	if (arg == "--name") {
		caso = value;
	} else if (arg == "--dec") {
		decF = atoi(value.c_str());
	} else if (arg == "--seed") {
		seed = atoi(value.c_str());
	} else if (arg == "--rareN") {
		ADrareN = atoi(value.c_str());
	} else if (arg == "--bootstrap") {
		bt_n = atoi(value.c_str());
		if (bt_n < 1) print_warning("Number of bootstrap iterations is < 1. Bootstraping will be ignored. Use e.g. --bootstrap=100 to enable it.");
	} else if (arg == "--alpha") {
		bt_alpha = atof(value.c_str());
	} else if (arg == "--synth") {
		s_synth = value;
		synth = translate_synth();
	} else if (arg == "--synth-pool") {
		nPool = atoi(value.c_str());
	} else if (arg == "--synth-min") {
		minRank = atof (value.c_str());
	} else if (arg == "--manage") {
		s_manage = value;
		manage = translate_management ();
	} else if (arg == "--mng-file") {
		mng_file = value;
	} else if (arg == "--rmatrix") {
		rm_file = value;
	} else if (arg == "--method") {
		s_method = value;
		method = translate_method ();
	} else if (arg == "--convergence") {
		convergence = atoi(value.c_str());
	} else if (arg == "--max-nmigrants") {
		nmigra = atoi(value.c_str());
	} else if (arg == "--lambda") {
		lambda = atof(value.c_str());
	} else if (arg == "--max-delta-F") {
		tasa = atof(value.c_str());
	} else if (arg == "--weight-F-isol") {
		weight_F_isol = atof(value.c_str());
	} else if (arg == "--max-off-mate") {
		max_off_mate = atoi(value.c_str());
	} else if (arg == "--ploidy") {
		ploidy = atoi(value.c_str());
	} else if (arg == "--sim-t") {
		simGens = atoi(value.c_str());
	} else if (arg == "--sim-r") {
		simReps = atoi(value.c_str());
	} else if (arg == "--SA-steps") {
		SA_steps = atoi(value.c_str());
	} else if (arg == "--SA-temp") {
		SA_t = atof(value.c_str());
	} else if (arg == "--SA-k") {
		SA_k = atof(value.c_str());
	// Read boolean options
	} else if (arg == "--debug") {
		debug = true;
	} else if (arg == "--nucleotide") {
		use_nucleotides = true;
	} else if (arg == "--pairwise-stats") {
		config_error("'pairwise_stats' option was deprecated in v2.4.2");
		pairwise_stats_matrix = true;
	} else if (arg == "--haploid") {
		ploidy = 1;
	} else if (arg == "--save-rmatrix") {
		print_coan = true;
	} else if (arg == "--save-tsv") {
		save_tsv = true;
	} else if (arg == "--nei-chesser") {
		NCcorr = true;
	} else if (arg == "--rarefaction") {
		ADrare = true;
	} else if (arg == "--no-rarefaction") {
		ADrare = false;
	} else if (arg == "--monogamy") {
		monogamy = true;
	} else if (arg == "--polygamy") {
		monogamy = false;
	} else if (arg == "--mtp") {
		if (format_call) config_error ("Conflictive specifications of input datafile format");
		format = MTP;
		s_format = "mtp";
		format_call = true;
	} else if (arg == "--gp") {
		if (format_call) config_error ("Conflictive specifications of input datafile format");
		format = GNP;
		s_format = "gp";
		format_call = true;
	} else if (arg == "--ped" || arg == "--pedigree") {
		if (format_call) config_error ("Conflictive specifications of input datafile format");
		format = PED;
		s_format = "ped";
		if (arg == "--pedigree") s_format = "pedigree";
		format_call = true;
	} else if (arg == "--simMode") {
		simMode = true;
		saveStats = true;
		saveFrecs = true;
		saveMigra = true;
	} else if (arg == "--mtp2ped") {
		convert_mode = "mtp2ped";
	} else if (arg == "--mtp2gp") {
		convert_mode = "mtp2gp";
	} else if (arg == "--gp2mtp") {
		convert_mode = "gp2mtp";
	} else if (arg == "--ped2mtp") {
		convert_mode = "ped2mtp";
	// Unknown option
	} else {
		config_error ("At least one entered option is unknown.");
	}

}
/* ********************************************************************************************
 * MTPConfigFile:: valid_paramter ()
 * --------------------------------------------------------------------------------------------
 * Checks if a read key is a valid parameter or not
 * ********************************************************************************************/
bool MTPConfigFile::valid_parameter (std::string key) {

	std::vector<std::string> parameters {
		"alpha",
		"bootstrap",
		"convergence",
		"convert",
		"debug",
		"dec",
		"format",
		"lambda",
		"lambda_synth",
		"manage",
		"max_delta_F",
		"max_migrants",
		"max_off_mate",
		"method",
		"minimum",
		"mng_file",
		"monogamy",
		"name",
		"NeiChesser",
		"nucleotide",
		"nPool",
		"pairwise_stats",
		"ploidy",
		"poolFile",
		"rarefaction",
		"rareN",
		"rmatrix",
		"save_rmatrix",
		"save_tsv",
		"saveFrequencies",
		"saveMigrants",
		"saveStats",
		"SA_k",
		"SA_temp",
		"SA_steps",
		"seed",
		"simGens",
		"simMode",
		"simReps",
		"synthetic",
		"weight_F_isol"
	};

	for (const auto& i : parameters) {
		if (key == i) {
			return true;
		}
	}

	return false;
}

/* ********************************************************************************************
 * MTPConfigFile::print_settings ()
 * --------------------------------------------------------------------------------------------
 * If debug mode is activated, it prints a header indicating the program settings.
 * ********************************************************************************************/
void MTPConfigFile::print_settings () {
	std::cout << "***************************************" << std::endl;
	std::cout << "       DEBUG MODE IS ACTIVATED" << std::endl;
	std::cout << "***************************************" << std::endl;
	std::cout << std::endl;
	std::cout << "---------------------------------------" << std::endl;
	std::cout << "SETTING PARAMETERS:" << std::endl;
	std::cout << "General options" << std::endl;
	std::cout << " - name: " << caso << std::endl;
	std::cout << " - format: " << s_format << std::endl;
	std::cout << " - nucleotide: " << use_nucleotides << std::endl;
	std::cout << " - ploidy: " << ploidy << std::endl;
	std::cout << " - debug: " << debug << std::endl;
	std::cout << " - dec: " << decF << std::endl;
	std::cout << " - save_rmatrix: " << print_coan << std::endl;
	std::cout << " - save_tsv: " << save_tsv << std::endl;
	//std::cout << " - pairwise_stats: " << pairwise_stats_matrix << std::endl;
	std::cout << " - seed: " << seed << std::endl;
	std::cout << "Population analysis" << std::endl;
	std::cout << " - NeiChesser: " << NCcorr << std::endl;
	std::cout << " - rarefaction: " << ADrare << std::endl;
	std::cout << " - rareN: " << ADrareN << std::endl;
	std::cout << " - bootstrap: " << bt_n << std::endl;
	std::cout << " - alpha: " << bt_alpha << std::endl;
	std::cout << " - synthetic: " << s_synth << std::endl;
	std::cout << " - nPool: " << nPool << std::endl;
	std::cout << " - minimum: " << minRank << std::endl;
	std::cout << "Management" << std::endl;
	std::cout << " - manage: " << s_manage << std::endl;
	std::cout << " - mng-file: " << mng_file << std::endl;
	std::cout << " - rmatrix: " << rm_file << std::endl;
	std::cout << " - method: " << s_method << std::endl;
	std::cout << " - convergence: " << convergence << std::endl;
	std::cout << "Restrictions" << std::endl;
	std::cout << " - max_migrants: " << nmigra << std::endl;
	std::cout << " - lambda: " << lambda << std::endl;
	std::cout << " - monogamy: " << monogamy << std::endl;
	std::cout << " - max_delta_F: " << tasa << std::endl;
	std::cout << " - weight_F_isol: " << weight_F_isol << std::endl;
	std::cout << "Simulated annealing" << std::endl;
	std::cout << " - SA_steps: " << SA_steps << std::endl;
	std::cout << " - SA_temp: " << SA_t << std::endl;
	std::cout << " - SA_k: " << SA_k << std::endl;
	std::cout << "Simulation mode" << std::endl;
	std::cout << " - simMode: " << simMode << std::endl;
	std::cout << " - simReps: " << simReps << std::endl;
	std::cout << " - simGens: " << simGens << std::endl;
	std::cout << " - saveStats: " << saveStats << std::endl;
	std::cout << " - saveFrequencies: " << saveFrecs << std::endl;
	std::cout << " - saveMigrants: " << saveMigra << std::endl;
	std::cout << "Cross-software compatibility" << std::endl;
	std::cout << " - convert: " << convert_mode << std::endl;
	std::cout << " - lambda_synth: " << lambda_rank << std::endl;
	std::cout << std::endl;
	std::cout << "---------------------------------------" << std::endl;

}

/* ********************************************************************************************
 * MTPConfigFile::print_help ()
 * --------------------------------------------------------------------------------------------
 *  Returns a short summary of the program options
 * ********************************************************************************************/
void MTPConfigFile::print_help () {

	std::cout << "SYNOPSIS: metapop <datafile> <configfile>" << std::endl;
	std::cout << "          metapop <datafile>" << std::endl;
	std::cout << "          metapop [--config=<configfile>] [options] <datafile>" << std::endl;
	std::cout << "          metapop --help" << std::endl;
	std::cout << std::endl;
	std::cout << "DESCRIPTION: Analysis of gene and allelic diversity in subdivided populations, as well as a tool for management in conservation programs." << std::endl;
	std::cout << std::endl;
	std::cout << "OPTIONS:" << std::endl;
	std::cout << std::endl << "  General options" << std::endl;
	std::cout << "  --config=<FILE>\tIt allows to read a configuration file and all parameters within." << std::endl;
	std::cout << "  --help\t\tPrints this help." << std::endl;
	std::cout << "  --name=<NAME>\t\tGive a name to the run analysis." << std::endl;
	std::cout << "  --ped/--mtp/--gp\tSet the input format to plink PED (ped, by default), metapop (mtp) or genepop (gp)." << std::endl;
	std::cout << "  --pedigree\t\tInput datafile has PLINK PED format, and genealogical information instead of molecular is read." << std::endl;
	std::cout << "  --rmatrix\t\tName of the relationship matrix file" << std::endl;
	std::cout << "  --save-rmatrix\tGenerates a relationship matrix file." << std::endl;
	std::cout << "  --save-tsv\t\tSaves tabular results into TSV files." << std::endl;
	std::cout << "  --nucleotide\t\tIndicate that genotypes are entered in nucleotide (A,C,T,G) form." << std::endl;
	std::cout << "  --haploid\t\tSets analysis for haploid genomes." << std::endl;
	std::cout << "  --ploidy=<INT>\tAllows to set a level of ploidy higher than 2 (experimental)." << std::endl;
	std::cout << "  --debug\t\tEnables debug mode." << std::endl;
	std::cout << "  --dec=<INT>\t\tSet floating point precission." << std::endl;
	std::cout << "  --seed=<INT>\t\tSet a seed for pseudorandom number generation." << std::endl;
	std::cout << "  --pairwise-stats\tPrints pairwise stats in matrix form." << std::endl;
	//std::cout << std::endl << "  Population options" << std::endl;
	std::cout << "  --rarefaction\t\tApplies a rarefaction method for allelic diversity (default)." << std::endl;
	std::cout << "  --no-rarefaction\tDisables rarefaction." << std::endl;
	std::cout << "  --rareN=<INT>\t\tSets the value for rarefaction sample size." << std::endl;
	std::cout << "  --nei-chesser\t\tCalculates F-stats using Nei & Chesser's correction." << std::endl;
	std::cout << "  --bootstrap\t\tComputes confidence intervals using a given number of bootstrap replicates." << std::endl;
	std::cout << "  --alpha\t\tWhen running bootstrap, it sets the level of significance for the confidence intervals." << std::endl;
	std::cout << std::endl << "  Synthetic pool" << std::endl;
	std::cout << "  --synth=<MODE>\tSets a method to obtain contributions to a pool (available modes are: 'none', 'all', 'Gt', 'At', 'He', 'NA')." << std::endl;
	std::cout << "  --synth-pool=<INT>\tSize of the synthetic pool." << std::endl;
	std::cout << "  --synth-min=<INT>\tMinimum contribution of each population." << std::endl;
	std::cout << std::endl << "  Management" << std::endl;
	std::cout << "  --manage=<MODE>\tSets a management strategy for the population (available modes are: 'none', 'genetic', 'allelic', 'Nalleles', 'random')." << std::endl;
	std::cout << "  --mng-file=FILE>\tName of the management file." << std::endl;
	std::cout << "  --method=<MODE>\tSets a method for the management of genetic diversity (available modes are: 'dynamic', 'ompg', 'isolated')." << std::endl;
	std::cout << "  --convergence=<INT>\tSets a minimum management steps (inside SA steps) required for convergence." << std::endl;
	std::cout << std::endl << "  Restrictions" << std::endl;
	std::cout << "  --max-nmigrants=<INT>\tMaximum number of migrants." << std::endl;
	std::cout << "  --lambda=<NUM>\tThe factor balancing the importante of within anf between diversity." << std::endl;
	std::cout << "  --max-delta-F=<NUM>\tMaximum per-generation rate of inbreeding." << std::endl;
	std::cout << "  --monogamy\t\tSets monogamy behaviour (default)." << std::endl;
	std::cout << "  --polygamy\t\tAllows poligamic behaviour." << std::endl;
	std::cout << "  --weight-F-isol=<NUM>\tWeighting factor of inbreeding in relation to coancestry." << std::endl;
	std::cout << "  --max-off-mate=<INT>\tMaximum number of offspring." << std::endl;
	std::cout << std::endl << "  Simmulated annealing" << std::endl;
	std::cout << "  --SA-steps=<INT>\tNumber of simmulated annealing steps." << std::endl;
	std::cout << "  --SA-temp=<NUM>\tInitial value of temperature in SA algorithm." << std::endl;
	std::cout << "  --SA-k=<NUM>\t\tTemperature change rate in SA algorithm." << std::endl;
	std::cout << std::endl << "  Simulation mode" << std::endl;
	std::cout << "  --simMode\t\tEnables simulation mode, saving all relevant output files." << std::endl;
	std::cout << "  --sim-t=<INT>\t\tSets number of generations." << std::endl;
	std::cout << "  --sim-r=<INT>\t\tSets number of replicates." << std::endl;
	std::cout << std::endl << "  Cross-software Compatibility" << std::endl;
	std::cout << "  --mtp2ped\t\tEnables conversion mode from metapop to plink PED format." << std::endl;
	std::cout << "  --mtp2gp\t\tEnables conversion mode from metapop to genepop format." << std::endl;
	std::cout << "  --ped2mtp\t\tEnables conversion mode from plink PED to metapop format." << std::endl;
	std::cout << "  --gp2mtp\t\tEnables conversion mode from genepop to metapop format." << std::endl;

	std::cout << std::endl;
	std::cout << "More help in the README file or in <https://gitlab.com/elcortegano/metapop2>" << std::endl;

	exit(0);
}

/* ********************************************************************************************
 * MTPConfigFile::config_error ()
 * --------------------------------------------------------------------------------------------
 * Returns configuration error messages
 * ********************************************************************************************/
void MTPConfigFile::config_error ( const std::string& message) {
	std::cerr << std::endl;
	print_stars(message.size() + 21);
	std::cerr << "CONFIGURATION ERROR: " << message << std::endl;
	std::cerr << ">>> Check configuration options." << std::endl;
	std::cerr << std::endl;
	std::cerr << "Remember that you can run Metapop in three different ways:" << std::endl;
	std::cerr << std::endl;
	std::cerr << "* Using a configuration file:" << std::endl << std::endl;
	std::cerr << "\tmetapop datafile configfile" << std::endl << std::endl;
	std::cerr << "* Using command line terminal options." << std::endl << std::endl;
	std::cerr << "\tmetapop [--config=<configfile>] [--options] datafile" << std::endl << std::endl;
	std::cerr << "* Using default configuration:" << std::endl << std::endl;
	std::cerr << "\tmetapop datafile" << std::endl << std::endl;
	print_stars(message.size() + 21);
	exit(-1);
}
