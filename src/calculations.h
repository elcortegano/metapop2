#ifndef CALCULATIONS_H
#define CALCULATIONS_H

#include "MTPConfigFile.h"
#include "metapop.h"
#include "utils.h"


// =====   EXTERN VARIABLES   =====
extern bool DEBUG;


// =====   STRUCTS   =====
struct parameter {
	double value; // the real value of the parameter
	double low_CI; // lower CI
	double high_CI; // higher CI
};

template <typename T>
struct G_parameters { // Recurrent genetic parameters
	T f; // coancestry
	T F; // inbreeding
	T s; // self-coancestry
	T d; // distance
};

template <typename T>
struct A_parameters { // Recurrent allelic parameters
	T as;
	T da;
};

template <typename T>
struct Fisher {
	T is;
	T st;
	T it;
};


// ====   CLASSES   ====
class Calculations {

protected:

	// General atributes
	std::size_t pop;
	std::size_t loci;
	bool use_molecular_markers;
	int boot;
	double alpha;
	unshort ploidy;

	// Genetic diversity parameters
	G_parameters <double> bar_n;
	G_parameters <double> tot_n; // before: ftot_n, Dtot_n
	G_parameters <vdou1D> barR_n; // （before fbarR_n, sbarR_n
	G_parameters <vdou1D> totR_n; // (before ftotR_n, DtotR_n)
	Fisher <double> F_n; // (before: Fis_n, Fst_n, Fit_n)
	Fisher <vdou2D> D; // before D (is), Ds (st), Dr(it) (check if are is, st, it)
	vdou1D F;
	vdou1D G;
	double Gbar_n;
	vdou1D DNbar;
	vdou1D Fis_pop;
	vdou2D Fst_b;

	// Allelic diversity parameters
	double AST;
	double Kmed;
	std::vector<int>  Nrare_loc;
	A_parameters <double> A; // before: DA, AS
	A_parameters <vdou1D> con_; // before: con_As, con_Da
	A_parameters <double> con_n; // average of con_ values
	A_parameters <vdou2D> pop_; // before Da_pop, Ast_pop
	A_parameters <vdou1D> rem_; // before AS_rem, DA_rem
	vdou1D Kpop;

public:

	// Constructor & update
	Calculations () {};
	Calculations ( MTPConfigFile& , const Metapop& );

	// Calculations
	void calc_F_n ( G_parameters <double> , G_parameters <double> , const std::vector<population>& , bool );
	vdou2D calc_F_loc ( vp_vdou1D , vdou2D polyploid_f);
	void calc_F ( const vdou2D& , const std::vector<population>& , const std::vector<std::size_t>& );
	void calc_F ( vp_double );
	void calc_G ( vp_vdou1D , vp_double );
	void calc_D ( vp_vdou1D , vp_vdou1D );
	void calc_Gbar_n ( const std::vector<population>& , int );
	void calc_DNbar ( const Fisher <vdou3D>& , const std::vector<population>& , const std::vector<std::size_t>& , int );
	void calc_barR_n ( vp_vdou1D , vp_double , const std::vector<population>& , const vdou1D& );
	void calc_totR_n ( vp_vdou1D , const std::vector<population>& , const vdou1D& );
	void calc_Fis_pop ( vp_vdou1D );
	Fisher <vdou3D> calc_D_loc ( vp_vdou2D , vp_vdou2D );
	void calc_bar_n ( vp_vdou1D , vp_double , vp_vdou1D , const std::vector<population>& , int );
	void calc_tot_n ( vp_vdou1D , const std::vector<population>& , int );
	G_parameters <double> calc_bar ( vp_vdou1D , vp_double , vp_vdou1D );
	G_parameters <double> calc_tot ( vp_vdou1D );

	G_parameters <vdou1D> calc_bar_loc ( vp_vdou2D , const vdou2D& , vp_vdou1D , vp_vdou2D );
	G_parameters <vdou1D> calc_tot_loc ( const Fisher <vdou3D>& , vp_vdou2D );
	vdou2D calc_G_loc ( vp_vdou2D , vp_vdou1D );
	vdou1D calc_Gbar_loc_n ( const vdou2D& , const std::vector<population>& , const std::vector<std::size_t>& );
	vdou1D calc_Ho ();
	vdou2D calc_Fis_pop_loc ( vp_vdou2D , const vdou2D& );
	void calc_Fst_b ( vp_vdou1D , const std::vector<population>& );
	Fisher <vdou1D> calc_F_loc_n ( vp_vdou2D , const vdou2D& , vp_vdou1D , vp_vdou2D , vdou3D , const std::vector<population>& , const std::vector<std::size_t>& );

	void calc_Nrare (const std::vector<population>& , int );
	vdou3D calc_distAl_loc ( vdou2D& , allele2D , const std::vector<population>& , vp_vdou2D , bool );
	void calc_A ( const vdou2D& , const vdou3D& );
	void calc_Kmed ( const allele2D& );
	void calc_AST ();
	void calc_con_A ( const vdou2D& , const vdou3D& );
	void calc_con_n ( const std::vector<population>& , int );
	void calc_pop_A ( const vdou3D& );
	void calc_rem_A ( const vdou3D& );

	// Get methods
	vdou1D get_F () const { return F; }
	vdou1D get_G () const { return G; }
	vdou1D get_Fis_pop () const { return Fis_pop; }
	vdou2D get_Fst_b () const { return Fst_b; }
	double get_Gbar_n () const { return Gbar_n; }
	vdou1D get_DNbar () const { return DNbar; }
	Fisher <vdou2D> get_D () const { return D; }
	G_parameters <double> get_bar_n () const { return bar_n; }
	G_parameters <double> get_tot_n () const { return tot_n; }
	Fisher <double> get_F_n () const { return F_n; }
	G_parameters <vdou1D> get_barR_n () const { return barR_n; }
	G_parameters <vdou1D> get_totR_n () const { return totR_n; }

	std::vector<int> get_Nrare_loc () const { return Nrare_loc; }
	double get_Kmed () const { return Kmed; }
	double get_Ast () const { return AST; }
	A_parameters <double> get_A () const { return A; }
	A_parameters <vdou1D> get_con_a () const { return con_; }
	A_parameters <double> get_con_n () const { return con_n; }
	A_parameters <vdou2D> get_pop_a () const { return pop_; }
	A_parameters <vdou1D> get_rem () const { return rem_; }
	vdou1D get_Kpop () const { return Kpop; }

protected:

	// Estimate genetic parameters
	void calculations ( const MTPConfigFile& , const Metapop& );

	// Estimate allelic parameters
	void calculationsAD ( MTPConfigFile& , const Metapop& );

	// Check parameters

};

#endif
