#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <iomanip>
#include <sstream>
#include <numeric>
#include <cmath>
#include <stdexcept>
#include <string.h>
#include "alias.h"
#include <deque>
#include <algorithm>
#include <regex>

// =====   FUNCTION'S PROTOTYPES   =====
std::vector<std::string> split_string ( std::string , char );
std::vector<std::string> split_vector ( std::string , char );
bool isInt(	const std::string& );
void print_warning ( std::string );
void print_error ( std::string );
void print_stars ( std::size_t );
void rm_charseq_in_str ( std::string& , std::string );
void rm_char_in_str ( std::string& , char );
void clean_datafile ( std::string& );
double Combinatorial ( int , int );
double LogFactorial ( int );


// =====   FUNCTION TEMPLATES  =====

/* ********************************************************************************************
 * is_in ()
 * --------------------------------------------------------------------------------------------
 * Returns true if find an element inside a vector container
 * ********************************************************************************************/
template<typename T>
inline bool is_in (std::vector<T> v, T i) {
	if (v.empty()) return false;
	return (std::find(v.begin(), v.end(), i) != v.end());
}

/* =======================================================================================
 * repetition ()
 * ---------------------------------------------------------------------------------------
 * Look for repeated elements in a vector
 ======================================================================================= */
template<typename T>
bool repetition (std::vector<T> vector1) {

	size_t N(vector1.size());
	for (size_t i(0); i<N; ++i) {
		for (size_t j(i+1); j<N; ++j) {
			if (vector1[i] == vector1[j]) {
				return true;
			}
		}
	}
	return false;
}

/* =======================================================================================
 * variation ()
 * ---------------------------------------------------------------------------------------
 * Checks if there are variation in a vector
 ======================================================================================= */
template<typename T>
bool variation (std::vector<T> vector1) {
	size_t N(vector1.size());
	T first (vector1[0]);
	for (size_t i(1); i<N; ++i) {
		if (vector1[i] != first) return true;
	}
	return false;
}

template<typename T>
bool variation (std::deque<T> vector1) {
	size_t N(vector1.size());
	T first (vector1[0]);
	for (size_t i(1); i<N; ++i) {
		if (vector1[i] != first) return true;
	}
	return false;
}

/* ********************************************************************************************
 * mean ()
 * --------------------------------------------------------------------------------------------
 * Calculates the mean of the elements in a vector
 * ********************************************************************************************/
template <typename T>
double mean (std::vector<T> sample) {
	double result ( std::accumulate(sample.begin(), sample.end(), 0.0) / sample.size());
	return result;
}

/* ********************************************************************************************
 * variance ()
 * --------------------------------------------------------------------------------------------
 * Calculates the variance of the elements in a vector
 * ********************************************************************************************/
template <typename T>
double var (std::vector<T> sample) {
	double result ( std::inner_product(sample.begin(), sample.end(), sample.begin(), 0.0) / (sample.size() - 1) );
	return result;
}

/* =======================================================================================
 * Function pmatrix () & pvector ()
 * ---------------------------------------------------------------------------------------
 * This function prints a given matrix or vector. Its is used for testing.
 ======================================================================================= */
template<typename T>
void pmatrix (const std::vector<std::vector<T>>& m) {
	for (auto const& v: m) {
	for (auto const& i: v) {
		std::cout <<  i << " " ;
	}   std::cout << std::endl; }
}

template<typename T>
void pvector (const std::vector<T>& v) {
	for (auto const& i: v) {
		std::cout << i << " " ;
	}	std::cout << std::endl;
}

// =====   OPERATOR OVERLOADING  ====

/* =======================================================================================
 * operator* (std::vector - std:: vector)
 * ---------------------------------------------------------------------------------------
 * Difference between two std::vector objects.
 ======================================================================================= */
template<typename T>
std::vector<double> operator- (const std::vector<T>& vector1, double value) {
	std::vector<double> difference;
	for (size_t i(0), N(vector1.size()); i<N; ++i) {
		difference.push_back(vector1[i]-value);
	}
	return difference;
}

#endif
