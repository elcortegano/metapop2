/* =======================================================================================
 *                                                                                       *
 *       Filename:  synthetic.cpp                                                        *
 *                                                                                       *
 *    Description: This file contaings functions to calculate population contributions   *
 * 				   to a synthetic pool                                                   *
 *                                                                                       *
 *                  -> rankPops ()                                                       *
 *                  -> rankEvaluation ()                                                 *
 *                                                                                       *
 ======================================================================================= */

#include "synthetic.h"


/* ********************************************************************************************
 * rankPops ()
 * --------------------------------------------------------------------------------------------
 * This is the main function to calculate population contributions to a pool. Its ranks each
 * population according to the defined criteria (genetic or allelic diversity, etc.) and saves
 * their contributions in the main output file.
 * ********************************************************************************************/
void rankPops (const MTPConfigFile& config, const Metapop& metapop, const Calculations& calc, std::string output ) {

	if (DEBUG) std::cout << "SYNTHETIC POPULATION ANALYSIS:" << std::endl;
	else std::cout << "Computing contributions to a synthetic pool...";
	std::cout << std::flush;

	/* LOCAL VARIABLES */
	std::vector <Synthetic> v_synth;
	if (config.get_synth() == ALL) {
		/*v_synth.push_back(GT);
		v_synth.push_back(GW);
		v_synth.push_back(GB);
		v_synth.push_back(AT);
		v_synth.push_back(AW);
		v_synth.push_back(AB);*/
		v_synth.push_back(HE);
		v_synth.push_back(NA);
	} else {
		v_synth.push_back(config.get_synth());
	}
	std::size_t nPool (config.get_nPool());
	double t (config.get_SA_t());
	double k (config.get_SA_k());
	std::size_t steps (config.get_SA_steps());
	std::size_t convergence (config.get_convergence());
	double minimum (config.get_minRank());
	double lambda (config.get_lambda_rank());
	std::size_t pop (metapop.get_n_subpops());
	std::vector<population> deme (metapop.get_deme());
	allele2D code (metapop.get_alleles_per_locus());
	vp_vdou2D ps (metapop.get_p().b_par);
	vp_vdou1D f (metapop.get_f().t_par);
	std::size_t max_rep (1000);
	std::size_t ivig (max_rep);
	vint2D v_solutionOpt;
	vdou1D eneopt;
	double min_energy (0.0);

	// Special case with only one subpopulation
	if (pop==1) {
		eneopt = vdou1D (v_synth.size(), 0.0);
		for (std::size_t i(0); i<v_synth.size(); ++i) v_solutionOpt.push_back(vint1D(1, nPool));
		save_synthetic (output.data(), config, deme, v_synth, nPool, eneopt, v_solutionOpt);
		return;
	}

	/* VARIABLES FROM CALCULATIONS */
	vdou2D D (calc.get_D().is);
	vdou1D con_As (calc.get_con_a().as);
	vdou2D Da_pop (calc.get_pop_a().da);

	vdou1D ht;
	for (std::size_t i (0); i < pop; i++) {
		ht.push_back((*f[i])[i]);
	}

	for (std::size_t s(0); s < v_synth.size(); ++s) {

		Synthetic synth (v_synth[s]);
		std::string n_synth ("number of alleles");
		min_energy = 0.0;
		if (synth == NA) {
			for (std::size_t i(0); i<code.size(); ++i) min_energy -= code[i].size();
		} else if (synth == HE) {
			n_synth = "heterocigosity";
			min_energy = -1.0;
		} else if (synth == GT || synth == GW || synth == GB) {
			n_synth = "genetic diversity";
			min_energy = -1.0;
		} else if (synth == AT || synth == AW || synth == AB) {
			n_synth = "allelic diversity";
			for (std::size_t i(0); i<code.size(); ++i) min_energy -= code[i].size();
		}
		if (synth == GT || synth == AT) {
			n_synth = "total " + n_synth;
		} else if (synth == GW || synth == AW) {
			n_synth += " between populations";
		} else if (synth == GB || synth == AB) {
			n_synth += " within populations";
		}
		vint1D solution;
		vint1D solutionOpt;

		// Random initial solution
		for (std::size_t i (0); i < pop; i++) {
			solution.push_back(0);
		}
		for (std::size_t i (0); i < nPool; i++) {
			solution[(int) (random_dist(random_generator) * pop)]++;
		}

		double eneac (0.0);
		if ((synth == GT) || (synth == GW) || (synth == GB)) eneac = rankEvaluation(synth, ht, D, solution, pop, nPool, lambda);
		else if ((synth == AT) || (synth == AW) || (synth == AB)) eneac = rankEvaluation(synth, con_As, Da_pop, solution, pop, nPool, lambda);
		else if (synth == NA) eneac =  rankNumberAlleles (code, ps, solution, pop);
		else if (synth == HE) eneac =  rankHeterocigosity (code, ps, solution, pop);
		if (DEBUG) std::cout << "\r  Ranking " << n_synth << ": " << -eneac;

		eneopt.push_back(eneac);
		solutionOpt = solution;

		if (minimum > (100.0 / pop)) {
			print_warning("Minimum constributions set for synthetic pool too high. Set to " + round_s(100.0/pop,2) + " by population");
			minimum = 100.0 / pop;
		} else if (minimum < 0.0) {
			print_warning("Minimum constributions set for synthetic pool cannot be negative. Set to 0");
			minimum = 0.0;
		}
		if (minimum > 0.0) print_warning("Minimum contributions set for synthetic pool.");
		minimum = (minimum * 0.01) * nPool;

		double kt (t / k);
		for (std::size_t niv (0); niv < steps; niv++) {

			std::size_t camb (1 + (4 * ivig / max_rep));
			kt = kt*k;
			ivig = 0;
			std::deque<double> last_energy;

			for (std::size_t rep (0); rep < max_rep; rep++) {

				vint1D solutionAlt (solution);

				for (std::size_t l (0); l < camb; l++) {
					std::size_t m (0);
					std::size_t n (0);
					std::size_t count (0);
					do {
						m = (int) (random_dist(random_generator) * pop);
						++count;
					} while (solutionAlt[m] <= minimum && count < max_rep);
					solutionAlt[m]--;
					do {
						n = (int) (random_dist(random_generator) * pop);
					} while (m == n);
					solutionAlt[n]++;
				}
				double eneal;
				if ((synth == GT) || (synth == GW) || (synth == GB)) eneal = rankEvaluation(synth, ht, D, solutionAlt, pop, nPool, lambda);
				else if ((synth == AT) || (synth == AW) || (synth == AB)) eneal = rankEvaluation(synth, con_As, Da_pop, solutionAlt, pop, nPool, lambda);
				else if (synth == NA) eneal = rankNumberAlleles (code, ps, solutionAlt, pop);
				else if (synth == HE) eneal = rankHeterocigosity (code, ps, solutionAlt, pop);
				if (DEBUG) std::cout << "\r  Ranking " << n_synth << ": " << -eneal;

				//Annealing
				std::size_t ch (0);
				double delta (0.0);
				if ((eneal - eneac) > 0) delta = eneal - eneac;
				double omega (exp(-delta / kt));
				if (omega >= 1.0) {
					ch = 1;
					if (eneal < eneopt[s]) {
						eneopt[s] = eneal;
						for (std::size_t i (0); i < pop; i++) {
							solutionOpt[i] = solutionAlt[i];
						}
					}
				} else if (random_dist(random_generator) < omega) ch = 1;

				if (ch == 1) {
					for (std::size_t i (0); i < pop; i++) {
						solution[i] = solutionAlt[i];
					}
					eneac = eneal;
					ivig++;
				}
				if (eneal <= min_energy) break;
				last_energy.push_back(eneopt[s]);
				if (last_energy.size() == convergence) {
					if (!variation(last_energy)) {
						eneopt[s] = last_energy[0];
						break;
					}
					last_energy.pop_front();
				}
			}
		}

		if ((synth == GT) || (synth == GW) || (synth == GB)) eneopt[s] = rankEvaluation(synth, ht, D, solutionOpt, pop, nPool, 1);
		else if ((synth == AT) || (synth == AW) || (synth == AB)) eneopt[s] = rankEvaluation(synth, con_As, Da_pop, solutionOpt, pop, nPool, 1);
		else if (synth == NA) eneopt[s] = rankNumberAlleles(code, ps, solutionOpt, pop);
		else if (synth == HE) eneopt[s] = rankHeterocigosity(code, ps, solutionOpt, pop);
		if (DEBUG) {
			std::cout << "\r  Ranking " << n_synth << ": " << -eneopt[s];
			std::cout << std::endl;
		}
		v_solutionOpt.push_back(solutionOpt);
	}

	if (DEBUG) {
		std::cout << "Done" << std::endl;
		std::cout << std::endl;
		std::cout << "---------------------------------------" << std::endl;
	} else std::cout << "\rComputing contributions to a synthetic pool...done!" << std::endl;

	/* Save subpopulations contributions to a pool */
	save_synthetic (output.data(), config, deme, v_synth, nPool, eneopt, v_solutionOpt);
}

/* ********************************************************************************************
 * rankEvaluation ()
 * --------------------------------------------------------------------------------------------
 * Estimate population contributions according to genetic or allelic diversity measures.
 * ********************************************************************************************/
double rankEvaluation (const Synthetic& synth, const vdou1D& ht, const vdou2D& D, const vint1D& solution, const std::size_t& pops, const std::size_t& npre, const double& lambda) {

	/* LOCAL VARIABLES */
	double ene (0.0);
	double fii (0.0);

	vdou1D c (pops, 0.0);

	for (std::size_t i (0); i < pops; i++) {
		c[i] = (double) solution[i] / (double) npre;
	}

	if ((synth == GT) || (synth == AT)) {
		double sum1 (0.0), sum2 (0.0);
		for (std::size_t i (0); i < pops; i++) {
			sum2 = 0.0;
			for (std::size_t j (0); j < pops; j++) {
				sum2 += D[i][j] * c[j];
			}
			if (synth == AT) fii = 1.0 - ht[i];
			else fii = ht[i];
			sum1 += c[i]*(fii * lambda - sum2);
		}
		ene = -(1.0 - sum1);
	}

	if ((synth == GB) || (synth == AB)) {
		double sum1 (0.0), sum2 (0.0);
		for (std::size_t i (0); i < pops; i++) {
			sum2 = 0.0;
			for (std::size_t j (0); j < pops; j++) {
				sum2 += D[i][j] * c[j];
			}
			sum1 += c[i] * sum2;
		}
		ene = -sum1;
	}

	if ((synth == GW) || (synth == AW)) {
		double sum1 (0.0);
		for (std::size_t i (0); i < pops; i++){
			if (synth == AW) fii = 1.0 - ht[i];
			else fii = ht[i];
			sum1 += c[i] * fii;
		}
		ene = -(1.0 - sum1);
	}
    return (ene);
}

/* ********************************************************************************************
 * rankNumberAlleles ()
 * --------------------------------------------------------------------------------------------
 * Estimate population contributions according to the number of alleles.
 * ********************************************************************************************/
double rankNumberAlleles (const allele2D& code, const vp_vdou2D& q, const vint1D& solution, const std::size_t& pops){

	double prob_all (0);
	for (std::size_t m (0); m < code.size(); m++) {
		for (std::size_t a (0); a < code[m].size(); a++) {
			double prob_loss (1.0);
			for (std::size_t k (0); k < pops; k++){
				prob_loss *= pow(1.0-(*q[k])[m][a], solution[k]);
			}
			prob_all += (1.0- prob_loss);
		}
	}
	return (-prob_all/code.size());
}

/* ********************************************************************************************
 * rankHeterocigosity ()
 * --------------------------------------------------------------------------------------------
 * Estimate population contributions according to their heterocigosity
 * ********************************************************************************************/
double rankHeterocigosity (const allele2D& code, const vp_vdou2D& q, const vint1D& solution, const std::size_t& pops) {

	std::size_t nloci (code.size());
	double het (0.0);

	for (std::size_t m (0); m < nloci; ++m) {
		double sqfreq(0.0);
		double den (0.0);
		std::size_t n_alleles (code[m].size());

		for (std::size_t a (0); a < n_alleles; ++a) {
			double num_a (0.0);
			for (std::size_t k (0); k < pops; k++){
				num_a += (*q[k])[m][a] * solution[k];
			}
			den += num_a;
			sqfreq += ((num_a) * (num_a));
		}
		sqfreq /= (den*den);
		het += (1.0-sqfreq);
	}

	return (-het/(double) nloci);
}
