#ifndef SYNTHETIC_H
#define SYNTHETIC_H

#include <random>
#include "utils.h"
#include "MTPConfigFile.h"
#include "metapop.h"
#include "calculations.h"
#include "output.h"


// =====   EXTERN VARIABLES   =====
extern std::mt19937 random_generator;
extern std::uniform_real_distribution<double> random_dist;
extern bool DEBUG;


// =====   FUNCTION'S PROTOTYPES   =====
void rankPops ( const MTPConfigFile& , const Metapop& , const Calculations& , std::string );
double rankEvaluation( const Synthetic& , const vdou1D& , const vdou2D& , const vint1D& , const std::size_t& , const std::size_t& , const double& );
double rankNumberAlleles ( const allele2D& , const vp_vdou2D&, const vint1D& , const std::size_t& );
double rankHeterocigosity ( const allele2D& , const vp_vdou2D&, const vint1D& , const std::size_t& );


#endif
