#ifndef OPERATORS_H
#define OPERATORS_H


// =====   HEADER FILE INCLUDES   =====

#include <vector>


// =====   OPERATOR OVERLOADING  ====

/* =======================================================================================
 * operator* (std::vector * std:: vector)
 * ---------------------------------------------------------------------------------------
 * Multiplication between two std::vector objects.
 ======================================================================================= */
template<typename T>
std::vector<T> operator* (const std::vector<T>& vector1, const std::vector<T>& vector2) {
	std::vector<T> product                                                                 ;
	for (size_t i(0), N(vector1.size()); i<N; ++i) product.push_back(vector1[i]*vector2[i]);
	return product                                                                         ;
}

/* =======================================================================================
 * operator*= (std::vector * T)
 * ---------------------------------------------------------------------------------------
 * Multiplication between std::vector objects and an individual value.
 ======================================================================================= */
template<typename T>
std::vector<T> operator*= (std::vector<T>& vector, T value) {
	for (auto& i : vector) {
		i *= value;
	}
	return vector;
}

/* =======================================================================================
 * operator/ (std::vector<std::vector> / T)
 * ---------------------------------------------------------------------------------------
 * Divission between std::vector objects and an individual value.
 ======================================================================================= */
template<typename T>
std::vector<std::vector<T>> operator/ (std::vector<std::vector<T>>& vector, T value) {
	for (auto& i : vector) {
		for (auto& j: i) {
			j /= value;
		}
	}
	return vector;
}

/* =======================================================================================
 * operator/= (std::vector * T)
 * ---------------------------------------------------------------------------------------
 * Divission between std::vector objects and an individual value.
 ======================================================================================= */
template<typename T>
std::vector<T> operator/= (std::vector<T>& vector, T value) {
	for (auto& i : vector) {
		i /= value;
	}
	return vector;
}

/* =======================================================================================
 * operator* (std::vector - std:: vector)
 * ---------------------------------------------------------------------------------------
 * Difference between two std::vector objects.
 ======================================================================================= */
template<typename T>
std::vector<T> operator- (const std::vector<T>& vector1, const std::vector<T>& vector2) {
	std::vector<T> difference;
	for (size_t i(0), N(vector1.size()); i<N; ++i) {
		difference.push_back(vector1[i]-vector2[i]);
	}
	return difference;
}

/* =======================================================================================
 * operator+ (std::vector - std:: vector)
 * ---------------------------------------------------------------------------------------
 * Addition between two std::vector objects.
 ======================================================================================= */
template<typename T>
std::vector<T> operator+= (std::vector<T>& vector1, const std::vector<T>& vector2) {
	for (size_t i(0), N(vector1.size()); i<N; ++i) {
		vector1[i] += vector2[i];
	}
	return vector1;
}

/* =======================================================================================
 * operator- (std::vector - T)
 * ---------------------------------------------------------------------------------------
 * Diference between std::vector objects and an individual value.
 ======================================================================================= */
template<typename T>
std::vector<T> operator- (const std::vector<T>& vector1, T value) {
	std::vector<T> difference                                                            ;
	for (size_t i(0), N(vector1.size()); i<N; ++i) difference.push_back(vector1[i]-value);
	return difference                                                                    ;
}

// ---------------------------------------------------------------------------------------
template<typename T>
std::vector<T> operator-= (std::vector<T>& vector,T value) {
	for (auto& i : vector) {
		i -= value;
	}
	return vector;
}

#endif
