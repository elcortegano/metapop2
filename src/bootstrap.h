#ifndef BOOTSTRAP_H
#define BOOTSTRAP_H

#include "calculations.h"


// =====   STRUCTS   =====
template <typename T>
struct CI { // Confidence intervals
	T low;
	T high;
	CI& operator -(CI other) {
		T low = this.low - other.low;
		T high = this.high - other.high;
	};
	CI& operator -(double n) {
		T tmp = this.low;
		T low = n - this.high;
		T high = n - tmp;
	}
};

// =====   CLASSES   =====
class Calculations_boot : public Calculations {

protected:

	std::vector<Calculations> calculations_boot;
	std::size_t n_subpops;	// the number of supopulations

	vdou3D tmp_fextra;
	vdou2D tmp_s;
	vdou3D tmp_d;
	vint2D tmp_u_alleles;

	std::vector<std::vector<CI<double>>> fextra;
	std::vector<std::vector<CI<double>>> d;
	std::vector<CI<double>> s;
	CI<double> avg_fextra_ii;
	CI<double> avg_d_ii;
	CI<double> avg_s;

	std::vector<CI<double>> F;
	std::vector<CI<double>> G;
	std::vector<CI<double>> Fis_pop;
	CI<double> avg_F;
	CI<double> avg_G;
	CI<double> avg_Fis_pop;

	std::vector<std::vector<CI<double>>> D_is;
	std::vector<std::vector<CI<double>>> Fst_b;

	CI<double> HS;
	CI<double> DG;
	CI<double> HT;

	CI<double> GDwi;
	CI<double> GDbi;
	CI<double> GDws;
	CI<double> GDbs;

	CI<double> Fis;
	CI<double> Fst;
	CI<double> Fit;

	std::vector<CI<double>> con_as;
	std::vector<CI<double>> app;
	std::vector<CI<double>> u_alleles;
	CI<double> avg_con_as;
	CI<double> avg_app;
	CI<double> avg_u_alleles;

	std::vector<std::vector<CI<double>>> pop_as;
	std::vector<std::vector<CI<double>>> pop_da;

	CI<double> AS;
	CI<double> DA;
	CI<double> AT;

	CI<double> AST;

	std::vector<CI<double>> rem_HS;
	std::vector<CI<double>> rem_DG;
	std::vector<CI<double>> rem_HT;
	std::vector<CI<double>> rem_AS;
	std::vector<CI<double>> rem_DA;
	std::vector<CI<double>> rem_AT;
	
public:

	Calculations_boot ( MTPConfigFile& , const Metapop& );
	void set_ci ( double );

	CI<double> get_fextra (int i, int j) const { return fextra[i][j]; };
	CI<double> get_s (int i) const { return s[i]; };
	CI<double> get_d (int i, int j) const { return d[i][j]; };
	CI<double> get_F (int i) const { return F[i]; };
	CI<double> get_G (int i) const { return G[i]; };
	CI<double> get_Fis_pop (int i) const { return Fis_pop[i]; };
	CI<double> get_D_is (int i, int j) const { return D_is[i][j]; };
	CI<double> get_Fst_b (int i, int j) const { return Fst_b[i][j]; };
	CI<double> get_HS () const { return HS; };
	CI<double> get_DG () const { return DG; };
	CI<double> get_HT () const { return HT; };
	CI<double> get_GDwi () const { return GDwi; };
	CI<double> get_GDbi () const { return GDbi; };
	CI<double> get_GDws () const { return GDws; };
	CI<double> get_GDbs () const { return GDbs; };
	CI<double> get_Fis () const { return Fis; };
	CI<double> get_Fst () const { return Fst; };
	CI<double> get_Fit () const { return Fit; };

	CI<double> get_con_as (int i) const { return con_as[i]; };
	CI<double> get_app (int i) const { return app[i]; };
	CI<double> get_u_alleles (int i) const { return u_alleles[i]; };
	CI<double> get_pop_as (int i, int j) const { return pop_as[i][j]; };
	CI<double> get_pop_da (int i, int j) const { return pop_da[i][j]; };
	CI<double> get_AS () const { return AS; };
	CI<double> get_DA () const { return DA; };
	CI<double> get_AT () const { return AT; };
	CI<double> get_AST () const { return AST; };

	CI<double> get_avg_fextra () const { return avg_fextra_ii; };
	CI<double> get_avg_s () const { return avg_s; };
	CI<double> get_avg_d () const { return avg_d_ii; };
	CI<double> get_avg_F () const { return avg_F; };
	CI<double> get_avg_G () const { return avg_G; };
	CI<double> get_avg_Fis_pop () const { return avg_Fis_pop; };
	CI<double> get_avg_con_as () const { return avg_con_as; };
	CI<double> get_avg_app () const { return avg_app; };
	CI<double> get_avg_u_alleles () const { return avg_u_alleles; };

	CI<double> get_rem_HS (int i) const { return rem_HS[i]; };
	CI<double> get_rem_DG (int i) const { return rem_DG[i]; };
	CI<double> get_rem_HT (int i) const { return rem_HT[i]; };
	CI<double> get_rem_AS (int i) const { return rem_AS[i]; };
	CI<double> get_rem_DA (int i) const { return rem_DA[i]; };
	CI<double> get_rem_AT (int i) const { return rem_AT[i]; };
};

struct sort_vdou1D {
	inline bool operator() (const double& v1, const double& v2) {
		return (v1 < v2);
	}
};

#endif
