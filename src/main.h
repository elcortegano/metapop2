#ifndef MAIN_H
#define	MAIN_H

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstring>
#include <cstdio>
#include <numeric>
#include <functional>
#include <vector>
#include <cmath>
#include <sstream>
#include <stdexcept>
#include <random>
#include <time.h>
#include <exception>
#include <algorithm>
#include "utils.h"
#include "metapop.h"
#include "population.h"
#include "individual.h"
#include "locus.h"
#include "MTPConfigFile.h"
#include "calculations.h"
#include "bootstrap.h"
#include "output.h"
#include "synthetic.h"
#include "management.h"
#include "convert.h"
using namespace std;


/* GLOBAL VARIABLES */
const std::string version = "v2.5.3";
std::mt19937 random_generator; // Mersenne Twister pseudorandom number generator engine
std::uniform_real_distribution<double> random_dist(0.0, 1.0);
bool DEBUG (false);


//Bootstrapping
bool bootstrappingState = false;
void bootstrap ( const MTPConfigFile& , const Metapop& , int , double );
void bootstrapSampling();

#endif
