#ifndef OUTPUT_H
#define OUTPUT_H

#include <iomanip>
#include <numeric>
#include <random>
#include "operators.h"
#include "utils.h"
#include "MTPConfigFile.h"
#include "utils.h"
#include "calculations.h"
#include "bootstrap.h"
#include "metapop.h"


// =====   EXTERN VARIABLES   =====
extern const std::string version;
extern std::mt19937 random_generator;
extern std::uniform_real_distribution<double> random_dist;


// =====   CONSTANT VARIABLES   =====
const std::string H1 ("# ");
const std::string H2 ("## ");
const std::string H3 ("### ");
const std::string md_endl ("  ");
const std::string item_list ("* ");
const std::string row_sep ("***************************************");
const std::string delim ("\t");


// ====   INLINE FUNCTIONS   ====

/* =======================================================================================
 * print_header: prints a header line in markdown
 ======================================================================================= */
inline void print_header (std::ofstream& str, std::string text, const std::string type) {
	str << std::endl << std::endl << type << text << md_endl << std::endl;
}


/* =======================================================================================
 * Function round_s ()
 * ---------------------------------------------------------------------------------------
 * This function takes a number, rounds it, and returns in string form
 * =====================================================================================*/
inline std::string round_s (double number, std::size_t acc) {
	number = roundf (number * (pow(10,acc)))/pow(10,acc);
	if (number == -0.0) number = 0.0;
	std::string s_number (std::to_string(number));
	s_number.erase ( s_number.find_last_not_of('0') + 1, std::string::npos );
	if (s_number[s_number.size()-1] == '.') s_number.pop_back();
	return s_number;
}

/* =======================================================================================
 * print_dataline: prints a line of data with precission
 ======================================================================================= */
inline void print_dataline (std::ofstream& str, std::string text, double value, std::size_t acc) {
	str << text << " = " << round_s(value,acc) << md_endl << std::endl;
}

/* =======================================================================================
 * print_bootline: prints a line of data with precission and bootstrap CI
 ======================================================================================= */
template <typename T>
inline void print_bootline (std::ofstream& str, std::string text, double value, std::size_t acc, CI<T> ci) {
	str << text << " = " << round_s(value,acc) << " (" << round_s(ci.low,acc) << " - " << round_s(ci.high,acc) << ")" << md_endl << std::endl;
}

/* =======================================================================================
 * print_textline: prints a line of text
 ======================================================================================= */
inline void print_textline (std::ofstream& str, std::string text) {
	str << text << md_endl << std::endl;
}

/* =======================================================================================
 * b: bold text
 ======================================================================================= */
inline std::string bold (std::string text) {
	return ("**" + text + "**");
}

/* =======================================================================================
 * append: append a char a given number of times to the stream
 ======================================================================================= */
inline void append (std::ofstream& str, const char symbol, std::size_t ntimes) {
	for (size_t i(0); i<ntimes; ++i) {
		str << symbol;
	}
}

/* =======================================================================================
 * ndigits: determines the number of digits in a number (including - sign)
 ======================================================================================= */
template<typename T>
inline std::size_t ndigits (T number) {
	std::size_t digits (0);
	if (number < 0) {
		number = -number;
		++digits;
	}
	while (number) {
		number /= 10;
		++digits;
	}
	
	return digits;
}

/* =======================================================================================
 * print_table: generates a markdown table
 ======================================================================================= */
inline void print_table (std::ofstream& str, std::string header, std::vector<std::string> rows) {

	// Split header and rows
	str << std::endl;
	std::vector<std::string> v_header (split_vector(header,'|'));
	std::vector<std::vector<std::string>> m_rows;
	for (size_t i(0); i<rows.size(); ++i) {
		m_rows.push_back(split_vector(rows[i],'|'));
	}

	// Check that rows size is smaller than header size
	for (std::size_t i(0); i<m_rows.size(); ++i) {
		if (m_rows[i].size() > v_header.size()) {
			std::cerr << "ERROR: more fields to print for row cells than in header" << std::endl;
			exit(-1);
		}
	}

	// Generate an index of the maximum size by cell
	std::vector<std::size_t> cell_sizes;
	for (std::size_t i(0); i<v_header.size(); ++i) {
		cell_sizes.push_back(v_header[i].size());
	}

	for (std::size_t i(0); i<m_rows.size(); ++i) {
		for (std::size_t j(0); j<m_rows[i].size(); ++j) {
			if (m_rows[i][j].size() > cell_sizes[j]) {
				cell_sizes[j] = m_rows[i][j].size();
			}
		}
	}

	// Print header
	for (std::size_t i(0); i<cell_sizes.size(); ++i) {
		int diff (v_header[i].size() - cell_sizes[i]);
		if (diff == 0)  {
			str << v_header[i];
		} else if (diff < 0) {
			if (-diff%2 == 0) {
				append (str, ' ', -diff/2); str << v_header[i]; append(str, ' ', -diff/2);
			} else {
				append (str, ' ', -diff/2); str << v_header[i]; append(str, ' ', (-diff/2)+1);
			}
		} else {
			std::cerr << "ERROR: Wrong cell size" << std::endl;
			exit(-1);
		}
		if (i<cell_sizes.size()-1) {
			str << '|';
		}
	} str << std::endl;

	// Print dash line
	for (std::size_t i(0); i<cell_sizes.size(); ++i) {
		append (str,'-',cell_sizes[i]);
		if (i<cell_sizes.size()-1) {
			str << '|';
		}
	} str << std::endl;

	// Print rows
	for (std::size_t i(0); i<m_rows.size(); ++i) {
		for (std::size_t j(0); j<m_rows[i].size(); ++j) {
			int diff (m_rows[i][j].size() - cell_sizes[j]);
			if (diff == 0) {
				str << m_rows[i][j];
			} else if (diff < 0) {
				if (-diff%2 == 0) {
					append (str, ' ', -diff/2); str << m_rows[i][j]; append(str, ' ', -diff/2);
				} else {
					append (str, ' ', -diff/2); str << m_rows[i][j]; append(str, ' ', (-diff/2)+1);
				}
			} else {
				std::cerr << "ERROR: Wrong cell size" << std::endl;
				exit(-1);
			}
			if (j<cell_sizes.size()-1) {
				str << '|';
			}
		}
		str << std::endl;
	}
}

/* =======================================================================================
 * print_pairwise_table: generates a table of pairwise stats, in a markdown format
 ======================================================================================= */
inline void print_pairwise_table (std::ofstream& str, std::string title, vdou2D data_matrix, std::vector<population> deme, std::size_t acc) {

	// Create pairwise matrix (it must be squared)
	std::size_t dim (data_matrix.size());
	std::string header (" Subpop. ");
	std::vector <std::string> rows;
	for (std::size_t i(0); i < dim; ++i) {
		header += " | " + deme[i].getLabel();
		std::string row ( " " + deme[i].getLabel() + " ");
		for (std::size_t j(0); j<dim; ++j) {
			std::size_t k(i), l(j);
			if (k > l) { k=j; l=i; }
			row += " | " + round_s(data_matrix[k][l], acc);
		}
		rows.push_back(row);
	}

	// Title
	str << std::endl << title << std::endl << std::endl;

	// Split header and rows
	std::vector<std::string> v_header (split_vector(header,'|'));
	std::vector<std::vector<std::string>> m_rows;
	for (size_t i(0); i<rows.size(); ++i) {
		m_rows.push_back(split_vector(rows[i],'|'));
	}

	// Check that rows size is smaller than header size
	for (std::size_t i(0); i<m_rows.size(); ++i) {
		if (m_rows[i].size() > v_header.size()) {
			std::cerr << "ERROR: more fields to print for row cells than in header" << std::endl;
			exit(-1);
		}
	}

	// Generate an index of the maximum size by cell
	std::vector<std::size_t> cell_sizes;
	for (std::size_t i(0); i<v_header.size(); ++i) {
		cell_sizes.push_back(v_header[i].size());
	}

	for (std::size_t i(0); i<m_rows.size(); ++i) {
		for (std::size_t j(0); j<m_rows[i].size(); ++j) {
			if (m_rows[i][j].size() > cell_sizes[j]) {
				cell_sizes[j] = m_rows[i][j].size();
			}
		}
	}

	// Print header
	for (std::size_t i(0); i<cell_sizes.size(); ++i) {
		int diff (v_header[i].size() - cell_sizes[i]);
		if (diff == 0)  {
			str << v_header[i];
		} else if (diff < 0) {
			if (-diff%2 == 0) {
				append (str, ' ', -diff/2); str << v_header[i]; append(str, ' ', -diff/2);
			} else {
				append (str, ' ', -diff/2); str << v_header[i]; append(str, ' ', (-diff/2)+1);
			}
		} else {
			std::cerr << "ERROR: Wrong cell size" << std::endl;
			exit(-1);
		}
		if (i<cell_sizes.size()-1) {
			str << '|';
		}
	} str << std::endl;

	// Print dash line
	for (std::size_t i(0); i<cell_sizes.size(); ++i) {
		append (str,'-',cell_sizes[i]);
		if (i<cell_sizes.size()-1) {
			str << '|';
		}
	} str << std::endl;

	// Print rows
	for (std::size_t i(0); i<m_rows.size(); ++i) {
		for (std::size_t j(0); j<m_rows[i].size(); ++j) {
			int diff (m_rows[i][j].size() - cell_sizes[j]);
			if (diff == 0) {
				str << m_rows[i][j];
			} else if (diff < 0) {
				if (-diff%2 == 0) {
					append (str, ' ', -diff/2); str << m_rows[i][j]; append(str, ' ', -diff/2);
				} else {
					append (str, ' ', -diff/2); str << m_rows[i][j]; append(str, ' ', (-diff/2)+1);
				}
			} else {
				std::cerr << "ERROR: Wrong cell size" << std::endl;
				exit(-1);
			}
			if (j<cell_sizes.size()-1) {
				str << '|';
			}
		}
		str << std::endl;
	}
}

/* =======================================================================================
 * print_csv generates a csv table
 ======================================================================================= */
inline void print_csv (std::ofstream& str, std::string header, std::vector<std::string> rows) {
	if (header.size()) str << header << std::endl;
	for (auto& i: rows) str << i << std::endl;
}

/* =======================================================================================
 * Gets an allele number and returns the corresponding nucleotide (see Metapop::check_allele)
 ======================================================================================= */
inline std::string print_nucleotide (const allele_t& allele) {
	if (allele==1) return "A";
	else if (allele==2) return "C";
	else if (allele==3) return "T";
	else if (allele==4) return "G";
	else return "";
}

// =====   FUNCTION'S PROTOTYPES   =====
void save_header ( const MTPConfigFile& , std::string , std::string , time_t );
void save_freq ( const MTPConfigFile& , const Metapop& , std::string , std::size_t , std::size_t );
void save_pop_analysis ( const MTPConfigFile& , const Metapop& , std::string , const Calculations& , const Calculations_boot& );
void save_synthetic ( std::string , const MTPConfigFile& config , const std::vector<population>& , std::vector<Synthetic> , std::size_t , vdou1D , const vint2D& );
void printSolution ( const MTPConfigFile& , Metapop& , const Calculations& , std::string , vint3D& , const vdou2D& , double, vint2D& , int , int , double );
void save_tmatrix ( std::ofstream& , const vdou2D& );
std::string generaOff ( std::size_t , std::string , std::string );


#endif
