/* =======================================================================================
 *                                                                                       *
 *       Filename: calculations.cpp                                                      *
 *                                                                                       *
 *    Description: This file contaings functions to read and check input files.          *
 * 			       It also contains methods for using class MTPInput, which represents   *
 *                 the input datafile.                                                   *
 *                                                                                       *
 *                  -> calculations ()                                                   *
 *                  -> calculationsPetit ()                                              *
 *                  -> calculationsAD ()                                                 *
 *                  -> calc_* ()                                                         *
 *                  -> get_* ()                                                          *
 *                                                                                       *
 ======================================================================================= */

#include "calculations.h"


/* ********************************************************************************************
 * Calculations::Calculations ()
 * --------------------------------------------------------------------------------------------
 * Class Calculations constructor. Creates a deme from the input files and calculates genetic
 * parameters in the metapopulation
 * ********************************************************************************************/
Calculations::Calculations ( MTPConfigFile& config, const Metapop& metapop) {

	if (DEBUG) std::cout << "POPULATION ANALYSIS:" << std::endl;
	use_molecular_markers = metapop.get_use_molecular_markers();
	calculations (config, metapop);
	if (use_molecular_markers) {
		calculationsAD (config, metapop);
	}
	if (DEBUG) std::cout << "---------------------------------------" << std::endl;
}

/* ********************************************************************************************
 * Calculations ()
 * --------------------------------------------------------------------------------------------
 * Calculates many parameters related to gene diversity
 * ********************************************************************************************/
void Calculations::calculations (const MTPConfigFile& config, const Metapop& metapop) {

	std::size_t n_calc (12);
	if (DEBUG) {
		std::cout << "Measures of genetic diversity" << std::endl;
		std::cout << "\r  Progress: " << 0 << " / " << n_calc;
	}

	/* LOCAL VARIABLES */
	bool NCcorr (config.get_NCcorr());
	boot = config.get_bt_n();
	alpha = config.get_bt_alpha();
	ploidy = config.get_ploidy();
	loci = metapop.get_n_loci();
	pop = metapop.get_n_subpops();
	int totalN (metapop.get_N(false));
	std::vector<population> deme (metapop.get_deme());
	std::vector<std::size_t> sum_L (metapop.informative_inds());
	std::vector<std::size_t> sum_P (metapop.informative_loci());
	vp_vdou2D f_loc (metapop.get_f().b_par);
	vp_vdou1D fextra (metapop.get_f().t_par);
	vp_vdou1D s_loc (metapop.get_s().b_par);
	vp_double s (metapop.get_s().t_par);
	vp_vdou2D d_loc (metapop.get_d().b_par);
	vp_vdou1D d (metapop.get_d().t_par);
	vdou2D polyploid_f (metapop.get_polyploid_f());

	/* Calculate F_loc */
	vdou2D F_loc;
	if (use_molecular_markers) {
		F_loc = calc_F_loc (s_loc, polyploid_f);

		// Calculate F
		calc_F ( F_loc, deme, sum_P);
	} else {
		calc_F (s);
	}
	if (DEBUG) std::cout << "\r  Progress: " << 1 << " / " << n_calc;

	/* Calculate bar_loc */ // (NOT IN USE)
	G_parameters <vdou1D> bar_loc;
	if (false) bar_loc = calc_bar_loc (f_loc, F_loc, s_loc, d_loc);

	/* Calculate bar */
	G_parameters <double> bar;
	bar = calc_bar (fextra, s, d);
	if (DEBUG) std::cout << "\r  Progress: " << 2 << " / " << n_calc;

	/* Calculate bar_n */
	calc_bar_n (fextra, s, d, deme, totalN);
	if (DEBUG) std::cout << "\r  Progress: " << 3 << " / " << n_calc;

	/* Calculate G_loc */ // (NOT IN USE)
	vdou2D G_loc;
	if (false) G_loc = calc_G_loc (f_loc, s_loc);

	/* Calculate G */
	calc_G (fextra, s);
	if (DEBUG) std::cout << "\r  Progress: " << 4 << " / " << n_calc;

	/* Calculate Gbar_loc_n */ // (NOT IN USE)
	vdou1D Gbar_loc_n;
	if (false) Gbar_loc_n = calc_Gbar_loc_n (G_loc, deme, sum_L);

	/* Calculate Gbar_n */
	calc_Gbar_n (deme, totalN);
	if (DEBUG) std::cout << "\r  Progress: " << 5 << " / " << n_calc;

	/* Calculate D_loc; before D_loc, Ds_loc, Dr_loc */
	Fisher <vdou3D> D_loc;
	if (use_molecular_markers) {
		D_loc = calc_D_loc (f_loc, d_loc);
	}

	/* Calculate tot_loc */ // (NOT IN USE)
	G_parameters <vdou1D> tot_loc;
	if (false) tot_loc = calc_tot_loc ( D_loc, f_loc);

	/* Calculate D */
	calc_D (fextra, d);
	if (DEBUG) std::cout << "\r  Progress: " << 6 << " / " << n_calc;

	/* Calculate tot */
	G_parameters <double> tot;
	tot = calc_tot (fextra);
	if (DEBUG) std::cout << "\r  Progress: " << 7 << " / " << n_calc;

	/* Calculate tot_n */
	calc_tot_n (fextra, deme, totalN);
	if (DEBUG) std::cout << "\r  Progress: " << 8 << " / " << n_calc;

	/* CALCULATE DNbar */
	calc_DNbar (D_loc , deme, sum_L, totalN);
	if (DEBUG) std::cout << "\r  Progress: " << 9 << " / " << n_calc;

	/* Calculate the observed hterozygosity */ // (NOT IN USE)
	vdou1D Ho;
	if (false) Ho = calc_Ho();

	/* Calculate Fis */ // (NOT IN USE)
	vdou2D Fis_pop_loc;
	if (false) Fis_pop_loc = calc_Fis_pop_loc (f_loc, F_loc);

	/* Calculate Fis per population and locus */
	calc_Fis_pop (fextra);
	if (DEBUG) std::cout << "\r  Progress: " << 10 << " / " << n_calc;

	/*  Calculate Fst_pop_n (Fst between pairs of populations) */
	calc_Fst_b (fextra, deme);

	/* Estimate F_loc_n */ // (NOT IN USE)
	Fisher <vdou1D> F_loc_n;
	if (false) F_loc_n = calc_F_loc_n (f_loc, F_loc, s_loc, d_loc, D_loc.is, deme, sum_L);

	/* NEI & CHESSER CORRECTION */
	calc_F_n (bar, tot, deme, NCcorr);
	if (DEBUG) std::cout << "\r  Progress: " << 11 << " / " << n_calc;

	/* REMOVING */
	vdou1D sum_NR;
	for (std::size_t o (0); o < pop; o++) {
		double sum (0.0);
		for (std::size_t i (0); i < pop; i++) {
			if (i != o) sum += (double) deme[i].getEffectiveSize();
		}
		sum_NR.push_back(sum);
	}

	calc_barR_n (fextra, s, deme, sum_NR);
	calc_totR_n (fextra, deme, sum_NR);
	if (DEBUG) {
		std::cout << "\r  Progress: " << 12 << " / " << n_calc;
		std::cout << std::endl;
		std::cout << "Done" << std::endl;
		std::cout << std::endl;
	}

}

/* ********************************************************************************************
 * calculationsAD ()
 * --------------------------------------------------------------------------------------------
 * Calculates many parameters related to allelic diversity
 * ********************************************************************************************/
void Calculations::calculationsAD (MTPConfigFile& config, const Metapop& metapop) {

	std::size_t n_calc (6);
	if (DEBUG) {
		std::cout << "Measures of allelic diversity" << std::endl;
		std::cout << "\r  Progress: " << 0 << " / " << n_calc;
	}

	/* LOCAL VARIABLES */
	static std::size_t count (0);
	bool ADrare (config.get_ADrare());
	std::size_t ADrareN (config.get_ADrareN());
	pop = metapop.get_n_subpops();
	loci = metapop.get_n_loci();
	int totalN (metapop.get_N(false));
	std::vector<population> deme (metapop.get_deme());
	allele2D code (metapop.get_alleles_per_locus());
	vp_vdou2D ps (metapop.get_p().b_par);

	/* Check rarefaction values */
	if (!count) {
		if (ADrareN && !ADrare) {
			print_warning("Rarefaction sample size set, but rarefaction not enabled. It will be ignored.");
			ADrare = false; ADrareN = 0;
			config.disable_rarefaction();
		}
		if (ADrare && (ADrareN > metapop.get_maxN())) print_error ("Cannot use a rarefaction number higher than the largest population size.");
		if (ADrare && (metapop.get_sameN() && !config.get_simMode())) {
			print_warning("All subpopulations have the same size. Rarefaction will be ignored.");
			ADrare = false; ADrare = 0;
			config.disable_rarefaction();
		}
	}

	/* Calculate Rarefaction sample size */
	if (ADrare) calc_Nrare (deme, ADrareN);
	if (DEBUG) std::cout << "\r  Progress: " << 1 << " / " << n_calc;

	/* Calculate allelic distance between populations and allelic richness */
	vdou3D distAl_loc;
	vdou2D alRich;
	distAl_loc = calc_distAl_loc (alRich, code, deme, ps, ADrare);
	if (DEBUG) std::cout << "\r  Progress: " << 2 << " / " << n_calc;

	/* Within and between allelic diversity per population */
	calc_con_A (alRich, distAl_loc);
	calc_con_n (deme, totalN);
	if (DEBUG) std::cout << "\r  Progress: " << 3 << " / " << n_calc;

	// Average within and between allelic diversity and AST
	calc_A ( alRich, distAl_loc);
	calc_Kmed (code);
	calc_AST ();
	if (DEBUG) std::cout << "\r  Progress: " << 4 << " / " << n_calc;

	// Ast and Da/K between pairs of populations
	calc_pop_A (distAl_loc);
	if (DEBUG) std::cout << "\r  Progress: " << 5 << " / " << n_calc;

	/* REMOVING AD */
	calc_rem_A (distAl_loc);

	if (DEBUG) {
		std::cout << "\r  Progress: " << 6 << " / " << n_calc;
		std::cout << std::endl;
		std::cout << "Done" << std::endl;
		std::cout << std::endl;
	}
	++count;
}

/* ********************************************************************************************
 * Calculations::calc_F_N
 * --------------------------------------------------------------------------------------------
 * Calculates F_n
 * ********************************************************************************************/
void Calculations::calc_F_n (G_parameters <double> bar, G_parameters <double> tot, const std::vector<population>& deme, bool NCcorr) {

	// Calculate heterozygosity
	double inv_N (0.0);
	double h_mean;
	for (std::size_t i (0); i < pop; i++) {
		if (deme[i].getEffectiveSize() != 0) {
			inv_N += (1.0 / (double) deme[i].getEffectiveSize());
		}
	}
	h_mean = pop / inv_N;

	// Calculate F statistics (using NC correction or not)
	if (NCcorr) {
		double HoBar (1.0 - bar.F);
		double HsBar_C ((h_mean / (h_mean-1.0)) * (1.0 - bar.f - (HoBar/(2.0*h_mean))));
		double Ht_C (1.0 - tot.f + (HsBar_C/(h_mean*pop)) - (HoBar/(2*h_mean*pop)));

		if(HsBar_C!=0.0) F_n.is = 1.0 - (HoBar/HsBar_C);
		if(Ht_C!=0.0) F_n.it = 1.0 - (HoBar/Ht_C);
		if(Ht_C!=0.0) F_n.st = 1.0 - (HsBar_C/Ht_C);

	} else {
		if (bar_n.f != 1.0) F_n.is = (bar_n.F - bar_n.f) / (1.0 - bar_n.f);
		if (tot_n.f != 1.0) F_n.st = (bar_n.f - tot_n.f) / (1.0 - tot_n.f);
		if (tot_n.f != 1.0) F_n.it = (bar_n.F - tot_n.f) / (1.0 - tot_n.f);
	}
}

/* ********************************************************************************************
 * Calculations::calc_F_loc
 * --------------------------------------------------------------------------------------------
 * Calculates the inbreeding coefficient at a given locus, as in Caballero & Toro 2002.
 * ********************************************************************************************/
vdou2D Calculations::calc_F_loc (vp_vdou1D s_loc, vdou2D polyploid_f) {
	vdou2D F_loc;
	F_loc.assign(loci,vdou1D(pop,0.0));
	if (ploidy > 2) {
		for (std::size_t m (0); m < loci; m++) {
			for (std::size_t i (0); i < pop; i++) {
				F_loc[m][i] = polyploid_f[m][i];
			}
		}
	} else {
		for (std::size_t m (0); m < loci; m++) {
			for (std::size_t i (0); i < pop; i++) {
				F_loc[m][i] = (2.0 * (*s_loc[m])[i]) - 1.0;
			}
		}
	}
	return F_loc;
}

/* ********************************************************************************************
 * Calculations::calc_F
 * --------------------------------------------------------------------------------------------
 * Calculates the inbreeding coefficient (F) as the weighted mean of F per loci.
 * NOTE: To be used with information from molecular markers
 * ********************************************************************************************/
void Calculations::calc_F (const vdou2D& F_loc, const std::vector<population>& deme, const std::vector<std::size_t>& sum_P) {
	F.assign(pop,0.0);
	for (std::size_t i (0); i < pop; i++) {
		for (std::size_t m (0); m < loci; m++) {
			F[i] += F_loc[m][i] * deme[i].getIndPerLocus(m) / sum_P[i];
		}
	}
}

/* ********************************************************************************************
 * Calculations::calc_F
 * --------------------------------------------------------------------------------------------
 * Calculates the inbreeding coefficient (F) as the weighted mean of F per loci.
 * NOTE: To be used with information from coancestry matrix
 * ********************************************************************************************/
void Calculations::calc_F (vp_double s) {
	if (ploidy > 2) {
		for (std::size_t i (0); i < pop; i++) {
			if ((*s[i]) == 1.0) F.push_back(1.0);
			else F.push_back(0.0);
		}
	} else {
		for (std::size_t i (0); i < pop; i++) {
			F.push_back(2.0*(*s[i])-1.0);
		}
	}

	/*if (ploidy) {
		for (std::size_t i (0); i < pop; ++i) {
			F.push_back(((*s[i])*ploidy-1.0)/(ploidy-1.0));
		}
	} else for (std::size_t i (0); i < pop; ++i) F.push_back(0.0);*/
}

/* ********************************************************************************************
 * Calculations::calc_G
 * --------------------------------------------------------------------------------------------
 * Calculates G
 * ********************************************************************************************/
void Calculations::calc_G (vp_vdou1D fextra, vp_double s) {
	G.assign(pop, 0.0);
	for (std::size_t i (0); i < pop; i++) {
		if ((*fextra[i])[i] != 1.0) {
			G[i] += (*(s[i]) - (*fextra[i])[i]) / (1.0 - (*fextra[i])[i]);
		}
	}
}

/* ********************************************************************************************
 * Calculations::calc_D
 * --------------------------------------------------------------------------------------------
 * Calculates D
 * ********************************************************************************************/
void Calculations::calc_D (vp_vdou1D fextra, vp_vdou1D d) {
	D.is.assign(pop, vdou1D(pop, 0.0));
	D.it.assign(pop, vdou1D(pop, 0.0));
	D.st.assign(pop, vdou1D(pop, 0.0));
	for (std::size_t i (0); i < pop; i++) {
		for (std::size_t j (0); j < pop; j++) {
			D.is[i][j] = (*d[i])[j] - (((*d[i])[i] + (*d[j])[j]) / 2.0); // Distancia de Nei
			if ((*fextra[i])[j] != 1.0) {
				D.it[i][j] = D.is[i][j] / (1.0 - (*fextra[i])[j]);
			}
			if (((*fextra[i])[i] != 0.0) || ((*fextra[j])[j] != 0.0)) {
				D.st[i][j] = -log((*fextra[i])[j] / sqrt((*fextra[i])[i] * (*fextra[j])[j]));
			}
		}
	}
}

/* ********************************************************************************************
 * Calculations::calc_bar_n
 * --------------------------------------------------------------------------------------------
 * Calculates weighted averages of genetic parameters: f, F, s and d for the metapopulation.
 * ********************************************************************************************/
void Calculations::calc_bar_n (vp_vdou1D fextra, vp_double s, vp_vdou1D d, const std::vector<population>& deme, int totalN) {
	bar_n.f = 0.0;
	bar_n.F = 0.0;
	bar_n.s = 0.0;
	bar_n.d = 0.0;

	for (std::size_t i (0); i < pop; i++) {

		bar_n.f += ((*fextra[i])[i] * deme[i].getEffectiveSize()) / totalN;
		bar_n.F += (F[i] * deme[i].getEffectiveSize()) / totalN;
		bar_n.s += ((*s[i]) * deme[i].getEffectiveSize()) / totalN;
		bar_n.d += ((*d[i])[i] * deme[i].getEffectiveSize()) / totalN;

    }
}

/* ********************************************************************************************
 * Calculations::calc_tot_n
 * --------------------------------------------------------------------------------------------
 * Calculates tot_n
 * ********************************************************************************************/
void Calculations::calc_tot_n (vp_vdou1D fextra, const std::vector<population>& deme, int totalN) {
	tot_n.f = 0.0;
	tot_n.d = 0.0;
	for (std::size_t i (0); i < pop; i++) {
		for (std::size_t j (0); j < pop; j++) {
			tot_n.d += (D.is[i][j] * deme[i].getEffectiveSize() * deme[j].getEffectiveSize()) / pow(totalN, 2.0);
			tot_n.f += ((*fextra[i])[j] * deme[i].getEffectiveSize() * deme[j].getEffectiveSize()) / pow(totalN, 2.0);
		}
	}
}

/* ********************************************************************************************
 * Calculations::calc_bar
 * --------------------------------------------------------------------------------------------
 * Calculates mean genetic parameters: f, F, s and d per subpopulation
 * ********************************************************************************************/
G_parameters <double> Calculations::calc_bar (vp_vdou1D fextra, vp_double s, vp_vdou1D d) {
	G_parameters <double> bar;
	bar.f = 0;
	bar.F = 0;
	bar.s = 0;
	bar.d = 0;

	for (std::size_t i (0); i < pop; i++) {

		bar.f += (*fextra[i])[i] / (double)pop;
		bar.F += F[i] / (double)pop;
		bar.s += *(s[i]) / (double)pop;
		bar.d += (*d[i])[i] / (double)pop;

    }

	return bar;
}

/* ********************************************************************************************
 * Calculations::calc_tot
 * --------------------------------------------------------------------------------------------
 * Calculates tot
 * ********************************************************************************************/
G_parameters <double> Calculations::calc_tot (vp_vdou1D fextra) {
	G_parameters <double> tot;
	tot.f = 0.0;
	for (std::size_t i (0); i < pop; i++) {
		for (std::size_t j (0); j < pop; j++) {
			tot.f += (*fextra[i])[j] / pow((double)pop, 2.0);
		}
	}
	return tot;
}

/* ********************************************************************************************
 * Calculations::calc_Gbar_n
 * --------------------------------------------------------------------------------------------
 * Calculates Gbar_n
 * ********************************************************************************************/
void Calculations::calc_Gbar_n (const std::vector<population>& deme, int totalN) {
	Gbar_n = 0.0;
	for (std::size_t i (0); i < pop; i++) {
		Gbar_n += G[i] * deme[i].getEffectiveSize() / totalN;
	}
}

/* ********************************************************************************************
 * Calculations::calc_DNbar
 * --------------------------------------------------------------------------------------------
 * Calculates DNbar
 * ********************************************************************************************/
void Calculations::calc_DNbar (const Fisher <vdou3D>& D_loc , const std::vector<population>& deme, const std::vector<std::size_t>& sum_L, int totalN) {

	DNbar.assign(pop,0.0);
	/*vdou2D DNbar_loc;
	vdou2D DNbar_loc_n;
	vdou1D DNbar_n;

	DNbar_loc.assign(loci, vdou1D(pop, 0.0));
	DNbar_loc_n.assign(loci, vdou1D(pop, 0.0));
	DNbar_n.assign(pop,0.0);

	for (std::size_t m (0); m < loci; m++) {
		for (std::size_t i (0); i < pop; i++) {
			for (std::size_t j (0); j < pop; j++) {
				DNbar_loc[m][i] += D_loc.is[m][i][j] / pop;
				DNbar_loc_n[m][i] += (D_loc.is[m][i][j] * deme[j].getIndPerLocus(m)) / sum_L[m];
			}
		}
	}*/

	for (std::size_t i (0); i < pop; i++) {
		for (std::size_t j (0); j < pop; j++) {
			DNbar[i] += D.is[i][j] / pop;
			//DNbar_n[i] += (D.is[i][j] * deme[j].getEffectiveSize()) / totalN;
		}
	}

}

/* ********************************************************************************************
 * Calculations::calc_barR_n
 * --------------------------------------------------------------------------------------------
 * Calculates barR_n
 * ********************************************************************************************/
void Calculations::calc_barR_n (vp_vdou1D fextra, vp_double s, const std::vector<population>& deme, const vdou1D& sum_NR) {

	barR_n.f.assign(pop,0.0);
	barR_n.s.assign(pop,0.0);

	for (std::size_t o (0); o < pop; o++) {
		for (std::size_t i (0); i < pop; i++) {
			for (std::size_t j (0); j < pop; j++) {
				if ((i != o) && (j != o)) {
					if (i == j) {
						barR_n.f[o] += (*fextra[i])[i] * deme[i].getEffectiveSize() / sum_NR[o];
						barR_n.s[o] += (*s[i]) * deme[i].getEffectiveSize() / sum_NR[o];
					}
				}
			}
		}
	}

}

/* ********************************************************************************************
 * Calculations::calc_totR_n
 * --------------------------------------------------------------------------------------------
 * Calculates totR_n
 * ********************************************************************************************/
void Calculations::calc_totR_n (vp_vdou1D fextra, const std::vector<population>& deme, const vdou1D& sum_NR) {

	totR_n.f.assign(pop,0.0);
	totR_n.d.assign(pop,0.0);

	for (std::size_t o (0); o < pop; o++) {
		for (std::size_t i (0); i < pop; i++) {
			for (std::size_t j (0); j < pop; j++) {
				if ((i != o) && (j != o)) {
					totR_n.f[o] += (*fextra[i])[j] * deme[i].getEffectiveSize() * deme[j].getEffectiveSize() / pow(sum_NR[o], 2.0);
					totR_n.d[o] += (D.is[i][j] * deme[i].getEffectiveSize() * deme[j].getEffectiveSize()) / pow(sum_NR[o], 2.0);
				}
			}
		}
	}

}

/* ********************************************************************************************
 * Calculations::calc_Fis_pop
 * --------------------------------------------------------------------------------------------
 * Calculates Fis for each subpopulation (ie. deviation from Hardy-Weinberg equilibrium
 * ********************************************************************************************/
void Calculations::calc_Fis_pop (vp_vdou1D fextra) {
	Fis_pop.assign(pop,0.0);
	for (std::size_t i (0); i < pop; i++) {
		if ((*fextra[i])[i] != 1.0) {
			Fis_pop[i] = (F[i] - (*fextra[i])[i]) / (1.0 - (*fextra[i])[i]);
		}
	}
}

/* ********************************************************************************************
 * Calculations::calc_D_loc
 * --------------------------------------------------------------------------------------------
 * Calculates D_loc
 * ********************************************************************************************/
Fisher <vdou3D> Calculations::calc_D_loc (vp_vdou2D f_loc, vp_vdou2D d_loc) {

	Fisher <vdou3D> D_loc;
	D_loc.is.assign(loci,vdou2D(pop,vdou1D(pop, 0.0)));
	D_loc.it.assign(loci,vdou2D(pop,vdou1D(pop, 0.0)));
	D_loc.st.assign(loci,vdou2D(pop,vdou1D(pop, 0.0)));
	for (std::size_t m (0); m < loci; m++) {
		for (std::size_t i (0); i < pop; i++) {
			for (std::size_t j (0); j < pop; j++) {
				D_loc.is[m][i][j] = (*d_loc[m])[i][j] - (((*d_loc[m])[i][i] + (*d_loc[m])[j][j]) / 2.0);
				if ((*f_loc[m])[i][j] != 1.0) {
					D_loc.it[m][i][j] = D_loc.is[m][i][j] / (1.0 - (*f_loc[m])[i][j]);
				}
				if ((((*f_loc[m])[i][j] != 0.0) && ((*f_loc[m])[i][i] != 0.0)) && (((*f_loc[m])[i][j] != 0.0) && ((*f_loc[m])[j][j] != 0.0))) {
					D_loc.st[m][i][j] = -(log((*f_loc[m])[i][j] / sqrt((*f_loc[m])[i][i] * (*f_loc[m])[j][j])));
				}
			}
		}
	}
	return D_loc;
}

/* ********************************************************************************************
 * Calculations::bar_loc (NOT IN USE)
 * --------------------------------------------------------------------------------------------
 * Calculates bar_loc
 * ********************************************************************************************/
G_parameters <vdou1D> Calculations::calc_bar_loc (vp_vdou2D f_loc, const vdou2D& F_loc, vp_vdou1D s_loc, vp_vdou2D d_loc) { // (before: fbar_loc, Fbar_loc, sbar_loc, dbar_loc)

	G_parameters <vdou1D> bar_loc;
	bar_loc.f.assign(loci, 0.0);
	bar_loc.F.assign(loci, 0.0);
	bar_loc.s.assign(loci, 0.0);
	bar_loc.d.assign(loci, 0.0);

	for (std::size_t m (0); m < loci; m++) {
		for (std::size_t i (0); i < pop; i++) {
			bar_loc.f[m] += (*f_loc[m])[i][i] / pop;
			bar_loc.F[m] += F_loc[m][i] / pop;
			bar_loc.s[m] += (*s_loc[m])[i] / pop;
			bar_loc.d[m] += (*d_loc[m])[i][i] / pop;
		}
	}
	return bar_loc;
}

/* ********************************************************************************************
 * Calculations::tot_loc (NOT IN USE)
 * --------------------------------------------------------------------------------------------
 * Calculates tot_loc
 * ********************************************************************************************/
G_parameters <vdou1D> Calculations::calc_tot_loc (const Fisher <vdou3D>& D_loc, vp_vdou2D f_loc) {
	G_parameters <vdou1D> tot_loc;
	tot_loc.d.assign(loci, 0.0);
	tot_loc.f.assign(loci, 0.0);

	for (std::size_t m (0); m < loci; m++) {
		for (std::size_t i (0); i < pop; i++) {
			for (std::size_t j (0); j < pop; j++) {
				tot_loc.d[m] += D_loc.is[m][i][j] / pow(pop, 2.0);
				tot_loc.f[m] += (*f_loc[m])[i][j] / pow(pop, 2.0);
			}
		}
	}
	return tot_loc;
}

/* ********************************************************************************************
 * Calculations::calc_G_loc (NOT IN USE)
 * --------------------------------------------------------------------------------------------
 * Calculates the observed heterozygosity
 * ********************************************************************************************/
vdou2D Calculations::calc_G_loc (vp_vdou2D f_loc, vp_vdou1D s_loc) {
	vdou2D G_loc;
	G_loc.assign(loci, vdou1D(pop, 0.0));
	for (std::size_t m (0); m < loci; m++) {
		for (std::size_t i (0); i < pop; i++) {
			if ((*f_loc[m])[i][i] != 1.0) {
				G_loc[m][i] += ((*s_loc[m])[i] - (*f_loc[m])[i][i]) / (1.0 - (*f_loc[m])[i][i]);
			}
		}
	}
	return G_loc;
}

/* ********************************************************************************************
 * Calculations::calc_G_loc (NOT IN USE)
 * --------------------------------------------------------------------------------------------
 * Calculates the observed heterozygosity
 * ********************************************************************************************/
vdou1D Calculations::calc_Gbar_loc_n (const vdou2D& G_loc, const std::vector<population>& deme, const std::vector<std::size_t>& sum_L) {
	vdou1D Gbar_loc_n;
	Gbar_loc_n.assign(loci, 0.0);
	for (std::size_t m (0); m < loci; m++) {
		for (std::size_t i (0); i < pop; i++) {
			Gbar_loc_n[m] += G_loc[m][i] * deme[i].getIndPerLocus(m) / sum_L[m];
		}
	}
	return Gbar_loc_n;
}

/* ********************************************************************************************
 * Calculations::calc_Ho (NOT IN USE)
 * --------------------------------------------------------------------------------------------
 * Calculates the observed heterozygosity
 * ********************************************************************************************/
vdou1D Calculations::calc_Ho () {
	vdou1D Ho;
	for (std::size_t i (0); i < pop; ++i) {
		Ho.push_back(1.0 - F[i]);
	}
	return Ho;
}

/* ********************************************************************************************
 * Calculations::calc_Fis_pop_loc (NOT IN USE)
 * --------------------------------------------------------------------------------------------
 * Calculates Fis for each population
 * ********************************************************************************************/
vdou2D Calculations::calc_Fis_pop_loc (vp_vdou2D f_loc, const vdou2D& F_loc) {

	vdou2D Fis_pop_loc;
	Fis_pop_loc.assign(loci, vdou1D(pop, 0.0));
	for (std::size_t m (0); m < loci; m++) {
		for (std::size_t i (0); i < pop; i++) {
			if ((*f_loc[m])[i][i] != 1.0) {
				Fis_pop_loc[m][i] = (F_loc[m][i] - (*f_loc[m])[i][i]) / (1.0 - (*f_loc[m])[i][i]);
			}
		}
	}
	return Fis_pop_loc;
}

/* ********************************************************************************************
 * Calculations::calc_Fst_b
 * --------------------------------------------------------------------------------------------
 * 	Calculates Fst per pair of populations
 * ********************************************************************************************/
void Calculations::calc_Fst_b (vp_vdou1D fextra, const std::vector<population>& deme) {

	vdou2D ftot_n_;
	ftot_n_.assign(pop, vdou1D(pop, 0.0));
	vdou2D fbar_n_;
	fbar_n_.assign(pop, vdou1D(pop, 0.0));

	for (std::size_t i (0); i < pop; i++) {
		for (std::size_t j (i + 1); j < pop; j++) {
			ftot_n_[i][j] = ((*fextra[i])[i] * deme[i].getEffectiveSize() * deme[i].getEffectiveSize() + (*fextra[j])[j] * deme[j].getEffectiveSize() * deme[j].getEffectiveSize() + 2.0 * (*fextra[i])[j] * deme[i].getEffectiveSize() * deme[j].getEffectiveSize()) / pow((deme[i].getEffectiveSize() + deme[j].getEffectiveSize()), 2.0);
			fbar_n_[i][j] = ((*fextra[i])[i] * deme[i].getEffectiveSize() + (*fextra[j])[j] * deme[j].getEffectiveSize()) / (deme[i].getEffectiveSize() + deme[j].getEffectiveSize());
		}
	}

	Fst_b.assign(pop, vdou1D(pop, 0.0));
	for (std::size_t i (0); i < pop; i++) {
		for (std::size_t j (i + 1); j < pop; j++) {
			Fst_b[i][j] = (fbar_n_[i][j] - ftot_n_[i][j]) / (1 - ftot_n_[i][j]);
		}
	}

}

/* ********************************************************************************************
 * Calculations::calc_F_loc_n (NOT IN USE)
 * --------------------------------------------------------------------------------------------
 * Calculates global F-statistics for the metapopulation
 * ********************************************************************************************/
Fisher <vdou1D> Calculations::calc_F_loc_n (vp_vdou2D f_loc, const vdou2D& F_loc, vp_vdou1D s_loc , vp_vdou2D d_loc , vdou3D D_loc_is, const std::vector<population>& deme, const std::vector<std::size_t>& sum_L) {

	G_parameters <vdou1D> tot_loc_n; // before Dtot_loc_n, ftot_loc_n
	tot_loc_n.d.assign(loci, 0.0);
	tot_loc_n.f.assign(loci, 0.0);

	for (std::size_t m (0); m < loci; m++) {
		for (std::size_t i (0); i < pop; i++) {
			for (std::size_t j (0); j < pop; j++) {
				tot_loc_n.d[m] += (D_loc_is[m][i][j] * deme[i].getIndPerLocus(m) * deme[j].getIndPerLocus(m)) / pow(sum_L[m], 2.0);
				tot_loc_n.f[m] += ((*f_loc[m])[i][j] * deme[i].getIndPerLocus(m) * deme[j].getIndPerLocus(m)) / pow(sum_L[m], 2.0);
			}
		}
	}

	G_parameters <vdou1D> bar_loc_n;
	bar_loc_n.f.assign(loci, 0.0);
	bar_loc_n.F.assign(loci, 0.0);
	bar_loc_n.s.assign(loci, 0.0);
	bar_loc_n.d.assign(loci, 0.0);

	for (std::size_t m (0); m < loci; m++) {
		for (std::size_t i (0); i < pop; i++) {
			bar_loc_n.f[m] += ((*f_loc[m])[i][i] * deme[i].getIndPerLocus(m)) / sum_L[m];
			bar_loc_n.F[m] += (F_loc[m][i] * deme[i].getIndPerLocus(m)) / sum_L[m];
			bar_loc_n.s[m] += ((*s_loc[m])[i] * deme[i].getIndPerLocus(m)) / sum_L[m];
			bar_loc_n.d[m] += ((*d_loc[m])[i][i] * deme[i].getIndPerLocus(m)) / sum_L[m];
		}
	}

	Fisher <vdou1D> F_loc_n; // (before: Fis_loc_n, Fst_loc_n, Fit_loc_n)
	F_loc_n.is.assign(loci,0.0);
	F_loc_n.st.assign(loci,0.0);
	F_loc_n.it.assign(loci,0.0);

	for (std::size_t m (0); m < loci; m++) {

		if (bar_loc_n.f[m] != 1.0) {
			F_loc_n.is[m] = (bar_loc_n.F[m] - bar_loc_n.f[m]) / (1.0 - bar_loc_n.f[m]);
		}
		if (tot_loc_n.f[m] != 1.0) {
			F_loc_n.st[m] = (bar_loc_n.f[m] - tot_loc_n.f[m]) / (1.0 - tot_loc_n.f[m]);
			F_loc_n.it[m] = (bar_loc_n.F[m] - tot_loc_n.f[m]) / (1.0 - tot_loc_n.f[m]);
		}
	}
	return F_loc_n;
}

/* ********************************************************************************************
 * Calculations::Nrare_loc
 * --------------------------------------------------------------------------------------------
 * Calculates rarefation sample size for each locus. With ADrare = false this is the smallest
 * number of individuals with complete sampled alleles between subpopulations.
 * ********************************************************************************************/
void Calculations::calc_Nrare (const std::vector<population>& deme, int ADrareN) {
	Nrare_loc.clear();
	for (std::size_t m (0); m < loci; m++) {
		if (ADrareN) Nrare_loc.push_back (ADrareN);
		else Nrare_loc.push_back(deme[0].getIndPerLocus(m));
		for (std::size_t i (0); i < pop; i++) {
			if (deme[i].getIndPerLocus(m) < Nrare_loc[m]) {
				Nrare_loc[m] = deme[i].getIndPerLocus(m);
			}
		}
	}
}

/* ********************************************************************************************
 * Calculations::calc_distAl_loc
 * --------------------------------------------------------------------------------------------
 * Calculates the allelic distance between populations
 * ********************************************************************************************/
vdou3D Calculations::calc_distAl_loc (vdou2D& alRich, allele2D code, const std::vector<population>& deme, vp_vdou2D ps, bool ADrare) {

	// Calculate probability of loss and allelic richness
	vdou3D probLoss;
	for (std::size_t i (0); i < pop; i++) {
		std::vector<std::vector<double>> v2_probloss;
		for (std::size_t m (0); m < loci; m++) {
			std::vector<double> v1_probloss;
			for (std::size_t k (0); k < code[m].size(); k++) {
				double ind_probloss (0.0);
				int a (2 * deme[i].getIndPerLocus(m));
				double b (((*ps[i])[m][k] * 2.0 * deme[i].getIndPerLocus(m)));
				if (!ADrare) {
					if (b > 0.0) {
						ind_probloss = 0.0;
					} else {
						ind_probloss = 1.0;
					}
				} else {
					int c (2 * Nrare_loc[m]);
					if ((a - b) < c) {
						ind_probloss = 0.0;
					} else {
						double num = LogFactorial((int) (a - b)) - LogFactorial((int) (a - b - c)) - LogFactorial(c);
						double den = LogFactorial(a) - LogFactorial(a - c) - LogFactorial(c);
						ind_probloss = exp(num) / exp(den);
					}
				}
				v1_probloss.push_back(ind_probloss);
			}
			v2_probloss.push_back(v1_probloss);
		}
		probLoss.push_back(v2_probloss);
	}

	// Calculate number of alleles
	Kpop.assign (pop, 0);
	for (std::size_t i (0); i < pop; i++) {
		for (std::size_t m (0); m < loci; m++) {
			double num (0.0);
			for (std::size_t k (0); k < code[m].size(); k++) {
				if ((*ps[i])[m][k]>0.0) num += 1.0;
			}
			Kpop[i] += num/loci;
		}
	}

	// Calculate allelic richness
	alRich.assign(loci,vdou1D(pop, 0.0));
	for (std::size_t i (0); i < pop; i++) {
		for (std::size_t m (0); m < loci; m++) {
			for (std::size_t k (0); k < code[m].size(); k++) {
				alRich[m][i] += 1.0 - probLoss[i][m][k];
			}
		}
	}

	// Calculate allelic distance
	vdou3D distAl_loc;
	for (std::size_t m (0); m < loci; m++) {
		vdou2D mat_dis;
		for (std::size_t i (0); i < pop; i++) {
			vdou1D row_dis;
			for (std::size_t j (0); j < pop; j++) {
				double dis (0.0);
				for (std::size_t k (0); k < code[m].size(); k++) {
					dis += 0.5 * (((1.0 - probLoss[i][m][k]) * probLoss[j][m][k]) + (probLoss[i][m][k] * (1.0 - probLoss[j][m][k])));
				}
				row_dis.push_back(dis);
			}
			mat_dis.push_back(row_dis);
		}
		distAl_loc.push_back(mat_dis);
	}

	return distAl_loc;
}

/* ********************************************************************************************
 * Calculations::calc_A
 * --------------------------------------------------------------------------------------------
 * Calculates the average within and between allelic diversity
 * ********************************************************************************************/
void Calculations::calc_A (const vdou2D& alRich, const vdou3D& distAl_loc) {

	vdou1D As;
	As.assign(loci,0.0);
	vdou1D Da_loc;
	Da_loc.assign(loci,0.0);
	//vdou1D Ast_loc;
	//Ast_loc.assign(loci,0.0);
	for (std::size_t m (0); m < loci; m++) {
		for (std::size_t i (0); i < pop; i++) {
			As[m] += alRich[m][i];
		}
		As[m] = (As[m] / pop) - 1.0;

		for (std::size_t i (0); i < pop; i++) {
			for (std::size_t j (0); j < pop; j++) {
				Da_loc[m] += distAl_loc[m][i][j];
			}
		}
		Da_loc[m] = Da_loc[m] / (pop * pop);
		//Ast_loc[m] = Da_loc[m] / (As[m] + Da_loc[m]);
	}

	A.da = 0;
	A.as = 0;
	for (std::size_t m (0); m < loci; m++) {
		A.as += As[m] / loci;
		A.da += Da_loc[m] / loci;
	}
}

/* ********************************************************************************************
 * Calculations::calc_Kmed
 * --------------------------------------------------------------------------------------------
 * Calculates Kmed. This is the average number of alleles per locus.
 * ********************************************************************************************/
void Calculations::calc_Kmed (const allele2D& code) {
	Kmed = 0.0;
	for (std::size_t m (0); m < loci; m++) {
		Kmed += code[m].size();
	}
	Kmed = Kmed / loci;
}

/* ********************************************************************************************
 * Calculations::calc_AST
 * --------------------------------------------------------------------------------------------
 * Calculates the coefficient of allelic differentiation (Ast), a measure proportional to the
 * number of alleles in which two randomly chosen subpopulations differ.
 * ********************************************************************************************/
void Calculations::calc_AST () {
	AST = A.da / (A.as + A.da);
}

/* ********************************************************************************************
 * Calculations::calc_con_A
 * --------------------------------------------------------------------------------------------
 * Calculates within and between (distance) allelic diversity per population
 * ********************************************************************************************/
void Calculations::calc_con_A (const vdou2D& alRich, const vdou3D& distAl_loc) {
	for (std::size_t i (0); i < pop; i++) {
		double as (0.0);
		double da (0.0);
		for (std::size_t m (0); m < loci; m++) {
			as += (alRich[m][i] - 1.0) / loci;
			for (std::size_t j (0); j < pop; j++) {
				da += distAl_loc[m][i][j] / (loci * pop);
			}
		}
		con_.as.push_back(as);
		con_.da.push_back(da);
	}
}

void Calculations::calc_con_n (const std::vector<population>& deme, int totalN) {
	con_n.as = 0.0;
	con_n.da = 0.0;

	for (std::size_t i (0); i < pop; i++) {
		con_n.as += (con_.as[i] * deme[i].getEffectiveSize()) / totalN;
		con_n.da += (con_.da[i] * deme[i].getEffectiveSize()) / totalN;
    }
}

/* ********************************************************************************************
 * Calculations::calc_pop_A
 * --------------------------------------------------------------------------------------------
 * Calculates Ast and Da/K between pairs of populations
 * ********************************************************************************************/
void Calculations::calc_pop_A (const vdou3D& distAl_loc) {
	pop_.da.assign(pop, vdou1D(pop, 0.0));
	pop_.as.assign(pop, vdou1D(pop, 0.0));
	for (std::size_t i (0); i < pop; i++) {
		//vdou1D pop_as_row;
		//vdou1D pop_da_row;
		for (std::size_t j (i + 1); j < pop; j++) {
			//double da (0.0);
			for (std::size_t m (0); m < loci; m++) {
				//da += (distAl_loc[m][i][i] + distAl_loc[m][i][j] + distAl_loc[m][j][i] + distAl_loc[m][j][j]) / (4.0 * loci);
				//pop_.da[i][j] += ((distAl_loc[m][i][i] + distAl_loc[m][i][j] + distAl_loc[m][j][i] + distAl_loc[m][j][j]) / (4.0 * loci));
				pop_.da[i][j] += distAl_loc[m][i][j] / loci;
			}
			//pop_da_row.push_back(da);
			//pop_as_row.push_back(da / (((con_.as[i] + con_.as[j]) / 2.0) + da));
			pop_.as[i][j] = (pop_.da[i][j] / (((con_.as[i] + con_.as[j]) / 2.0) + pop_.da[i][j]));
		}
		//pop_.as.push_back(pop_as_row);
		//pop_.da.push_back(pop_da_row);
	}
}

/* ********************************************************************************************
 * Calculations::calc_rem_A
 * --------------------------------------------------------------------------------------------
 * Calculates calc_rem_A
 * ********************************************************************************************/
void Calculations::calc_rem_A (const vdou3D& distAl_loc) {

	rem_.as.assign(pop, 0.0);
	rem_.da.assign(pop, 0.0);

	for (std::size_t o (0); o < pop; o++) {
		for (std::size_t i (0); i < pop; i++) {
			if (i != o) {
				rem_.as[o] += (con_.as[i] / (pop - 1.0));
			}

			for (std::size_t j (0); j < pop; j++) {
				for (std::size_t m (0); m < loci; m++) {
					if ((i != o) && (j != o)) {
						rem_.da[o] += distAl_loc[m][i][j] / (loci * (pop - 1.0) * (pop - 1.0));
					}
				}
			}
		}
	}

}
