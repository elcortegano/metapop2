#ifndef MANAGEMENT_H
#define MANAGEMENT_H

#include "calculations.h"
#include "output.h"
#include "utils.h"


// =====   EXTERN VARIABLES   =====
extern bool DEBUG;


// =====   VARIABLES   =====
static std::size_t searchpop;
static std::size_t monogamy_penalty_count;
static std::size_t forbidden_penalty_count;
static std::size_t pop, loci, totalN, nmigra, sa_steps, migra, convergence;
static bool isol, monogamy, simMode, restricted, forbidden;
static double lambda, tasa, sa_t, sa_k;
static vdou2D fmol;
static vdou3D prob_caso_init, prob_ind_init;
static vshort2D offspring, struc;
static allele2D code;
static vint1D demeInd;
static vint2D forbid_mates;
static vint2D NperSex;
static Management manage;
static vint4D allelic_rich_map;


// =====   FUNCTION'S PROTOTYPES   =====
void management ( const MTPConfigFile& , Metapop& , const Calculations& , std::string , int , int );
double simulated_annealing ( const std::vector<population>& , vint3D& , vint2D& , const vshort2D& , double );
bool validSol ( const vint3D& , const vint2D& );
double evaluate ( const vint3D& , const vint2D& , double );
inline void evaluate_genetic_div ( double& , double& , const vint3D& );
inline void evaluate_allelic_div ( double& , double& , const vint3D& );
inline void evaluate_allelic_num ( double& , const vint3D& );
inline void initialize_migrants ( const vint3D& );
inline void calculate_migrants ( const vint3D& );
inline void initialize_allelic ( const std::vector<population>& );
bool check_forbid ( std::size_t , std::size_t , vint2D& , vint2D& );
bool check_forbid ( vint2D& , vint2D& );

#endif
