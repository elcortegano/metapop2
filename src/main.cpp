/* =======================================================================================
 *                                                                                       *
 *       Filename:  main.cpp                                                             *
 *                                                                                       *
 *    Description:  Metapop is a software Analysis of gene and allelic diversity in      *
 *                  subdivided populations from molecular genotype or coancestry data,   *
 *                  as well as a tool for management in conservation programs.           *
 *                                                                                       *
 *                  Please, refer to the README file to know more about input and output *
 *                  files, and how the program works.                                    *
 *                                                                                       *
 *                                                                                       *
 *        Version:  2.5.3                                                                *
 *        Created:  19/12/2011                                                           *
 *        Updated:  05/12/2024                                                           *
 *       Compiler:  g++ (11.4.0)                                                         *
 *                                                                                       *
 *         Author:  Eugenio López-Cortegano                                              *
 *          Email:  elcortegano@protonmail.com                                           *
 *   Organization:  Aviagen                                                              *
 *                                                                                       *
 ======================================================================================= */


#include "main.h"

int main (int argc, char** argv) {

	// Check arguments and config file. Initialization of setting variables
	std::string data_filename;
	std::string config_filename;
	time_t current_time (time(0));
	std::cout << "Metapop2 (" << version << ")" << std::endl;

	//checkArguments(argc, argv, data_filename, config_filename);
	MTPConfigFile config_file (argc, argv, data_filename);
	random_generator.seed(config_file.get_seed()); // seed the generator

	// Local variables
	std::string caso (config_file.get_caso());
	std::string res_filename ("res_" + caso + "_FULL.md");
	bool simMode (config_file.get_simMode());
	DEBUG = config_file.get_debug();

	// Main
	if (config_file.get_convert_mode() == "none") {

		save_header (config_file, data_filename, res_filename, current_time);

		for (int simR (0); simR < config_file.get_simReps(); simR++) {
			// Check and read datafile
			Metapop metapop (data_filename, config_file);
			metapop.check (config_file);

			for (int simT (0); simT < config_file.get_simGens (); simT++) {

				if (simMode) std::cout << "******************\nRep: " << simR+1 << "   Gen: " << simT << std::endl;
				if (!simT) std::cout << "Metapopulation input and checked!" << std::endl;

				// Calculate population frequencies
				std::cout << "Calculating coancestry and genetic distances...";
				std::cout << std::flush;
				metapop.compute_diversity ();
				std::cout << "\rCalculating coancestry and genetic distances...done!" << std::endl;
				save_freq (config_file, metapop, res_filename, simR, simT);
				Calculations calculations (config_file, metapop);

				// Save population analysis and calculate contributions to a pool
				if (!simR && !simT) {
					std::cout << "Running population analyses...";
					std::cout << std::flush;
					Calculations_boot calc_boot (config_file, metapop); // Compute boostrap replicates (if defined)
					save_pop_analysis (config_file, metapop, res_filename, calculations, calc_boot);
					std::cout << "\rRunning population analyses...done!" << std::endl;
					if (config_file.get_synth () != NO) {
						rankPops (config_file, metapop, calculations, res_filename);
					}
				}
				metapop.reset_genetic_parameters();

				// Run management method
				if ( config_file.get_manage() != NONE ) {
					management (config_file, metapop, calculations, res_filename, simR, simT);
				}
			}
		}
	} else if (config_file.get_convert_mode() == "mtp2gp" || config_file.get_convert_mode() == "mtp2ped") {
		Metapop datafile_c (data_filename, config_file);
		convert(datafile_c, config_file);
    } else {
		Metapop datafile_c (data_filename, config_file, config_file.get_convert_mode());
		convert(datafile_c, config_file);
	}

	if (simMode) std::cout << row_sep << std::endl;
	std::cout << "Finish!" << std::endl;

	return 0;
}
