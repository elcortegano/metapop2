#ifndef METAPOP_H
#define METAPOP_H


#include <cmath>
#include <random>
#include <deque>
#include "alias.h"
#include "utils.h"
#include "population.h"
#include "MTPConfigFile.h"


// =====   LOCAL VARIABLES   =====
const std::string OPT_GENERATIONS = "t";
const std::string OPT_MATRIX = "matrix";
const std::string OPT_RESTRI = "restrictions";
const std::string OPT_FORBID = "forbidden";


// =====   EXTERN VARIABLES   =====
extern bool DEBUG;
extern std::mt19937 random_generator;


// =====   ERROR VARIABLES   =====
const std::string

	ERROR_PAIRS = "Locus identifiers must have a pair number of digits when using diploid genomes.",
	ERROR_DLOCI = "All individuals must have input for all declared loci.",
	ERROR_MTP_LABEL = "A one-word label is required for naming each subpopulation.",
	ERROR_MTP_NLOCI = "Individuals have different number of loci.",
	ERROR_MTP_NEXTG = "Number of individuals in next generation has not been set for all subpopulations",
	ERROR_MTP_SIZEP = "Population size set, but not for all subpopulations",
	ERROR_MTP_NESZP = "Effective population size set, but not for all subpopulations",
	ERROR_MTP_MTCHN = "Number of subpopulations set does not match actual number of subpopulations",
	ERROR_MTP_MTCHL = "Number of loci set does not match actual number of loci",
	ERROR_MTP_SUBPS = "Specified subpopulation size does not match actual size",
	ERROR_MTP_SEXFM = "Individuals sex should take values of 0 or 1.",
	ERROR_MTP_FMSYM = "Coancestry matrix is not symmetric.",
	ERROR_MTP_FMRNG = "Coancestry values out of range [0,1].",
	ERROR_MTP_MXFOR = "Cannot mix legacy and simplified entry methods for input individuals' information",
	ERROR_MTP_COMMA = "Simplified metapop entry format only accepts one comma per individual inputed",
	ERROR_MTP_HEADS = "Simplified metapop entry format requires an optional ID, and a label, but more terms are given at the left of the 'comma' character",
	ERROR_MTP_PLOAL = "Number of alleles detected is not a multiple of the ploidy level in at leat one individual (eg. it should be even for diploid genomes)",
	ERROR_MTP_SXMNG = "Management method set, but sex information has not been included in metapop simplified input file.",
	ERROR_MTP_OPTSX = "(Optional) Individual sex declared for some individuals but not for all. Remove or declare it for every individual.",
	ERROR_MTP_NUMFM = "Management method set, but at least one subpopulation has individuals of the same gender.",
	ERROR_MTP_BOOTL = "Boostrap sample has an unexpected number of loci.",
	ERROR_GNP_DELIM = "Expected comma delimiter in genotypes lines.",
	ERROR_OPT_NCOLS = "Restrictions matrix requires for columns: female ID, male ID, number of female offspring, and male offspring. It is mandatory to include one row per female.",
	ERROR_OPT_FEMID = "Females IDs are wrong, non-sorted or incomplete in restriction matrix. Remember that IDs must be sorted from 1 to N for management with restrictions.",
	ERROR_OPT_SIRID = "Unexpected sirs ID in restriction/forbidden matrix. It must be a valid ID of a male in the same subpopulation than the female. Remember that IDs must be sorted from 1 to N for management with restrictions.",
	ERROR_OPT_MSIZE = "Restriction matrix does not include all declared females",
	ERROR_OPT_NEWID = "The declared progeny in the restriction matrix is lower than the desired number of individuals for the next generation (defined with 'newN').",
	ERROR_OPT_FSIZE = "Forbidden matrix does not include all declared females",
	ERROR_OPT_FEMIF = "Females IDs are wrong, non-sorted or incomplete in forbidden matrix",
	ERROR_OPT_MALEN = "All males are forbidden for at least one female. At least one must be available.",
	ERROR_OPT_REPID = "At least one male ID is repeated in forbidden matrix.";


// =====   STRUCTS   =====
template <typename T>
struct BT_parameter { // The name of the struct refers to parameters estimated for the between-populations or (total) metapopulation component
	std::vector<std::vector<T>*> b_par;
	std::vector<T*> t_par;
};


// =====   INLINE FUNCTIONS   =====
inline bool test_pop (std::string pop) {
	bool is_pop (true);
	pop.erase(pop.begin(), std::find_if(pop.begin(), pop.end(), std::bind1st(std::not_equal_to<char>(), ' ')));
	if (pop != "POP" && pop != "Pop" && pop != "pop") {
		is_pop = false;
	}
	return is_pop;
}


// ====   CLASSES   ====
class Metapop {

protected:

	/* Metapopulation */
	std::vector<population> deme;	// a vector for each subpopulation, with phenotypic genetic information

	/* Config */
	Format input_format;
	bool for_management;
	std::deque<int> mng_t; // under simulation for management, it indicates the generations with structural changes
	std::deque<vshort2D> mng_t_offspring; // newN offspring matrix for generations of simulation beyond t=1
	bool use_nucleotides;
	unshort ploidy;

	/* Parameters directly read form the input file */
	std::string filename;	// name of the input file
	std::size_t n_subpops;	// the number of supopulations
	std::size_t n_loci;		// the number of loci genotyped
	std::size_t N;		// total number of individuals
	std::size_t Ne;		// total effective size
	vshort2D progeny;		// A matrix with a row per subpopulation,
							// and a column for daughters and other for
							// sons to be born in the next generation

	/* Parameters directly inferred from the input file */
	std::size_t N_females;
	allele2D alleles_per_locus; // a matrix with a row per loci, and the different alleles shorted in each row
	vshort2D struc;
	vdou2D f_mol;					// inputed matrix of molecular coancestry

	/* Parameters used to check input file format */
	bool legacy_inds;
	bool progeny_set;
	bool set_sex;
	bool size_set;

	/* Additional options */
	bool use_molecular_markers; // true if coancestry is calculated from molecular markers and is not inputed in matrix form
	bool for_simulation;
	int sim_gens;
	bool fmatrix_read;
	bool temporal_marks;
	bool restricted_mates;
	bool forbidden_mates;
	bool read_pedigree;
	vint2D mates; // a matrix with one female per row, with information on the mated male, and the number of females and males in the progeny
	vint2D mates_padopt; // mates matrix in padopt format (one row per subpopulation, and a column per female, the values indicate the index of the mating male
	std::size_t check_sir ( population , int );
	std::size_t check_sir_contrib ( int , bool );
	vshort2D mates_limit; // a matrix with one row per individual,  a column with the number of daughters, and another for the sons
	vint2D mates_forbid; // a matrix with one female per row, and forbidden male mates in columns (first column is reserved to female ID)

	/* Basic population genetic parameters */
	mp_vdou2D ind_freq;			// this vector contains the frecuencies of each locus and individual:
								// ind_freq [subpopulation] [individual] [locus] [allele (as in "code")]
	mp_vbool1D eval_ind_locus;	// this vector keep track of the individuals with information at a given locus
	BT_parameter <vdou1D> p;	// Populational frequencies
	BT_parameter <double> s;	// Self-coancestries
	BT_parameter <vdou1D> f;	// Average coancestries
	BT_parameter <vdou1D> d;	// Average distances
	vint1D unique_alleles;		// Number of private alleles per subpopulation
	vdou2D polyploid_f;			// Vector of inbreeding for polyploids

public:

	// Constructor & destructor
	Metapop ( std::string , const MTPConfigFile& );
	Metapop ( std::string , const MTPConfigFile& , std::string ); // user for conversion to metapop format
	Metapop ( const Metapop& ); // copy constructor for bootstrap
	void update ( std::string , bool );
	void check ( const MTPConfigFile& );
	~Metapop ();

	// Estimate genetic parameters
	void compute_diversity ();
	std::vector<std::size_t> informative_inds () const;
	std::vector<std::size_t> informative_loci () const;

	// "Get" methods
	std::string get_filename () const { return filename; }
	std::vector<population> get_deme () const { return deme; }
	allele2D get_alleles_per_locus () const { return alleles_per_locus; }
	vshort2D get_struc () const { return struc; }
	std::size_t get_n_subpops () const { return n_subpops; }
	std::size_t get_n_loci () const { return n_loci; }
	std::size_t get_N ( bool e) const {
		if (e) return Ne;
		else return N;
	}
	std::size_t get_maxN () const {
		std::size_t maxN(0);
		for (const auto& pop: deme) if (pop.getPopulationSize() > maxN) maxN = pop.getPopulationSize();
		return maxN;
	}
	bool get_sameN () const {
		std::size_t Nfirst (deme[0].getPopulationSize());
		for (std::size_t i(1); i<deme.size(); ++i) if (deme[i].getPopulationSize() != Nfirst) return false;
		return true;
	}
	vdou2D get_fmol () const { return f_mol; }
	vshort2D get_progeny () const { return progeny; }
	bool get_use_molecular_markers () const { return use_molecular_markers; }
	bool get_use_restricted_mates () const { return restricted_mates; }
	bool get_use_forbidden_mates () const { return forbidden_mates; }
	vint2D get_padopt () const { return mates_padopt; }
	vshort2D get_limit () const { return mates_limit; }
	vint2D get_forbidden () const { return mates_forbid; }
	bool get_setsex () const { return set_sex; }
	vint1D get_unique_alleles () const { return unique_alleles; }
	std::deque<int> get_mng_t () const { return mng_t; };
	std::deque<vshort2D> get_mng_t_offspring () const { return mng_t_offspring; }

	BT_parameter <vdou1D> get_p () const { return p; }
	BT_parameter <double> get_s () const { return s; }
	BT_parameter <vdou1D> get_f () const { return f; }
	BT_parameter <vdou1D> get_d () const { return d; }

	vdou2D get_polyploid_f () const { return polyploid_f; }

	// "Set" methods
	void pop_mng_t () { mng_t.pop_front(); mng_t_offspring.pop_front(); }

	// "Free" methods
	void free_bt ( std::string );
	void free_bt_all ();
	void reset_genetic_parameters ();


protected:

	// Input methods
	void input_metapop ( std::string );
	void input_genepop ( std::string );
	void input_pedfile ( std::string, bool, std::string );
	void default_mng ();
	void input_management ( std::string );
	void end_body ( std::size_t& , std::size_t& , std::size_t& , std::string& , std::vector<individual>& );

	// Estimate genetic parameters
	void calculate_ind_freq ();
	void calculate_p ();
	void calculate_s ();
	void calculate_f ();
	void calculate_d ();
	void calculate_unique_alleles ();

	// Parameters inference
	void append_alleles ( const allele1D& , std::size_t );
	void clean_alleles ();
	void set_struc ();

	// Check parameters
	void print_error ( std::string );
	void print_mng_error ( std::string );
	std::string translate_format ();
	void wrong_subpop ( std::string );
	void wrong_optional ( std::string );
	void row_error ( std::size_t );
	void short_frow ();
	void large_frow ();
	void large_cols ( std::size_t );
	void test_synth_inpmatrix ( Synthetic );
	void test_management_inpmatrix ( Management );
	void test_mng ();
	void test_rm ();
	void test_pedigree (const vint2D& );
	void test_final ();
	allele_t get_allele ( std::string& );

	// Coancestry methods
	//void coancestry_matrix ();
	void coancestry_from_freq ();
	void genealogical_matrix ( const vint2D& );
	double MeanCoancestryInd ( int , int , int , int );

	// Optional methods
	void opt_tmp_mng ( std::stringstream& , std::string );
	void read_rm ( std::stringstream& );
	void read_manage ( std::stringstream& );
	void read_fmatrix ( std::stringstream& );
	void read_mates ( std::stringstream& );
	void read_forbid ( std::stringstream& );

};

#endif
