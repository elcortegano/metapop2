#ifndef INDIVIDUAL_H
#define	INDIVIDUAL_H


#include <string>
#include <vector>
#include <sstream>
#include "locus.h"
using namespace std;

class individual {

	std::vector <locus> loci;
	std::size_t n_loci;
	std::string label;
	std::size_t uniqueID;
	unshort gender;

public:

	individual ( std::string , unshort , unshort , const std::vector<locus>& );

	locus getLocus ( const std::size_t& ) const;
	std::size_t getLociNumber ();
	std::string getLabel ();
	int getSex ();
	int getID ();
	std::string getGenAsStr ();

};

#endif
