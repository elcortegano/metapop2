/* =======================================================================================
 *                                                                                       *
 *       Filename: population.cpp                                                        *
 *                                                                                       *
 *    Description: This file defines the object class population.                        *
 *                                                                                       *
 *                  -> population::population ()                                         *
 *                  -> population::get_* ()                                              *
 *                  -> population::calc* ()                                              *
 *                                                                                       *
 ======================================================================================= */

#include "population.h"
#include "individual.h"

/* ********************************************************************************************
 * population::population ()
 * --------------------------------------------------------------------------------------------
 * Constructor that initializes the population with values for default or information about the
 * number of individuals and loci
 * ********************************************************************************************/
population::population (std::string name, std::vector<individual>& Inds) {
	label = name;
	pop = Inds;
	N = pop.size();
	Ne = N;
	std::size_t n_loci (pop[0].getLociNumber()); // note that all individuals are checked for having the same number of loci
	this->sortInds();
	this->calcNperSex();
	this->calcIndPerLoci(n_loci);
}

/* ********************************************************************************************
 * Population::addIndividuals ()
 * --------------------------------------------------------------------------------------------
 * Updates the population status with more individuals
 * ********************************************************************************************/
void population::addIndividuals (std::vector<individual> is) {
	for (auto& i:is) {
		pop.push_back(i);
		++N;
		++Ne;
	}
	this->sortInds();
	this->calcNperSex();
	this->calcIndPerLoci(is[0].getLociNumber());
}

/* ********************************************************************************************
 * Population::sortInds ()
 * --------------------------------------------------------------------------------------------
 * Sort individuals in the population, ladies first
 * ********************************************************************************************/
void population::sortInds () {

	// Check if sorting is needed
	bool need_sorting (false);
	bool ladies_first (false);
	if (pop[0].getSex() == 0) ladies_first = true;
	if (!ladies_first) need_sorting = true;
	if (!need_sorting) {
		for (std::size_t i(1); i<pop.size(); ++i) {
			if ((pop[i].getSex() == 0) && (pop[i-1].getSex())) {
				need_sorting = true;
				break;
			}
		}
	}

	// Sorting
	if (need_sorting) {
		std::vector<individual> tmp_pop;
		for (std::size_t i(0); i<pop.size(); ++i) {
			if (pop[i].getSex()==0) tmp_pop.push_back(pop[i]);
		}
		for (std::size_t i(0); i<pop.size(); ++i) {
			if (pop[i].getSex()==1) tmp_pop.push_back(pop[i]);
		}
		pop = tmp_pop;
	}
}

/* ********************************************************************************************
 * Population::calcIndPerLocii ()
 * --------------------------------------------------------------------------------------------
 * Calculates the number of individuals per loci
 * ********************************************************************************************/
void population::calcIndPerLoci (int loci) {
	indLocus.clear();
	for (int l (0); l < loci; ++l) {
		std::size_t cont_ind (0);
		std::size_t cont_gen (0);
		for (std::size_t i (0); i < N; ++i) {
			locus gene (pop[i].getLocus(l));
			if (gene.status()) {
				++cont_ind;
				allele1D v_alleles (pop[i].getLocus(l).getAllels());
				for (auto a: v_alleles) if (a) ++cont_gen;
			}
		}
		indLocus.push_back(cont_ind);
	}
}

/* ********************************************************************************************
 * Population:: calcNperSex ()
 * --------------------------------------------------------------------------------------------
 * Calculates the number of females (0) and males (1) in the population 
 * ********************************************************************************************/
void population::calcNperSex() {
	NperSex.clear();
	NperSex.resize (2, 0);
	for (std::size_t i (0); i < pop.size(); ++i) {
		++NperSex[pop[i].getSex()];
	}
}
