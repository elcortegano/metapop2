#ifndef LOCUS_H
#define	LOCUS_H

#include <string>
#include "alias.h"
using namespace std;

class locus {

	allele1D alleles;
	bool exist;

public:

	locus ( const std::vector<allele_t>& );
	locus ( const allele_t& , const allele_t& );
	allele_t getA ( const std::size_t& ) const;
	allele1D getAllels () const { return alleles; }
	allele_t getNAllels ( const allele_t& ) const;
	bool status () const;

protected:

	void check ();
};

#endif

