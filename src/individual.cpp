/* =======================================================================================
 *                                                                                       *
 *       Filename: individual.cpp                                                        *
 *                                                                                       *
 *    Description: This file defines the object class indvidual. It is oriented to the   *
 *                 classical definition of individual by Metapop, but also of Genepop    *
 *                 and PLINK.                                                            *
 *                                                                                       *
 *                  -> individual::individual ()                                         *
 *                  -> individual::get_* ()                                              *
 *                                                                                       *
 ======================================================================================= */

#include "individual.h"
#include "locus.h"

/* ********************************************************************************************
 * individual::individual ()
 * --------------------------------------------------------------------------------------------
 * Constructor that initializes the individual with values for default or information about the
 * number of loci
 * ********************************************************************************************/
individual::individual (std::string fname, unshort ID, unshort sex, const std::vector<locus>& genes) {
	label = fname;
	uniqueID = ID;
	gender = sex;
	loci = genes;
	n_loci = loci.size();
}

/* ********************************************************************************************
 * individual:: GET methods ()
 * --------------------------------------------------------------------------------------------
 * List of methods to return protected attributes
 * ********************************************************************************************/
locus individual::getLocus (const std::size_t& index) const {
	return loci[index];
}

std::string individual::getLabel() {
	return label;
}

std::size_t individual::getLociNumber () {
	return n_loci;
}

int individual::getID() {
	return uniqueID;
}

int individual::getSex() {
	return gender;
}

std::string individual::getGenAsStr() {
	std::string cad = "";
	for (std::size_t l (0); l < loci.size(); l++) {
		std::ostringstream os;
		os << loci[l].getA(0) << " " << loci[l].getA(1) << " ";
		cad.append(os.str());
	}
	return cad;
}
