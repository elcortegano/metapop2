

# METAPOP2 OUTPUT  
Version: v2.5.3  
5/12/2024 at 14:26:42  
Seed: 1234  

Data file: templates/input.ped  
Configuration file: templates/conf_example  
Name: example  


## 1. Results of Population Analysis  


### GENE DIVERSITY  


### Within-subpopulation parameters  
* **$f_{ii}$**: Average coancestry between individuals.  
* **$s_{i}$**: Average self-coancestry of individuals.  
* **$F_{i}$**: Average coefficient of inbreeding of individuals.  
* **$d_{ii}$**: Average Nei distance between individuals.  
* **$G_{i}$**: Proportion of diversity between individuals.  
* **$α_{i}$**: Average deviation from Hardy-Weinberg equilibrium.  

  Subpop.   | $f_{ii}$ | $s_{i}$  | $F_{i}$  | $d_{ii}$ | $G_{i}$ | $α_{i}$ 
------------|----------|----------|----------|----------|---------|----------
    Pop1    |  0.3611  |  0.7083  |  0.4167  |  0.3472  | 0.5435  |  0.087   
    Pop2    |  0.3611  |  0.7083  |  0.4167  |  0.3472  | 0.5435  |  0.087   
    Pop3    |  0.4688  |   0.75   |   0.5    |  0.2813  | 0.5294  |  0.0588  
**Average** |**0.388** |**0.7188**|**0.4375**|**0.3307**|**0.54** |**0.0809**


### Between-subpopulation parameters  
* **$f_{ij}$**: Average coancestry between pairs of subpopulations.  
* **$D_{G,ij}$**: Nei's minimum genetic distance between pairs of subpopulations.  
* **$F_{ST,ij}$**: Gene frequency differentiation index between pairs of subpopulations.  

  Subpop.   | $f_{ij}$ | $D_{G,ij}$ | $F_{ST,ij}$ 
------------|----------|------------|-------------
Pop1 - Pop2 |  0.3611  |     0      |      0      
Pop1 - Pop3 |  0.3854  |   0.0295   |    0.0232   
Pop2 - Pop3 |  0.3854  |   0.0295   |    0.0232   


### Average gene diversity parameters  
* $H_{S}$: Average gene diversity within subpopulations = 0.612  
* $D_{G}$: Average gene diversity between subpopulations (Nei's minimum genetic distance) = 0.0111  
* $H_{T}$: Total gene diversity = 0.623  


### F-statistics  
* $F_{IS}$ = 0.0809  
* $F_{ST}$ = 0.0178  
* $F_{IT}$ = 0.0972  


### Partition of gene diversity variation (in proportion)  
* Within individuals = 0.4519  
* Between individuals = 0.5304  
* Within subpopulations = 0.9822  
* Between subpopulations = 0.0178  


### Percentage of loss (+) or gain (-) of gene diversity after removal of each subpopulation  

Subpop. removed | $H_{S}$ | $D_{G}$ | $H_{T}$
----------------|---------|---------|--------
     Pop1       | 2.6469  | -0.508  | 2.1388 
     Pop2       | 2.6469  | -0.508  | 2.1388 
     Pop3       | -4.212  | 1.7323  |-2.4796 


### ALLELIC DIVERSITY (with rarefaction)  
Average rarefacted sample size per locus = 4  


### Within-subpopulation parameters  
* **$A_{S,i}$**: Within-subpopulation allelic diversity (allelic richness with rarefaction or not, minus 1).  
* **$K_{i}$**: Mean number of alleles per locus in the subpopulation.  
* **$A_{p,i}$**: Mean number of private alleles per locus in the subpopulation.  


   Subpop.    | $A_{S,i}$ | $K_{i}$ | $A_{p,i}$
--------------|-----------|---------|----------
     Pop1     |  2.2677   |   3.5   |     0    
     Pop2     |  2.2677   |   3.5   |     0    
     Pop3     |    2.5    |   3.5   |     0    
  **Average** |**2.3258** | **3.5** |   **0**  


### Between-subpopulation parameters  
* **$D_{A,ij}$**: Allelic distance between pairs of subpopulations.  
* **$A_{ST,ij}$**: Allelic differentiation index between pairs of subpopulations.  

  Subpop.   | $D_{A,ij}$ | $A_{ST,ij}$ 
------------|------------|-------------
Pop1 - Pop2 |   0.1723   |    0.0706   
Pop1 - Pop3 |   0.1162   |    0.0465   
Pop2 - Pop3 |   0.1162   |    0.0465   


### Average allelic diversity parameters  
* $A_{S}$: Average allelic diversity within subpopulations = 2.3258  
* $D_{A}$: Average allelic diversity between subpopulations = 0.1346  
* $A_{T}$: Total allelic diversity = 2.4603  


### $A_{ST}$-statistic  
* $A_{ST}$ = 0.0518  


### Percentage of loss (+) or gain (-) of allelic diversity after removal of each subpopulation  

Subpop. removed |  $A_{S}$   |   $D_{A}$   | $A_{T}$ 
----------------|------------|-------------|---------
      Pop1      |  -1.5655   |  1.0937     |  -0.4719
      Pop2      |  -1.5655   |  1.0937     |  -0.4719
      Pop3      |  3.1311    |  -1.7828    |  1.3482 


### SYNTHETIC POPULATION  

Percentage of individuals contributing from each subpopulation to a pool of 1000 individuals with maximal expected heterozygosity ($H$) and total number of alleles ($K$).  


Subpopulation | $H$ | $K$ 
--------------|-----|-----
      Pop1    | 49  | 33.7
      Pop2    | 51  | 33.8
      Pop3    |  0  | 32.5


## 2. Mating matrix  

Subpop. | Family | fem. | mal.  | Pop1 |      | Pop2 |      | Pop3 |     
--------|--------|------|-------|------|------|------|------|------|-----
  Pop1  |   1    |  1   |   4   |   1  |   0  |   0  |   0  |   0  |   0 
  Pop1  |   2    |  2   |   6   |   1  |   2  |   1  |   0  |   1  |   0 
  Pop1  |   3    |  3   |   5   |   1  |   1  |   0  |   0  |   0  |   0 
  Pop2  |   4    |  7   |   11  |   0  |   0  |   1  |   1  |   0  |   1 
  Pop2  |   5    |  8   |   10  |   0  |   0  |   1  |   1  |   1  |   0 
  Pop3  |   6    |  14  |   16  |   0  |   0  |   0  |   1  |   0  |   1 
Energy = -2.4882  
Migrations = 5  
Mean (female) contributions = 2.6667  
Variance of (female) contributions = 1.8667  
