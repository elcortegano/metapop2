# metapop v.2.alpha.0
This version matches that olderly tagged as **metapop+ 14.08**

## Installation
- create a new empty directory wherever you want: 
```bash
mkdir metapop
cd metapop
```
- download metapop:
```bash
wget https://gitlab.com/elcortegano/metapop2/blob/master/releases/v.2.alpha.0/metapop2.alpha.0.tar.gz
```
- Unzip:
```bash
tar -zxvf metapop.14.08.tar.gz
```
- Compile:
```bash
make
```
- Move the binary called `metapop` to a directory in your PATH or to a working directory

## To run metapop
Metapop2 needs a datafile (in the right format) and a configuration file with the options and parameters (a template is provided in the above distribution as `configMTP`, copy it into your working directory and modify it accordingly to your needs) [*Documentation about this file and its options is to come, but using the manual for the old versions of Metapop would help to understand most of the options. Anyaway, don't hesitate to ask me any question about these options*]

Assuming you have your `datafile` and your modified `configMTP` files (*Note that you can use the names you want for these files*) in your working directory and the `metapop` executable in a directory in your $PATH, then the program is launched in this way:
```
metapop datafile configMTP
```
After finish and depending on the options used, you will see a bunch of files (to be documented) with the name you have specified in the configuration file for this run. Of all of them you probably want that with the suffix **.FULL** that have a text report with all the results as older versions of Metapop showed in html.


